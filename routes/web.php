<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
				'namespace' => 'User',
				// 'prefix' => LaravelLocalization::setLocale(),
				// 'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
], function(){

	Route::get('/','RoutesController@index')->name('index');
	Route::get('/faq','RoutesController@faq')->name('faq');
	Route::get('/hakkimizda','RoutesController@hakkinda')->name('hakkinda');
	Route::get('/servislerimiz','RoutesController@servislerimiz')->name('servislerimiz');
	Route::get('/servis/{slug}','RoutesController@servis')->name('servis');
	Route::get('/arac-kirala','RoutesController@rentacar')->name('rentacar');
	

	Route::get('/iletisim','RoutesController@iletisim')->name('iletisim');
    Route::get('/iletisim/send', 'RoutesController@sendEmail')->name('iletisim.sendEmail');
	Route::get('/emlak-kategori/{id?}','RoutesController@ilanlar')->name('ilanlar');
	Route::get('/emlak-kategori/{id?}/{id2?}/detay','RoutesController@ilandetay')->name('ilan');
	Route::get('/duyurular','RoutesController@duyurular')->name('duyurular');
	Route::get('/duyuru/{slug}','RoutesController@duyuru')->name('duyuru');


	Route::get('/all-houses','RoutesController@tumIlanlar')->name('tumIlanlar');
	Route::get('/house-details/{id?}','RoutesController@ilanDetaylari')->name('ilanDetaylari');

	Route::get('/searchb/','RoutesController@productSearch')->name('productSearch');
	Route::get('/search/','RoutesController@houseSearch')->name('house.search');

		// AJAX
	Route::get('/search/ilan-ver/ilan-turu/{id?}', 'RoutesController@ilanturu');
	Route::get('/search/ilan-ver/il/{id?}', 'RoutesController@ilcelerBul');
	Route::get('/search/ilan-ver/ilan-grubu/{id}/{id2}', 'RoutesController@ilangrubu');


	Route::post('/house-reservation','RoutesController@houseReservation')->name('user.house.reservation');

	 Route::post('/iletisim/email-gonder','RoutesController@mailSend')->name('contact.mail');
	 Route::post('/iletisim/email-gonder-2','RoutesController@mailSend')->name('contact.mail2');

});


Route::group([
				'namespace' => 'Members',
				'verify' => false,
				//'prefix' => LaravelLocalization::setLocale(),
				//'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
], function(){

	Route::get('/member/convert','CoinController@convert')->name('member.index');
	Route::post('/member/my-investments/convert/{amount_usd}','CoinController@convert')->name('member.index');
	
	Route::get('/member/cash-deposit-transactions','FinancialController@casheDeposit')->name('member.casheDeposit');
	Route::get('/member/cash-deposit-transactions-cancel','FinancialController@casheDepositDelete')->name('member.casheDeposit.cancel');
	Route::post('/member/cash-deposit-transactions-confirmation','FinancialController@casheDepositSend')->name('member.casheDeposit.Send');
	Route::get('/member/cash-deposit-confirmation','FinancialController@casheDepositConfirmation')->name('member.casheDeposit.casheDepositConfirmation');
	
	Route::get('/member/my-investments','FinancialController@myInvestments')->name('member.myInvestments');
	Route::get('/member/cash-withdrawal-transactions','FinancialController@casheWithdrawal')->name('member.casheWithdrawal');
	Route::get('/member/cash-withdrawal-request/{amount_usd?}','FinancialController@requsetCasheWithdrawal')->name('member.requsetCasheWithdrawal');


	Route::get('/member/home','HomeController@index')->name('member.index');
	Route::get('/member/profile','ProfileController@profile')->name('member.profile');
	Route::get('/member/profile/edit','ProfileController@editProfile')->name('member.editProfile');
	Route::post('/member/profile/change-password','ProfileController@changePassword')->name('member.changePassword');
	Route::post('/member/profile/save','ProfileController@profileSave')->name('member.profileSave');
	
	Route::get('/member/wallet-addresses','ProfileController@walletAddresses')->name('member.walletAddresses');
	Route::post('/member/wallet-addresses','ProfileController@walletAddressesSave')->name('member.walletAddresses.save');

	Route::get('/member/reference-list','ReferenceController@reference')->name('member.reference');
  	Route::get('/about-the-system', 'HomeController@aboutSystem')->name('member.aboutSystem');
	
	
	
});



Auth::routes(['verify' => true]);


Route::group([
				'namespace' => 'Admin',
				'prefix' => 'cpadmin',
				
], function(){
	
  ######### Auth Routes Starts
  Route::get('/', 'AdminController@index')->name('admin.dashboard');
  Route::get('dashboard', 'AdminController@index')->name('admin.dashboard');
  // Route::get('register', 'AdminController@create')->name('admin.register');
  // Route::post('register', 'AdminController@store')->name('admin.register.store');
  Route::get('login', 'Auth\LoginController@login')->name('admin.auth.login');
  Route::post('login', 'Auth\LoginController@loginAdmin')->name('admin.auth.loginAdmin');
  Route::post('logout', 'Auth\LoginController@logout')->name('admin.auth.logout');

  ######### Auth Routes Ends

  Route::post('/dashboard','Kullaniciontroller@changeAdminPassword')->name('admin.changeAdminPassword');


  Route::get('/all-users', 'UsersController@getAllUsers')->name('admin.getAllUsers');
  Route::get('/delete-user/{id?}', 'UsersController@delete')->name('admin.delete.user');

  Route::get('/payment-list', 'InvestmentController@paymentList')->name('admin.payment.list');
  Route::get('/confirmed-payment-list', 'InvestmentController@confirmedList')->name('admin.payment.confirmed');
  Route::get('/confirmed-payment/{id?}', 'InvestmentController@confirmPayment')->name('admin.confirm.payment');
  Route::get('/delete-payment/{id?}', 'InvestmentController@deletePayment')->name('admin.delete.application');


  Route::get('/withdrawal-list', 'WithdrawalController@withdrawalList')->name('admin.withdrawal.list');
  Route::get('/confirmed-withdrawal-list', 'WithdrawalController@confirmedList')->name('admin.withdrawal.confirmed');
  Route::get('/confirmed-withdrawal/{id?}', 'WithdrawalController@confirmWithdrawal')->name('admin.confirm.withdrawal');
  Route::get('/withdrawal-confirmed-withdrawal/{id?}', 'WithdrawalController@withdrawalConfirmWithdrawal')->name('admin.withdrawal.confirm.withdrawal');
  Route::get('/delete-withdrawal/{id?}', 'WithdrawalController@deletewithdrawal')->name('admin.withdrawal.delete');

  Route::get('/end-of-month-withdraw-list', 'WithdrawalController@endMonthPaymentList')->name('admin.payment.month');
 

  Route::get('/user-details/{id?}', 'Kullaniciontroller@checkDetails')->name('admin.user.checkDetails');



	 // Ilan Özellikler

	Route::get('/property/all', 'PropertyController@index')->name('admin.property.all');
    Route::post('property/save', 'PropertyController@save')->name('admin.property.ekle.save');
	Route::get('/property/delete/{id?}', 'PropertyController@delete')->name('admin.property.delete');

	// Ilan Posts



	Route::get('/house/all', 'HouseController@index')->name('admin.house.all');
	Route::get('/house/add', 'HouseController@add')->name('admin.house.add');
    Route::post('house/add/save', 'HouseController@save')->name('admin.house.add.save');
	Route::get('/house/detay/{id?}', 'HouseController@detay')->name('admin.house.detay');
	
	Route::get('/house/delete/{id?}', 'HouseController@delete')->name('admin.house.delete');
   
    Route::get('/house/edit/{id?}', 'HouseController@duzenle')->name('admin.house.duzenle');
	Route::get('/house/image/{id?}', 'HouseController@deleteimage')->name('admin.house.deleteimage');
	Route::post('house/edit/save/{id?}', 'HouseController@duzenleYap')->name('admin.house.duzenle.save');


	Route::get('/house-reservation-list','HouseController@getAllReservations')->name('admin.house.reservation.list');
	

	Route::get('/all-information','NewsController@index')->name('admin.information.list');





});