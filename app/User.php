<?php

namespace App;

use App\Members\Investment;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{ 
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                'name',
                'email',
                'password',
                'phone',
                'city',
                'country',
                'avatar',
                'code',
                'status'
            ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',

    ];

   
    public function investments()
    {
        return $this->hasMany('App\Members\Investment');
    }


    public function withdraw()
    {
        return $this->hasMany('App\Members\Withdraw');
    }

      public function user()
    {
        return $this->hasOne('App\User','id', 'owner_id');
    }


     
}
