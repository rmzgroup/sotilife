<?php

namespace App\Members;

use App\Members\Investment;
use Illuminate\Database\Eloquent\Model;

class Investment extends Model
{	
	protected $fillable = [
		'user_id','amount_usd','amount_crypto','status','address_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function petek()
    {
        return $this->belongsTo('App\Members\Petek');
    }
   
    public function owner()
    {
        return $this->belongsTo('App\User','id', 'owner_id');
    }



    public function ActiveTeamUsers($id)
    {
        $numberOfActiveTeamUsers = Investment::where([
                        'owner_id' => $id,
                        ])->orderBy('created_at', 'desc')->count();
       
        return $numberOfActiveTeamUsers;
    }



    public static function getGainPerActiveTeamUsers($number)
    {
            
        if ($number == 4 ) {
            return 80;
        }elseif ($number == 1) {
           return 65;
        }elseif ($number == 2) {
            return 70;
        }elseif($number == 3) {
            return 75;
        }else{
            return 60;  
        }

       
    }



    public function getUserAddress($id)
    {
        $userAddress = Address::where([
                        'user_id' => $id,
                        ])->first();
        
        return $userAddress;
    }


}
