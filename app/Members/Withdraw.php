<?php

namespace App\Members;

use App\Members\Address;
use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
	protected $fillable = [
		'user_id','amount_usd','request_date','status'
	];

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function address()
	{
		return $this->belongsTo('App\Members\Address');
	}

	public function investment()
	{
		return $this->belongsTo('App\Members\Investment');
	}


    public function ActiveTeamUsers($id)
    {
        $numberOfActiveTeamUsers = Investment::where([
                        'owner_id' => $id,
                        ])->orderBy('created_at', 'desc')->count();

        return $numberOfActiveTeamUsers;
    }



    public static function getGainPerActiveTeamUsers($numberOfActiveTeamUsers)
    {
        

        if ($numberOfActiveTeamUsers == 1 ) {
            return 65;
        }elseif ($numberOfActiveTeamUsers == 2) {
           return 70;
        }elseif ($numberOfActiveTeamUsers == 3) {
            return 75;
        }elseif ($numberOfActiveTeamUsers > 3) {
            return 80;
        }else{
            return 60;
        }       
       
    }

    
      public function getUserAddres($id)
    {
        $userAddress = Address::where([
                        'id' => $id,
                        ])->first();
       
        return $userAddress;
    }

}
