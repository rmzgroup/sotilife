<?php

namespace App\Members;

use Illuminate\Database\Eloquent\Model;

class Petek extends Model
{
	protected $fillable = [
		'user_id','coin_type','amount_usd','amount_crypto','status'
	];

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function investments()
    {
        return $this->hasMany('App\Members\Investment');
    }
   
}
