<?php

namespace App\Members;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
	protected $fillable = [
		'user_id','team_id',
	];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
