<?php

namespace App\Model\Admin;

use App\Model\Admin\House;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{	
	protected $fillable = [
		'title'
	];



     public function houses()
    {
        return $this->belongsToMany(House::class);
    }
}
