<?php

namespace App\Model\Admin;

use App\Model\Admin\Property;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{	

	protected $fillable = [
		'title','house_no','price','city','address','dimension','room_number','hit_system','bathrom_number','balcon','main_image','latitude','longitude','house_active','description'
	];



     public function properties()
    {
        return $this->belongsToMany(Property::class);
    }
}
