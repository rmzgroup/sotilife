<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
		'house_id','name','email','phone','entry','out'
	];

}
