<?php
namespace App\Traits;

use App\Members\Investment;
use App\Members\Petek;
use App\Members\Soty;
use App\Members\Team;
use App\User;
use Auth;


trait SotyTrait {



  // Get the number of petek from the amount in USD
  public function getNumberOfPetek($amount_usd)
  {
    return $amount_usd / 50;
  }

  public function updateSotiesNumber($id, $numberOfSoties)
  {

    $getSoty = Investment::where('id', $id)->first();

    $owner = User::where('id', $getSoty->owner_id)->first();

    if ($owner) {
       $ownerPresentSotiesNumber = $owner->soties_number + $numberOfSoties;

      User::where('id', $owner->id)
                 ->update(['soties_number' => $ownerPresentSotiesNumber]);
    }
  }

  
  public function updatePetekNumber($id, $numberOfSoties)
  {

    $getSoty = Investment::where('id', $id)->first();

    $user = User::where('id', $getSoty->user_id)->first();

    $userPresentPetekNumber = $user->petek_number + $numberOfSoties;

    User::where('id', $user->id)
               ->update(['petek_number' => $userPresentPetekNumber]);
  }


    public function AddPetekToDatabase($id, $numberOfSoties)
  {

    $getSoty = Investment::where('id', $id)->first();

    $numberOfPeteks = $getSoty->petek_number;

   

    for ($i=0; $i < $numberOfPeteks; $i++) { 
       
        $newPetek = new Petek;
       
        $newPetek->user_id = $getSoty->user_id;
        $newPetek->investment_id = $getSoty->id;
        $newPetek->save();
    }

  }

      public function DeletePetekToDatabase($id, $numberOfSoties)
  {

    $getSoty = Investment::where('id', $id)->first();

    $numberOfPeteks = $getSoty->petek_number;

    Petek::where('investment_id', $getSoty->id)->delete();

  }



    



    public function updateDeleteSotiesNumber($id, $numberOfSoties)
  {

    $getSoty = Investment::where('id', $id)->first();

    $owner = User::where('id', $getSoty->owner_id)->first();

    if ($owner) {
       $ownerPresentSotiesNumber = $owner->soties_number - $numberOfSoties;

      User::where('id', $owner->id)
                 ->update(['soties_number' => $ownerPresentSotiesNumber]);
    }
  }


    public function updateDeletePetekNumber($id, $numberOfSoties)
  {

    $getSoty = Investment::where('id', $id)->first();

    $user = User::where('id', $getSoty->user_id)->first();

    $userPresentPetekNumber = $user->petek_number - $numberOfSoties;

    User::where('id', $user->id)
               ->update(['petek_number' => $userPresentPetekNumber]);
  }


      public function getTheNumberOfSotiesFromAmount($amount_usd)
    {
      
        if ($amount_usd == 80 ) {
            return 4;
        }elseif ($amount_usd == 75) {
           return 3;
        }elseif ($amount_usd == 70) {
            return 2;
        }elseif ($amount_usd > 65) {
            return 1;
        }else{
            return 0;
        }       
       
    }


    public function updateDeleteWithdrawSotiesNumber($id, $numberOfSoties)
  {

    $user = User::where('id', $id)->first();

       $ownerPresentSotiesNumber = $user->soties_number - $numberOfSoties;

      User::where('id', $user->id)
                 ->update(['soties_number' => $ownerPresentSotiesNumber]);
  }


    public function updateDeleteWithdrawPetekNumber($id, $numberOfSoties)
  {

     $user = User::where('id', $id)->first();

       $ownerPresentSotiesNumber = $user->petek_number - $numberOfSoties;

      User::where('id', $user->id)
                 ->update(['petek_number' => $ownerPresentSotiesNumber]);
  }





































    public function getNumberOfSotiesBags()
    {

        $allSoties = Auth::user()->soties_number;
        
        return $allSoties;
    }

   public function getNumberOfPaidSoties()
   {
        $userID = Auth::user()->id;

        $allSoties = Investment::where([
                          ['owner_id', '=' , $userID],
                          ['status', '=' , 'Inactive'],
                        ])->count();
        
        return $allSoties;
   }

    public function getGainPerActiveTeamUsers()
    {
    	
        $user = Auth::user();
        
        $numberOfActiveTeamUsers = Investment::where([
                        'owner_id' => $user->id,
                        'status' => 'Active'
                        ])->orderBy('created_at', 'desc')->count();


        if ($numberOfActiveTeamUsers == 1 ) {
            return 65;
        }elseif ($numberOfActiveTeamUsers == 2) {
           return 70;
        }elseif ($numberOfActiveTeamUsers == 3) {
            return 75;
        }elseif ($numberOfActiveTeamUsers > 3) {
            return 80;
        }else{
            return 60;
        }       
       
    }

    public function ActiveTeamUsers()
    {
         $user = Auth::user();
        
        $numberOfActiveTeamUsers = Investment::where([
                         ['owner_id','=', $user->id],
                         ['status', '=','Active'],
                         ['status', '!=' , 'Paid']
                        ])->orderBy('created_at', 'desc')->count();

        return $numberOfActiveTeamUsers;
    }

}