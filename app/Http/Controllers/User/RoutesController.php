<?php

namespace App\Http\Controllers\User;

use App;
use App\Http\Controllers\Controller;
use App\Model\Admin\House;
use App\Model\Admin\House_property;
use App\Model\Admin\Houseimage;
use App\Model\Admin\Reservation;
use App\Model\admin\application;
use App\Model\admin\info;
use App\Model\admin\product;
use App\Model\admin\productimage;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Image;
use LaravelLocalization;
use Redirect;
use Storage;
use Validator;

class RoutesController extends Controller
{

    public function index()
    {   

        $houses = House::orderBy('created_at','DESC')->paginate('10');

        
      return view('user/index',compact('houses'));
    }
   


    public function iletisim()
    {
      return view('user/contact');
    }

    public function hakkinda()
    {
      return view('user/about-us');
    }

    public function duyurular()
    {
      return view('user/informations');
    }


    public function duyuru()
    {
      return view('user/information');
    }

    
    public function servislerimiz()
    {
      return view('user/servis');
    }

     public function servis()
    {
      return view('user/servis');
    }

    public function tumIlanlar()
    {

      $houses = House::orderBy('created_at','DESC')->paginate('10');

      return view('user/tumIlanlar',compact('houses'));
    }

    public function ilanDetaylari($id)
    {
        $house = House::where('id',$id)->first();
        $houseimages = Houseimage::where('house_id',$id)->get();

        $house_properties = House_property::where('house_id',$id)->get();

      return view('user/ilan-detaylari',compact('house','houseimages','house_properties'));
    }



    public function houseReservation(Request $request)
    {

        $newRes = Reservation::create($request->all());

        $house = House::where('id',$newRes->house_id)->first();
        $houseimages = Houseimage::where('house_id',$newRes->house_id)->get();

        $house_properties = House_property::where('house_id',$newRes->house_id)->get();


        $string = $newRes->name.' thank you for your reservation. We will contact you soon to confirm your reservation. Have a pleasant day.';



         return redirect()->back()->with('success', $string);

    }
    
    public function rentacar()
    {
      return view('user/cars');
    }



    
    public function houseSearch(Request $request)
    {

      $city = $request->city;
      $price = explode(" - ", $request->price);

      $minPrice = explode("$", $price[0]);
      $maxPrice = explode("$", $price[1]);

      if ($city) {
          $houses = House::where([
                                ['city', '=', $city],
                                ['price', '>=', $minPrice[1]],
                                ['price', '<=', $maxPrice[1]]
                            ])->orderBy('created_at','DESC')->paginate('10');
      }else{
          $houses = House::where([
                                ['price', '>=', $minPrice[1]],
                                ['price', '<=', $maxPrice[1]]
                            ])->orderBy('created_at','DESC')->paginate('10');
      }

      return view('user/tumIlanlar',compact('houses'));
     
    }








    public function sendEmail(Request $request)
    {
    
     $data = array(
                    'name' => $request->name,
                    'email' => $request->email,
                    'phonenumber' => $request->phone,
                    'subject' => $request->subject,
                    'bodyMessage'  => $request->message
                   );

  

    

            if( count(Mail::failures()) > 0 ){
            
                $request->session()->flush();  
                return redirect('iletisim')->with('error','Ooopppsss, E-postanız gönderilemedi. Lütfen daha sonra tekrar deneyiniz.');
            }else{
                
                $request->session()->flush();
               
                return redirect('iletisim')->with('success','E-postanız başarıyla gönderildi. Bizimle iletişime geçtiğiniz için teşekkürler.');
            }

     
    }


            public function ilcelerBul($id)
        {
          $ilceler = DB::table('ilcelers')
                             ->where('il_id', '=', $id)
                             ->orderBy('name','ASC')
                             ->pluck("name","id");

           return json_encode($ilceler);  
        }

          public function ilanturu($id)
        {
          

          $ilanturu = DB::table('types')
                             ->orderBy('title_tr','ASC')
                             ->pluck("title_tr","id");


           return json_encode($ilanturu);  
        }

        public function ilangrubu($id, $id2)
        {
           $ilceler = DB::table('emlak_types')
                             ->where([
                              'type_id' => $id,
                              'emlak_id' => $id2
                             ])
                             ->orderBy('title_tr','ASC')
                            ->pluck("title_tr","id");

           return json_encode($ilceler);  
        }



    public function mailSend(Request $request)
    {
    
     $data = array(
                    'name' => $request->name,
                    'email' => $request->email,
                    'subject' => 'Sotylife Contact Form',
                    'bodyMessage'  => $request->message
                   );

   

     Mail::send(
                    'emails.contact',
                    $data, 
                    function($message) use ($data) {
                        $message->from( $data['email'] );
                        $message->to('info@sotylife.com')
                        // ->bcc(array(''))
                        ->subject( $data['subject'] );
                    }
                );


            if( count(Mail::failures()) > 0 ){
                return redirect()->back()->with('error','Ooopppsss, There was error sending your email. Please try again later');
            }else{
                return redirect()->back()->with('success','Your email was successfully sent.');
            }

     
    }


}



