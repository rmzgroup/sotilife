<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Members\Soty;
use Illuminate\Http\Request;
use Jimmerioles\BitcoinCurrencyConverter\Converter;
use Jimmerioles\BitcoinCurrencyConverter\Provider\BitpayProvider;
use Jimmerioles\BitcoinCurrencyConverter\Provider\CoinbaseProvider;
use Jimmerioles\BitcoinCurrencyConverter\Provider\CoindeskProvider;
use Auth;


class CoinController extends Controller
{   

          /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function convert(Request $request)
    {   

    	$convert = new Converter(new CoinbaseProvider);
		// $convert = new Converter(new CoindeskProvider);
		// $convert = new Converter(new BitpayProvider);
		// 

    	// $info = $convert->toCurrency('USD', 1);

    	// dd(json_encode($info));
    	// dd($request);

    	// $bitcoin = $convert->toCurrency('USD', $request->amount_usd);

    	$bitcoin = $convert->toBtc($request->amount_usd, 'USD');
    	$ether = $convert->toBtc($bitcoin, 'ETH');
    	$litecoin = $convert->toBtc($bitcoin, 'LTC');
    	$dogecoin = $convert->toBtc($bitcoin, 'LTC');

    	$data = array(
	      "bitcoin" => $bitcoin,
	      "ether" => $ether,
	      "litecoin" => $litecoin,
	      "dogecoin" => $dogecoin
	    );


		return json_encode($data);


    }


}
