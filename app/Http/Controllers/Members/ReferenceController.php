<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Members\Investment;
use App\Members\Soty;
use App\Members\Team;
use App\User;
use Auth;
use Illuminate\Http\Request;

class ReferenceController extends Controller
{	

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
     public function reference()
    {   

      $user = Auth::user();

      $team = User::where([
                    ['owner_id', '=' , $user->id], 
                    ['id', '!=' ,'$user->id']
                  ])->orderBy('created_at', 'desc')->get();
     
      $soties = Investment::where('status', 'Active')->orderBy('created_at', 'desc')->get();
     

      return view('members/reference', compact('team','soties'));
    }
   
}
