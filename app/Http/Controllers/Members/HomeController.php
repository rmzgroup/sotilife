<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Members\Investment;
use App\Members\Withdraw;
use App\Traits\SotyTrait;
use App\User;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{	

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    
    use SotyTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    
     public function index()
    {   

        $user = Auth::user();

       $numberOfActiveTeamInvestments = $user->soties_number;
       $number_of_investments = $user->petek_number; 

        $teamNumber = User::where('owner_id', $user->id)->count();


        $allUserWithdraws = Withdraw::where([
                                                ['user_id','=',$user->id],
                                                ['status','=','Paid']
                                            ])->get();
        $userEarnings = 0;

           foreach ($allUserWithdraws as $userWithdraws) {
                    $userEarnings = $userEarnings + $userWithdraws->amount_usd - 50;
           }
       
         $totalUserEarnings = 0;

           foreach ($allUserWithdraws as $userWithdraws) {
                    $totalUserEarnings = $totalUserEarnings + $userWithdraws->amount_usd;
           }
        

           $allUserInvestements = Investment::where([
                                                ['user_id','=',$user->id],
                                                ['status','=','Active']
                                            ])->get();

           $totalUserInvestment = 0;

           foreach ($allUserInvestements as $userInvestment) {
                    $totalUserInvestment = $totalUserInvestment + $userInvestment->amount_usd;
           }
        
            $allUsersNumber = User::count();

       return view('members/home',compact('user','number_of_investments','numberOfActiveTeamInvestments','teamEarnings','teamNumber','userEarnings','totalUserEarnings','totalUserInvestment','allUsersNumber'));
    }
   

   public function aboutSystem()
   {
       return view('members/aboutSystem');
   }
}


