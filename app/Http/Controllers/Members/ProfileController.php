<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Members\Investment;
use App\Members\Soty;
use App\User;
use Auth;
use Hash;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image;

class ProfileController extends Controller
{	

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
     public function profile()
    {   
      return view('members/profile');
    }

    public function editProfile()
    {  
      $soties = Investment::where([
                            ['user_id', '=' , Auth::user()->id],
                            ['status', '!=' ,'Paid']
                                ])->orderBy('created_at', 'desc')->count(); 

      return view('members/editProfile', compact('soties'));
    }

    public function profileSave(Request $request)
    {  
        
        $this->validate($request, [
                    'name' => 'max:255',
                    'phone' => 'regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                    'email' => 'email',
                    'country' => 'max:255',
                    'city' => 'max:255'
                     ]);
        $user = Auth::user();

         // cache the file
        $avatar = $request->file('avatar');

       if ($avatar) {
            // generate a new avatarname. getClientOriginalExtension() for the avatar extension
            $avatarname = $user->id. '_' . time() . '.' . $avatar->getClientOriginalExtension();

            // Get the file from the request
            $requestImage = request()->file('avatar');

            // Get the filepath of the request file (.tmp) and append .jpg
            $requestImagePath = $requestImage->getRealPath() . '.jpg';

            // Modify the image using intervention
            $interventionImage = Image::make($requestImage)->resize(450, 450)->encode('jpg');

            // Save the intervention image over the request image
            $interventionImage->save($requestImagePath);

            // Send the image to file storage
            $url = Storage::putFileAs('public/avatar', new File($requestImagePath), $avatarname);
            
            if ($user->avatar) {
                  Storage::delete('/public/' . $user->avatar);
             }

             User::where('id', '=' ,$user->id)->update([
                'avatar' =>  'avatar/'.$avatarname,
           ]);

       }
        
        

        User::where('id', '=' ,$user->id)->update([
                'name' => $request->name,
                'phone' => $request->phone,
                'email' =>  $request->email,
                'country' =>  $request->country,
                'city' =>  $request->city
           ]);



        return redirect()->route('member.profile')
            ->with('success','Your Profile has been prodified successfully');



      
    }

      public function walletAddresses()
    {  

      $walletAddresses = Auth::user();
      return view('members/walletAddresses' ,compact('walletAddresses'));
    }

     public function walletAddressesSave(Request $request)
    {
        
       
       $user = Auth::user();

       if ($user->address) {
             
          User::where('id', '=' ,$user->id)->update([
                                  'type' => $request->type,
                                  'address' => $request->address,
                             ]);
           $walletAddresses = User::where('id', $user->id)->first();
         
          // return view('members/walletAddresses' ,compact('walletAddresses'));

            return redirect()->route('member.walletAddresses')
            ->with('success','Your Profile has been modified successfully');
       }
        
        $walletAddresses = User::where('id', '=' ,$user->id)->update([
                                  'type' => $request->type,
                                  'address' => $request->address,
                             ]);

         // return view('members/walletAddresses' ,compact('walletAddresses'));
         // 
          return redirect()->route('member.walletAddresses')
            ->with('success','Your Profile has been modified successfully');

     }

  

    public function changePassword(Request $request){


            if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
                // The passwords matches
                 return redirect()->back()->with("error","The Password you have provide does not match your current password!");
            }

            $this->validate($request, [
                    'password' => 'required|confirmed',
            ]);

             //Change Password
            $user = Auth::user();
            $user->password = bcrypt($request->get('password'));
            $user->save();
           

            return redirect()->back()->with("success","Your password has been updated successfully!");
    }

   
}

