<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Members\Address;
use App\Members\Investment;
use App\Members\Soty;
use App\Members\Withdraw;
use App\Traits\SotyTrait;
use Auth;
use Illuminate\Http\Request;

class FinancialController extends Controller
{	

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    

    use SotyTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    
     public function myInvestments()
    {  

      $user = Auth::user();

      $myInvestments = Investment::where([
                                   [ 'user_id','=', $user->id],
                                   [ 'status','=', 'Active'],
                                  ])->orderBy('created_at', 'desc')->get();

      return view('members/myInvestments', compact('myInvestments','user'));
    }


  

    public function casheWithdrawal()
    {  
      $user = Auth::user();

      $lastInvestment = Investment::where([
                                   [ 'user_id','=', $user->id],
                                   [ 'status','=', 'Active'],
                                  ])->orderBy('id', 'desc')->first();
     
      $withdrawals = Withdraw::where([
                                   [ 'user_id','=', $user->id],
                                   [ 'status','=', 'Request'],
                                  ])->orderBy('created_at', 'desc')->get();

      $Confirmedwithdrawals = Withdraw::where([
                                   [ 'user_id','=', $user->id],
                                   [ 'status','=', 'Paid'],
                                  ])->orderBy('created_at', 'desc')->get();


      $numberOfActiveTeamInvestments = $user->soties_number;
      $number_of_investments = $user->petek_number; 
   
      return view('members/casheWithdrawal',compact('user', 'withdrawals', 'Confirmedwithdrawals','lastInvestment','numberOfActiveTeamInvestments','number_of_investments'));
    }
  



    public function casheDepositDelete()
    {  

      $user = Auth::user();
      
       Investment::where([
                    'user_id' => $user->id,
                    'status' => 'Inactive'
                    ])->delete();

       $casheDepositRequest = Investment::where([
                            'user_id' => $user->id,
                            'status' => 'Inactive'
                            ])->orderBy('created_at', 'desc')->first();

      return view('members/casheDeposit', compact('casheDepositRequest'))->with('success', 'Your payment request has been cancelled successfully.');
    }
 



      public function casheDeposit()
    {   


       $user = Auth::user();
       $casheDepositRequest = Investment::where([
                            'user_id' => $user->id,
                            'status' => 'Inactive'
                            ])->orderBy('created_at', 'desc')->first();
      // date('m d Y H:i:s',strtotime('+2 hour +00 minutes',strtotime($casheDepositRequest->created_at))) }} GMT+0200
    
        return view('members/casheDeposit', compact('user','casheDepositRequest'));
    }


    public function casheDepositSend(Request $request)
    {

          $user = Auth::user();


          //Delete all old payment request
          $oldPaymentDemands = Investment::where([
                                    'user_id' => $user->id,
                                    'status' => 'Inactive',
                                    ])->delete();
            

           $addCoinDeposit = new Investment;
                  
           $addCoinDeposit->user_id = $user->id;
           $addCoinDeposit->owner_id = $user->owner_id;
           $addCoinDeposit->petek_number = $this->getNumberOfPetek($request->amount_usd);
          

           $addCoinDeposit->coin_type = $request->coin_type;
           $addCoinDeposit->amount_usd = $request->amount_usd;
           
        if ($request->coin_type == "bitcoin") {
                $addCoinDeposit->amount_crypto = $request->amount_bitcoin;
           }elseif ($request->coin_type == "ether") {
                $addCoinDeposit->amount_crypto = $request->amount_ether;
           }elseif ($request->coin_type == "litecoin") {
                $addCoinDeposit->amount_crypto = $request->amount_litecoin;
           }elseif ($request->coin_type == "dogecoin") {
                $addCoinDeposit->amount_crypto = $request->amount_dogecoin;
           }

           if (!$user->address) {
              return redirect('/member/wallet-addresses')->with('address','Please provide the address information of the the coin you will use for deposit and for withrawals!');
           }
              

           if ($user->type != $request->coin_type) {
              $addressError = "Please update the your address to " . $request->coin_type . " and try again.";
              return redirect('/member/wallet-addresses')->with('addressError', $addressError);
           }


           $addCoinDeposit->save();   
         

       return redirect('/member/cash-deposit-confirmation')->with('success','Your cashe deposite request has been approved. You have 2 hours to perform the payment through the address below!');
      // session(['success' => 'Your cashe deposite request has been approved. You have 2 hours to perform the payment through the address below!']);

      
    }

    // Return cash deposit confirmation page
    public function casheDepositConfirmation()
    { 
       $user = Auth::user();
       $addCoinDeposit = Investment::where([
                            'user_id' => $user->id,
                            'status' => 'Inactive'
                            ])->orderBy('created_at', 'desc')->first();

       if($addCoinDeposit){
             // date('m d Y H:i:s',strtotime('+2 hour +00 minutes',strtotime($addCoinDeposit->created_at))) }} GMT+0200
            return view('members.casheDepositConfirmation', compact('user','addCoinDeposit'));
       }else{

            return redirect('/member/cash-deposit-transactions')->with('error','You do not have any pending payment request');
       }
     
    }

    public function requsetCasheWithdrawal($amount_usd)
    {   
        $user = Auth::user();
        $numberOfSoties = $this->getTheNumberOfSotiesFromAmount($amount_usd);

        $newWithdraw = new Withdraw;

        $newWithdraw->user_id =  $user->id;
        $newWithdraw->petek_number =  $numberOfSoties;
        $newWithdraw->amount_usd =  $amount_usd;
        $newWithdraw->status =  'Request';
        $newWithdraw->save();

       $this->updateDeleteWithdrawSotiesNumber($user->id, $numberOfSoties);



         //  Secondly we have to update the number of petek of the user
         
        $this->updateDeleteWithdrawPetekNumber($user->id, 1);

        //Lastly we have to make active the investment

        return redirect()->back()->with('success','Your Payment Withdrawal Request has been successfully sent');

    }




}
