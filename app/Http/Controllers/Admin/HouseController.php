<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\House;
use App\Model\Admin\House_property;
use App\Model\Admin\Houseimage;
use App\Model\Admin\Property;
use App\Model\Admin\Reservation;
use App\Model\Admin\category;
use App\Model\Admin\ilan;
use App\Model\Admin\ilan_property;
use App\Model\Admin\ilanimage;
use App\Model\Admin\maincategory;
use DB;
use File;
use Illuminate\Http\Request;
use Image;
use Input;
use Redirect;
use Storage;
use Validator;

class HouseController extends Controller
{	
	public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {   

        $houses = House::where('house_active','Active')->orderBy('created_at','DESC')->get();

        return view('admin.house.yonet', compact('houses'));
    }

     public function add()
    {   
        $properties = Property::orderBy('title','ASC')->get();

        return view('admin.house.ekle', compact('properties'));
    }



    public function save(Request $request)
    {   
       
          $newHouse = House::create($request->all());
                

         if ($request->hasFile('main_image')) {
          
            $image = $request->file('main_image');
            $imagename = time().'_main_image' .'.' .  $image->getClientOriginalExtension();
            
            $location = public_path('upload/' . $imagename);
       
            Image::make($image)->resize(750,540)->save($location); 
           
            $newHouse->main_image = $imagename;
        }



           $newHouse->house_no = time();

           $imageSaved = $newHouse->save();


         if ($request->images) {

            $i = 2;

           foreach($request->file('images') as $image){

                  

                    $imagename = time().'_other'. $i .'.' .  $image->getClientOriginalExtension();

                    $location = public_path('upload/' . $imagename);

                    Image::make($image)->resize(750,540)->save($location);

                    $newimage = new Houseimage;

                    $newimage->house_id = $newHouse->id;
                    $newimage->name = $imagename;

                    $newimage->save();

                    $i ++;
                }  
            }




            if ($request->properties) {
                foreach ($request->properties as $property) {
                  
                  $newProp = new House_property;

                
                  $newProp->property_id = $property;
                  $newProp->house_id = $newHouse->id;
                  $newProp->save();

                }
            }


           return redirect()->route('admin.house.all')->with('success','The house has been added successfully');
            
    }





    public function detay($id)
    {   
      
        $house = House::where('id',$id)->first();
        $houseimages = Houseimage::where('house_id',$id)->get();
        $house_properties = House_property::where('house_id',$id)->get();

        return view('admin.house.detaylari', compact('house','houseimages','house_properties'));
    }




    public function getAllReservations()
    {
        $reservations = Reservation::orderBy('created_at','DESC')->get();

        return view('admin.house.reservation', compact('reservations'));
    }



    public function delete($id)
    {  

     
       $post = House::where('id', $id)->first();  


      
       $otherimages = Houseimage::where('house_id', $post->id)->get();

      

       foreach ($otherimages as $otherimage) {
           $image = $otherimage->name;
           $locimage = public_path('upload/' . $image);
           File::delete($locimage);
       }



       $main_image = $post->main_image;
       $loc_main_image = public_path('upload/' . $main_image);

       File::delete($loc_main_image);

       House::where('id',$id)->delete();
       return redirect()->back()->with('success','This house has been successfully deleted');
    }


    public function duzenle($id)
    {   
        $house = House::where('id',$id)->first();
        $houseimages = Houseimage::where('house_id',$id)->get();
        $house_properties = House_property::where('house_id',$id)->get();

        $all_house_properties = Property::get();

        return view('admin.house.edit', compact('house','houseimages','house_properties','all_house_properties'));

    }


     public function deleteimage($id)
    {
       $post = Houseimage::where('id', $id)->first();  

       $one = $post->name;


     
       $locOne = public_path('upload/' . $one);

       File::delete($locOne);
       

       Houseimage::where('id',$id)->delete();

       return redirect()->back()->with('success','Image Deleted successfully');
    }



    public function duzenleYap(Request $request, $id)
    {
        
         $update = House::where('id', $id)->first();  


        if ($update){
                
                DB::table('houses')->where('id', $update->id)->update([

                     'title' => $request->title,
                     'price' => $request->price,
                     'city' => $request->city,
                     'address' => $request->address,
                     'dimension' => $request->dimension,
                     'room_number' => $request->room_number,
                     'hit_system' => $request->hit_system,
                     'bathrom_number' => $request->bathrom_number,
                     'balcon' => $request->balcon,
                     'description' => $request->description,

                  ]);




        if ($request->hasFile('main_image')) {
          
            $image = $request->file('main_image');
            $imagename = time().'_main_image' .'.' .  $image->getClientOriginalExtension();
            
            $location = public_path('upload/' . $imagename);
       
            Image::make($image)->resize(750,540)->save($location); 

          
            $OldImage = public_path('upload/' . $update->main_image);
            File::delete($OldImage);
          
               DB::table('houses')->where('id', $update->id)->update([
                         'main_image' => $imagename,
                       ]);

        }



         if ($request->images) {

            $i = 2;

           foreach($request->file('images') as $image){

                  

                    $imagename = time().'_other'. $i .'.' .  $image->getClientOriginalExtension();

                    $location = public_path('upload/' . $imagename);

                    Image::make($image)->resize(750,540)->save($location);

                    $newimage = new Houseimage;

                    $newimage->house_id = $update->id;
                    $newimage->name = $imagename;

                    $newimage->save();

                    $i ++;
                }  
            }



          if ($request->properties) {

            $ilanozellikler = DB::table('house_properties')->where('house_id',$id)->get();
         
                foreach ($ilanozellikler as $props) {
                  House_property::where('property_id', $props->property_id)->delete();

                }
        


                foreach ($request->properties as $property) {
                  
                  $newProp = new House_property;

                
                  $newProp->property_id = $property;
                  $newProp->house_id = $update->id;
                  $newProp->save();

                }
            }



            return redirect()->route('admin.house.all')->with('success','Your modifications has been saved');
          }

       
  
    }
























    public function ekle()
    {    
        
        $maincategories = DB::table('maincategories')->get();
        $categories = DB::table('categories')->get();
        $emlaks = DB::table('emlaks')->get();
        $emlak_types = DB::table('emlak_types')->orderBy('title_tr','ASC')->get();
        $types = DB::table('types')->orderBy('title_tr','ASC')->get();
        $illers = DB::table('illers')->orderBy('name','ASC')->get();
        $ilcelers = DB::table('ilcelers')->orderBy('name','ASC')->get();

        $properties = DB::table('properties')->orderBy('title_tr','ASC')->get();
        $property_categories = DB::table('property_categories')->orderBy('title_tr','ASC')->get();
       


        return view('admin.house.ekle', compact('maincategories','categories','emlak_types','illers','ilcelers','emlaks','types','properties','property_categories'));
        
    }

    public function ilcelerBul($id)
    {
      $ilceler = DB::table('ilcelers')
                         ->where('il_id', '=', $id)
                         ->orderBy('name','ASC')
                         ->pluck("name","id");

       return json_encode($ilceler);  
    }

      public function ilanturu($id)
    {
      $ilanturu = DB::table('types')
                         ->orderBy('title_tr','ASC')
                        ->pluck("title_tr","id");

       return json_encode($ilanturu);  
    }

    public function ilangrubu($id, $id2)
    {
       $ilceler = DB::table('emlak_types')
                         ->where([
                          'type_id' => $id,
                          'emlak_id' => $id2
                         ])
                         ->orderBy('title_tr','ASC')
                        ->pluck("title_tr","id");

       return json_encode($ilceler);  
    }





    public function activate($id)
    {
   
        $update = ilan::where('id', $id)->first();  
        
        if ($update) {
                        DB::table('ilans')->where('id', $update->id)->update([
                           
                       'urun_aktif'  => 1,
                       

                          ]);

            return redirect('admin/ilan/yonet')->with('success','Değişiklikleriniz başarıyla kaydedildi');
        }else{
            return redirect('admin/ilan/yonet')->with('error','Değişiklikleriniz kaydedilemedi');
           
        }
    }


    public function deactivate($id)
    {
          $update = ilan::where('id', $id)->first();  
        
        if ($update) {
                        DB::table('ilans')->where('id', $update->id)->update([
                           
                       'urun_aktif'  => 0,
                       

                          ]);

            return redirect('admin/ilan/yonet')->with('success','Değişiklikleriniz başarıyla kaydedildi');
        }else{
            return redirect('admin/ilan/yonet')->with('error','Değişiklikleriniz kaydedilemedi');
           
        }
    }



}
