<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Members\Address;
use App\Members\Investment;
use App\Members\Soty;
use App\Members\Withdraw;
use App\User;
use Auth;
use DB;
use Exception;
use Hash;
use Illuminate\Http\Request;
use Mail;

class WithdrawalController extends Controller
{	
	public function __construct()
    {
        $this->middleware('auth:admin');
    }




     public function withdrawalList()
    {
        $paymentList = Withdraw::where('status', 'Request')->orderBy('created_at','DESC')->get();

       
        return view('admin.withdrawal.yonet', compact('paymentList'));
    }


    public function endMonthPaymentList()
    {   
        
        $paymentList = User::where([
                                ['petek_number', '!=', '0']
                            ])->orderBy('created_at','DESC')->get();
       
        return view('admin.withdrawal.endmonthyonet', compact('paymentList'));
    }



     public function confirmedList()
    {
        $confirmedList = Withdraw::where('status', 'Paid')->orderBy('created_at','DESC')->get();
        return view('admin.withdrawal.confirm', compact('confirmedList'));
    }

    
    public function confirmWithdrawal($id)
    {
       
        $investment = Investment::where([
                          'id' => $id,
                        ])->update(['status' => 'Paid']);
        
        Withdraw::where([
                      'investment_id' => $investment->id,
                    ])->update(['status' => 'Paid']);

        return redirect()->back()->with([
                'success' => 'The payment request has been successfully approved!',
            ]);

       
    }

    public function withdrawalConfirmWithdrawal($id)
    {   

        $investment =  Withdraw::where('id', $id)->first();

        Withdraw::where([
                      'user_id' => $investment->user->id,
                    ])->update(['status' => 'Paid']);
           
        return redirect()->back()->with([
                'success' => 'The payment request has been successfully approved!',
            ]);
    }



    public function deletewithdrawal($id)
    {   

        Investment::where('id', $id)
                ->update(['status' => 'Active']);

        return redirect()->back()->with([
                    'success' => 'The payment request confirmation has been cancelled successfully',
                ]);   
    }

    

}
