<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Property_category;
use App\Model\Admin\Property;

use DB;
use Validator;
use Redirect;
use Storage;
use Image;
use File;

class PropertyController extends Controller
{	
	public function __construct()
    {
        $this->middleware('auth:admin');
    }



    public function index()
    {   
        
        $properties = Property::orderBy('title','ASC')->get();

        return view('admin.property.yonet', compact('properties'));
    }
    

    public function save(Request $request)
    {
      
        $post = new Property;

        $post->title = $request->title;
        $result = $post->save();
      
        
        return redirect()->route('admin.property.all')->with('success','The property has been added succssfully');
            
    }

    public function delete($id)
    {  

       property::where('id',$id)->delete();
       return redirect()->back()->with('success','The Property has been successfully deleted');
    }



}
