<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\setting;

use App\Model\Admin\Sliderimage;

use DB;
use Redirect;
use Storage;
use Image;
use File;


class AyarlariController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {   
       $settings = DB::table('settings')->first();
       return view('admin.ayarlari.genelayarlari', compact('settings'));

    }

    public function save(Request $request)
    {
    	
  

            $exists = DB::table('settings')->first();

            if ($exists) {
                   $result = DB::table('settings')->where('id', $exists->id)->update([

    						  'company_name' => $request->company_name,
                  'default_lang' => $request->default_lang,
                  'facebook' => $request->facebook,
                  'instagram' => $request->instagram,
                  'twitter' => $request->twitter,
                  'youtube' => $request->youtube,
                  'linkedin' => $request->linkedin,
                  'googleplus' => $request->googleplus,
                  'phoneone' => $request->phoneone,
                  'phonetwo' => $request->phonetwo,
                  'phonethree' => $request->phonethree,
                  'smtp_host' => $request->smtp_host,
                  'smtp_username' => $request->smtp_username,
                  'smtp_password' => $request->smtp_password,
                  'mailone_address' => $request->mailone_address,
                  'mailone_name' => $request->mailone_name,
                  'mailtwo_address' => $request->mailtwo_address,
                  'mailtwo_name' => $request->mailtwo_name,
                  'seo_baslik_tr' => $request->seo_baslik_tr,
                  'seo_baslik_en' => $request->seo_baslik_en,
                  'seo_baslik_fr' => $request->seo_baslik_fr,
                  'seo_baslik_ar' => $request->seo_baslik_ar,
                  'seo_baslik_es' => $request->seo_baslik_es,
                  'seo_baslik_de' => $request->seo_baslik_de,
                  'seo_baslik_ru' => $request->seo_baslik_ru,
                  'seo_baslik_fa' => $request->seo_baslik_fa,
                  'seo_baslik_pt' => $request->seo_baslik_pt,
                  'seo_keyword_tr' => $request->seo_keyword_tr,
                  'seo_keyword_en' => $request->seo_keyword_en,
                  'seo_keyword_fr' => $request->seo_keyword_fr,
                  'seo_keyword_ar' => $request->seo_keyword_ar,
                  'seo_keyword_es' => $request->seo_keyword_es,
                  'seo_keyword_de' => $request->seo_keyword_de,
                  'seo_keyword_ru' => $request->seo_keyword_ru,
                  'seo_keyword_fa' => $request->seo_keyword_fa,
                  'seo_keyword_pt' => $request->seo_keyword_pt,
                  'seo_description_tr' => $request->seo_description_tr,
                  'seo_description_en' => $request->seo_description_en,
                  'seo_description_fr' => $request->seo_description_fr,
                  'seo_description_ar' => $request->seo_description_ar,
                  'seo_description_es' => $request->seo_description_es,
                  'seo_description_de' => $request->seo_description_de,
                  'seo_description_ru' => $request->seo_description_ru,
                  'seo_description_fa' => $request->seo_description_fa,
                  'seo_description_pt' => $request->seo_description_pt,
                  'copyright_tr' => $request->copyright_tr,
                  'copyright_en' => $request->copyright_en,
                  'copyright_fr' => $request->copyright_fr,
                  'copyright_ar' => $request->copyright_ar,
                  'copyright_es' => $request->copyright_es,
                  'copyright_de' => $request->copyright_de,
                  'copyright_ru' => $request->copyright_ru,
                  'copyright_fa' => $request->copyright_fa,
                  'copyright_pt' => $request->copyright_pt,

                    ]);
            } else {
                
                $settingsave = new setting;

                  $settingsave->company_name = $request->company_name;
                  $settingsave->default_lang = $request->default_lang;
                  $settingsave->facebook = $request->facebook;
                  $settingsave->instagram = $request->instagram;
                  $settingsave->twitter = $request->twitter;
                  $settingsave->youtube = $request->youtube;
                  $settingsave->linkedin = $request->linkedin;
                  $settingsave->googleplus = $request->googleplus;
                  $settingsave->phoneone = $request->phoneone;
                  $settingsave->phonetwo = $request->phonetwo;
                  $settingsave->phonethree = $request->phonethree;
                  $settingsave->smtp_host = $request->smtp_host;
                  $settingsave->smtp_username = $request->smtp_username;
                  $settingsave->smtp_password = $request->smtp_password;
                  $settingsave->mailone_address = $request->mailone_address;
                  $settingsave->mailone_name = $request->mailone_name;
                  $settingsave->mailtwo_address = $request->mailtwo_address;
                  $settingsave->mailtwo_name = $request->mailtwo_name;
                  $settingsave->seo_baslik_tr = $request->seo_baslik_tr;
                  $settingsave->seo_baslik_en = $request->seo_baslik_en;
                  $settingsave->seo_baslik_fr = $request->seo_baslik_fr;
                  $settingsave->seo_baslik_ar = $request->seo_baslik_ar;
                  $settingsave->seo_baslik_es = $request->seo_baslik_es;
                  $settingsave->seo_baslik_de = $request->seo_baslik_de;
                  $settingsave->seo_baslik_ru = $request->seo_baslik_ru;
                  $settingsave->seo_baslik_fa = $request->seo_baslik_fa;
                  $settingsave->seo_baslik_pt = $request->seo_baslik_pt;
                  $settingsave->seo_keyword_tr = $request->seo_keyword_tr;
                  $settingsave->seo_keyword_en = $request->seo_keyword_en;
                  $settingsave->seo_keyword_fr = $request->seo_keyword_fr;
                  $settingsave->seo_keyword_ar = $request->seo_keyword_ar;
                  $settingsave->seo_keyword_es = $request->seo_keyword_es;
                  $settingsave->seo_keyword_de = $request->seo_keyword_de;
                  $settingsave->seo_keyword_ru = $request->seo_keyword_ru;
                  $settingsave->seo_keyword_fa = $request->seo_keyword_fa;
                  $settingsave->seo_keyword_pt = $request->seo_keyword_pt;
                  $settingsave->seo_description_tr = $request->seo_description_tr;
                  $settingsave->seo_description_en = $request->seo_description_en;
                  $settingsave->seo_description_fr = $request->seo_description_fr;
                  $settingsave->seo_description_ar = $request->seo_description_ar;
                  $settingsave->seo_description_es = $request->seo_description_es;
                  $settingsave->seo_description_de = $request->seo_description_de;
                  $settingsave->seo_description_ru = $request->seo_description_ru;
                  $settingsave->seo_description_fa = $request->seo_description_fa;
                  $settingsave->seo_description_pt = $request->seo_description_pt;
                  $settingsave->copyright_tr = $request->copyright_tr;
                  $settingsave->copyright_en = $request->copyright_en;
                  $settingsave->copyright_fr = $request->copyright_fr;
                  $settingsave->copyright_ar = $request->copyright_ar;
                  $settingsave->copyright_es = $request->copyright_es;
                  $settingsave->copyright_de = $request->copyright_de;
                  $settingsave->copyright_ru = $request->copyright_ru;
                  $settingsave->copyright_fa = $request->copyright_fa;
                  $settingsave->copyright_pt = $request->copyright_pt;

                  $result = $settingsave->save();
            }


          if ($result){
                    
                return redirect('/admin/ayarlari/genelayarlari')->with('success','Değişiklikleriniz başarıyla kaydedildi');
                 }else{
                 return redirect('/admin/ayarlari/genelayarlari')->with('error', "Oups, Değişiklikleriniz kaydedilemedi");
            }   
            
    }

    public function slider()
    {
        $slider = DB::table('sliderimages')->get();
        return view('admin.ayarlari.slider', compact('slider'));
    }


    public function saveslider(Request $request)
    {
            $result = 0;

        if ($request->images) {

           foreach($request->file('images') as $image){
                    $imagename = time() .'.' . $image->getClientOriginalExtension();
                    $location = public_path('sliderFotograflari/' . $imagename);

                    Image::make($image)->resize(1700, 850)->save($location);

                    $newimage = new Sliderimage;

                    $newimage->image = $imagename;

                   $result = $newimage->save();
                }  
            }
                  
            if ( $result) {
                return redirect('admin/ayarlari/slider')->with('success','Fotoğraf(lar)ı başarıyla eklendi');
             }else{
                  return redirect('admin/ayarlari/slider')->with('error', "Oups, Fotoğraf(lar)ı eklenemedi");
        }
    }

    public function deletslider($id)
    {
        $image = sliderimage::where('id', $id)->first();  
       $imagename = $image->image;
       $location = public_path('sliderFotograflari/' . $imagename);

       File::delete($location);

       sliderimage::where('id',$id)->delete();
       return redirect('admin/ayarlari/slider')->with('success','Fotoğraf(lar)ı başarıyla silindi');
    }
}
