<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use DB;
use Validator;
use Redirect;
use Storage;
use Image;
use File;

class NewsController extends Controller
{	
	public function __construct()
    {
        $this->middleware('auth:admin');
    }



    public function index()
    {   
        // $posts = DB::table('infos')->orderBy('created_at','DESC')->get();
        return view('admin.haber.yonet');
    }
    
    public function ekle()
    {    
        return view('admin.haber.ekle');
    }

    public function save(Request $request)
    {
       
       $post = new info;

       if ($request->hasFile('image')) {
       
           $image = $request->file('image');
            $imagename = time() .'.' . $image->getClientOriginalExtension();
            $location = public_path('haberFotograflari/' . $imagename);
       
            Image::make($image)->resize(800,600)->save($location);

            $post->image = $imagename;
        }

        $post->title_tr = $request->title_tr;
        $post->title_en = $request->title_en;
        $post->title_fr = $request->title_fr;
        $post->title_ar = $request->title_ar;
        $post->title_es = $request->title_es;
        $post->title_de = $request->title_de;
        $post->title_ru = $request->title_ru;
        $post->title_fa = $request->title_fa;
        $post->title_pt = $request->title_pt;
        $post->slug_tr = $request->slug_tr;
        $post->slug_en = $request->slug_en;
        $post->slug_fr = $request->slug_fr;
        $post->slug_ar = $request->slug_ar;
        $post->slug_es = $request->slug_es;
        $post->slug_de = $request->slug_de;
        $post->slug_ru = $request->slug_ru;
        $post->slug_fa = $request->slug_fa;
        $post->slug_pt = $request->slug_pt;
        $post->text_tr = $request->text_tr;
        $post->text_en = $request->text_en;
        $post->text_fr = $request->text_fr;
        $post->text_ar = $request->text_ar;
        $post->text_es = $request->text_es;
        $post->text_de = $request->text_de;
        $post->text_ru = $request->text_ru;
        $post->text_fa = $request->text_fa;
        $post->text_pt = $request->text_pt;

          
           $result = $post->save();

             if ($result){
                    
                      return redirect('admin/haber/yonet')->with('success','haberi başarıyla kaydedildi');
                 }else{
                 return redirect('admin/haber/ekle')->with('error', "Oups, haberi kaydedilemedi");
            }



            
    }

    public function delete($id)
    {  

       $post = info::where('id', $id)->first();  
       $imagename = $post->image;
       $location = public_path('haberFotograflari/' . $imagename);

       File::delete($location);

       info::where('id',$id)->delete();
       return redirect()->back();
    }

    public function duzenle($id)
    {   
          $update = info::where('id', $id)->first();
          return view('admin.haber.edit', compact('id', 'update'));
        
    }

    public function duzenleYap(Request $request, $id)
    {
        


         $update = info::where('id', $id)->first();  
        

        if ($request->hasFile('image') && $update) {
        
            $image = $request->file('image');

            if ($update->image == null) {
               $imagename = time() .'.' . $image->getClientOriginalExtension();
            }else{
                 $imagename = $update->image;
            }
           
            

            $location = public_path('haberFotograflari/' . $imagename);

           
            
            File::delete($location);

           
            $imagename = time() .'.' . $image->getClientOriginalExtension();

            $location = public_path('haberFotograflari/' . $imagename);

            Image::make($image)->resize(800,600)->save($location);

            DB::table('infos')->where('id', $update->id)->update([ 
             
                       'title_tr'  => $request->title_tr,
                       'title_en'  => $request->title_en,
                       'title_fr'  => $request->title_fr,
                       'title_ar'  => $request->title_ar,
                       'title_es'  => $request->title_es,
                       'title_de'  => $request->title_de,
                       'title_ru'  => $request->title_ru,
                       'title_fa'  => $request->title_fa,
                       'title_pt'  => $request->title_pt,
                       'slug_tr' => $request->slug_tr,
                       'slug_en' => $request->slug_en,
                       'slug_fr' => $request->slug_fr,
                       'slug_ar' => $request->slug_ar,
                       'slug_es' => $request->slug_es,
                       'slug_de' => $request->slug_de,
                       'slug_ru' => $request->slug_ru,
                       'slug_fa' => $request->slug_fa,
                       'slug_pt' => $request->slug_pt,
                       'text_tr' => $request->text_tr,
                       'text_en' => $request->text_en,
                       'text_fr' => $request->text_fr,
                       'text_ar' => $request->text_ar,
                       'text_es' => $request->text_es,
                       'text_de' => $request->text_de,
                       'text_ru' => $request->text_ru,
                       'text_fa' => $request->text_fa,
                       'text_pt' => $request->text_pt,
                       'image' => $imagename


                  ]);

             return redirect('admin/haber/yonet')->with('success','Değişiklikleriniz başarıyla kaydedildi');
            
               }elseif ($update) {
                   $result = DB::table('infos')->where('id', $update->id)->update([
                           
                           'title_tr'  => $request->title_tr,
                           'title_en'  => $request->title_en,
                           'title_fr'  => $request->title_fr,
                           'title_ar'  => $request->title_ar,
                           'title_es'  => $request->title_es,
                           'title_de'  => $request->title_de,
                           'title_ru'  => $request->title_ru,
                           'title_fa'  => $request->title_fa,
                           'title_pt'  => $request->title_pt,
                           'slug_tr' => $request->slug_tr,
                           'slug_en' => $request->slug_en,
                           'slug_fr' => $request->slug_fr,
                           'slug_ar' => $request->slug_ar,
                           'slug_es' => $request->slug_es,
                           'slug_de' => $request->slug_de,
                           'slug_ru' => $request->slug_ru,
                           'slug_fa' => $request->slug_fa,
                           'slug_pt' => $request->slug_pt,
                           'text_tr' => $request->text_tr,
                           'text_en' => $request->text_en,
                           'text_fr' => $request->text_fr,
                           'text_ar' => $request->text_ar,
                           'text_es' => $request->text_es,
                           'text_de' => $request->text_de,
                           'text_ru' => $request->text_ru,
                           'text_fa' => $request->text_fa,
                           'text_pt' => $request->text_pt    
                          ]);

            return redirect('admin/haber/yonet')->with('success','Değişiklikleriniz başarıyla kaydedildi');
        }else{
            return redirect('admin/haber/yonet')->with('error','Değişiklikleriniz kaydedilemedi');
           
        }
        
       
     
       


    }

}
