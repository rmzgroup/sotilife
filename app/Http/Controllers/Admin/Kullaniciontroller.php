<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Members\Investment;
use App\Members\Withdraw;
use App\User;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;

class Kullaniciontroller extends Controller
{	
	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //Rreturn user details
    

    public function checkDetails($id)
    {
       $user = User::where('id', $id)->first();
       $teamUsers = User::where('owner_id', $id)->get();

       $userInvestments = Investment::where([
                                                ['user_id', '=', $id],
                                                ['status', '=', 'Active']
                                            ])->orderBy('created_at','DESC')->get();
       $userWithdraws = Withdraw::where([
                                             ['user_id', '=', $id],
                                             ['status', '=', 'Paid']
                                        ])->orderBy('created_at','DESC')->get();


        return view('admin.kullanici.details', compact('user','userInvestments','teamUsers','userWithdraws'));
    }

    public function changeAdminPassword(Request $request){

            if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
                // The passwords matches
                 return redirect()->back()->with("error","The Password you have provide does not match your current password!");
            }

            $this->validate($request, [
                    'password' => 'required|confirmed',
            ]);

             //Change Password
         
            $user = Auth::guard('admin')->user();
            $user->password = bcrypt($request->get('password'));
            $user->save();
           

            return redirect()->back()->with("success","Your password has been updated successfully!");
    }
}
