<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\article;

use DB;
use Validator;
use Redirect;
use Storage;
use Image;
use File;

class BilgilerController extends Controller
{	
	public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function yonet()
    {   
        $posts = DB::table('articles')->orderBy('created_at','DESC')->get();
        return view('admin.kurumsal.bilgiler.yonet', compact('posts'));
    }
    
    public function save(Request $request)
    {
       
          $post = new article;

          if ($request->hasFile('file')) {
                $file = $request->file('file');
                $filename = $file->getClientOriginalName();
           
                $location = public_path('bilgilerBelgeler/');

                $file->move( $location, $filename);

                $post->file = $filename;
            }



        if ($request->hasFile('image')) {

                  $image = $request->file('image');
                  $imagename = time() .'.' . $image->getClientOriginalExtension();
                  $location = public_path('bilgilerFotograflari/' . $imagename);
             
                  Image::make($image)->resize(800,600)->save($location);

                  $post->image = $imagename;
              }

              $post->title_tr = $request->title_tr;
              $post->title_en = $request->title_en;
              $post->title_fr = $request->title_fr;
              $post->title_ar = $request->title_ar;
              $post->title_es = $request->title_es;
              $post->title_de = $request->title_de;
              $post->title_ru = $request->title_ru;
              $post->title_fa = $request->title_fa;
              $post->title_pt = $request->title_pt;
             
              $post->text_tr = $request->text_tr;
              $post->text_en = $request->text_en;
              $post->text_fr = $request->text_fr;
              $post->text_ar = $request->text_ar;
              $post->text_es = $request->text_es;
              $post->text_de = $request->text_de;
              $post->text_ru = $request->text_ru;
              $post->text_fa = $request->text_fa;
              $post->text_pt = $request->text_pt;
          
              $result = $post->save();

             if ($result){
                    
                      return redirect('admin/bilgiler/yonet')->with('success','bilgiler başarıyla kaydedildi');
                 }else{
                 return redirect('admin/bilgiler/ekle')->with('error', "Oups, bilgiler kaydedilemedi");
            }



            
    }

    public function delete($id)
    {  

       $post = article::where('id', $id)->first();  
       $imagename = $post->image;
       $filename = $post->file;

       $imagelocation = public_path('bilgilerFotograflari/' . $imagename);
       $filelocation = public_path('bilgilerBelgeler/' . $filename);

       File::delete($imagelocation);
       File::delete($filelocation);

       article::where('id',$id)->delete();
       return redirect()->back();
    }

    public function duzenle($id)
    {   
          $update = article::where('id', $id)->first();
          return view('admin.kurumsal.bilgiler.edit', compact('update'));
        
    }

    public function duzenleYap(Request $request, $id)
    {
        
  
     $update = article::where('id', $id)->first();  
        
 

     if ($request->hasFile('file')) {

                $oldfilename = $update->file;
                $oldlocation = public_path('bilgilerBelgeler/' . $oldfilename);
                File::delete($oldlocation);


                $file = $request->file('file');
                $filename = $file->getClientOriginalName();
           
                $location = public_path('bilgilerBelgeler/');

                $file->move( $location, $filename);

            $result =   DB::table('articles')->where('id', $update->id)->update([ 
                     'file' => $filename,
                  ]);
            }


        if ($request->hasFile('image') ) {
        
            $image =    $request->file('image');

            if ($update->image == null) {
               $imagename = time() .'.' . $image->getClientOriginalExtension();
            }else{
                $oldimagename = $update->image;
                $oldlocation = public_path('bilgilerFotograflari/' . $oldimagename);
                File::delete($oldlocation);

                $image =    $request->file('image');
                $imagename = time() .'.' . $image->getClientOriginalExtension();
                $location = public_path('bilgilerFotograflari/' . $imagename);

                Image::make($image)->resize(800,600)->save($location);
            }
           

            $result =  DB::table('articles')->where('id', $update->id)->update([ 
                     'image' => $imagename,
                  ]);
        }

            $resultfinal =  DB::table('articles')->where('id', $update->id)->update([
                       'title_tr'  => $request->title_tr,
                       'title_en'  => $request->title_en,
                       'title_fr'  => $request->title_fr,
                       'title_ar'  => $request->title_ar,
                       'title_es'  => $request->title_es,
                       'title_de'  => $request->title_de,
                       'title_ru'  => $request->title_ru,
                       'title_fa'  => $request->title_fa,
                       'title_pt'  => $request->title_pt,
                  
                       'text_tr' => $request->text_tr,
                       'text_en' => $request->text_en,
                       'text_fr' => $request->text_fr,
                       'text_ar' => $request->text_ar,
                       'text_es' => $request->text_es,
                       'text_de' => $request->text_de,
                       'text_ru' => $request->text_ru,
                       'text_fa' => $request->text_fa,
                       'text_pt' => $request->text_pt,

                  ]);


        if ($resultfinal || $result) {
            return redirect('admin/bilgiler/yonet')->with('success','Değişiklikleriniz başarıyla kaydedildi');
        }else{
            return redirect('admin/bilgiler/yonet')->with('error','Değişiklikleriniz kaydedilemedi');
           
        }
        

    }

}
