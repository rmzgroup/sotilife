<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Members\Soty;
use App\User;
use Auth;
use DB;
use Exception;
use Hash;
use Illuminate\Http\Request;
use Mail;

class UsersController extends Controller
{	
	public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function getAllUsers()
    {
         $users = User::orderBy('created_at','DESC')->get();
        return view('admin.kullanici.yonet', compact('users'));
    }  


    public function delete($id)
    {
         User::where('id',$id)->delete();
         return redirect()->back()->with('success','The user has been deleted successfully');
    }



}
