<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Members\Investment;
use App\Members\Soty;
use App\User;
use Auth;
use DB;
use Exception;
use Hash;
use Illuminate\Http\Request;
use Mail;

use App\Traits\SotyTrait;

class InvestmentController extends Controller
{	

    use SotyTrait;


	public function __construct()
    {
        $this->middleware('auth:admin');
    }

     public function paymentList()
    {
        $paymentList = Investment::where('status', 'Inactive')->orderBy('created_at','DESC')->get();
        return view('admin.soty.yonet', compact('paymentList'));
    }


     public function confirmedList()
    {
         $confirmedList = Investment::where('status', 'Active')->orderBy('created_at','DESC')->get();
        return view('admin.soty.confirm', compact('confirmedList'));
    }

    

    public function confirmPayment($id)
    {
        

        $getSoty = Investment::where('id', $id)->first();

        
        // Firsly we have to update the number of soties of the owner_id
        $numberOfSoties = $getSoty->petek_number;
        $investmentID = $getSoty->id;

        $this->updateSotiesNumber($investmentID, $numberOfSoties);


        // Secondly we have to update the number of petek of the user
        
        $this->updatePetekNumber($investmentID, $numberOfSoties);

        //Add the number of bought petek to the petek dabatabase table
        
        $this->AddPetekToDatabase($investmentID, $numberOfSoties);

        
        
        //Lastly we have to make active the investment
        Investment::where('id', $id)
                   ->update(['status' => 'Active']);

        return redirect()->back()->with([
                    'success' => 'The payment has been successfully approved!']);
       
       
    }

    public function deletePayment($id)
    {
       

         // Firsly we have to update the number of soties of the owner_id
        $getSoty = Investment::where('id', $id)->first();
         
        $numberOfSoties = $getSoty->petek_number;
        $investmentID = $getSoty->id;

        $this->updateDeleteSotiesNumber($investmentID, $numberOfSoties);


         //  Secondly we have to update the number of petek of the user
         
        $this->updateDeletePetekNumber($investmentID, $numberOfSoties);

         //Delete the number of bought petek to the petek dabatabase table
        
        $this->DeletePetekToDatabase($investmentID, $numberOfSoties);

        //Lastly we have to make active the investment
        
         Investment::where('id', $id)
                   ->update(['status' => 'Inactive']);


         return redirect()->back()->with([
                    'success' => 'The payment has been cancelled successfully',
                ]);   
    }

    

}
