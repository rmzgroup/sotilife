<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        $maincategories = DB::table('maincategories')->get();
      
        $categories = DB::table('categories')->get();
        // $products = DB::table('products')->get();
        $advertisements = DB::table('advertisements')->get();
        // $applications = DB::table('applications')->get();
        $news = DB::table('infos')->get();


        $emlaks = DB::table('emlaks')->get();
        $emlakturu = DB::table('types')->get();
        $emlaktipi = DB::table('emlak_types')->get();
        $ilans = DB::table('ilans')->get();
        $ilans_bas = DB::table('ilans')->where('ilan_active','0')->get();
        $users = DB::table('users')->get();
      




        return view('admin.home', compact('news','categories','products','advertisements','applications','maincategories','emlaks','emlakturu','emlaktipi','ilans','users','ilans_bas'));
    }
}
