<?php

use App\Members\Address;
use App\Members\Investment;
use App\Members\Soty;
use App\Members\Team;
use App\Members\Withdraw;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);

         $faker = Faker\Factory::create();

         // for ($i=1; $i < 12; $i++) { 
         // 	Address::create([
		       //  'user_id' => $i,
		       //  'bitcoin_address' => str_random(35),
		       //  'ether_address' => str_random(35),
		       //  'litecoin_address' => str_random(35),
		       //  'dogecoin_address'  =>  str_random(35),
	        // ]);
         // }
        
        for ($i=0; $i < 50; $i++) { 
        	 // Insert into order_product table
	       
	        // Investment::create([
	        // 	'user_id' => rand(1, 11),
	        // 	'owner_id' => rand(1, 11),
	        // 	'soty_id' => rand(1, 50),
	        // 	'status' => $faker->randomElement(['Active', 'Inactive','Active','Active']),
	        // ]);

	        // Team::create([
	        //     'user_id' => rand(1, 11),
	        //     'owner_id' => rand(1, 11),
	        //     // 'status' => $faker->randomElement(['Active', 'Inactive','Active','Active']),
	        // ]);

	        //  Soty::create([
	        // 	'user_id' => rand(1, 11),
	        // 	'coin_type' => $faker->randomElement(['Bitcoin', 'Ether','Litecoin','Dogecoin']),

	        // 	'amount_usd' =>  $faker->randomElement(['50', '100','150','200','250']),
	        // 	'amount_crypto' => $faker->randomFloat(20, 0, 1),
	        // 	'status' => 'Inactive',
	        // ]);


        };

     //     for ($i=0; $i < 10; $i++) { 

	    //     Withdraw::create([
	    //     	'user_id' => rand(1, 11),
	    //     	'investment_id' => rand(1, 50),
	    //     	'status' => $faker->randomElement(['Inactive', 'Active','Refused','Active','Inactive','Inactive',]),
	    //     ]);

	    // }
    
    }
}
