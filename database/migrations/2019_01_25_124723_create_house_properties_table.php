<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('house_properties', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('property_id')->unsigned();
            $table->integer('house_id')->unsigned();

           $table->foreign('property_id')
                  ->references('id')
                  ->on('properties')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('house_id')
                  ->references('id')
                  ->on('houses')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('house_properties');
    }
}
