<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();

            $table->foreign('user_id')->references('id')->on('users')
                  ->onUpdate('cascade')->onDelete('cascade');

            $table->integer('owner_id')->unsigned()->nullable();

            $table->foreign('owner_id')->references('id')->on('users')
                  ->onUpdate('cascade')->onDelete('cascade');

            $table->string('petek_number')->nullable();
            $table->string('amount_usd')->nullable();
            $table->string('coin_type')->nullable();
            $table->string('amount_crypto')->nullable();

            $table->string('status')->nullable()->default('Inactive');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investments');
    }
}
