<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('house_id')->unsigned();
             $table->foreign('house_id')
                  ->references('id')
                  ->on('houses')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->string('name');      
            $table->string('email');      
            $table->string('phone');     
            $table->string('entry');      
            $table->string('out');      
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
