<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('house_no')->nullable();
            $table->integer('price')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('dimension')->nullable();
            $table->string('room_number')->nullable();
            $table->string('hit_system')->nullable();
            $table->string('bathrom_number')->nullable();
            $table->string('balcon')->nullable();
            $table->string('main_image')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('house_active')->default('Active'); 
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
