@extends('admin/app')

@section('pagename','Ayarlar')

@section('head')

@endsection

@section('mainBody')
 <!-- page content -->
            <div class="page-title">
              <div class="title_left">
              <h2>Slider Fotografları <small> gallery design </small></h2>
              </div>

              <div class="title_right">
                <div class="col-md-8 col-sm-8 col-xs-12 form-group pull-right top_search">
                 
                            <form class="form-horizontal" method="POST" action="{{ route('admin.ayarlari.slider.save') }}" enctype="multipart/form-data" required="true">
                                                    {{ csrf_field() }}

                                             
                                   <div class="row" style="background-color: lightblue; margin:20px; padding:20px;">
                               	           <div class="box-body col-md-6">
                                            <label for="title">Resim(ler) Şeç - (1700, 850)</label>
                                              <input type="file" name="images[]" id="image" accept="image/x-png,image/gif,image/jpeg,image/jpg,image/svg,image/png" multiple="multiple" required="true">
                                          </div>
                                                   
                                           <div class="box-footer col-md-6">
                                              <input type="submit" class="btn btn-primary btn-block" value="Kaydet">
                                            </div>
                                   </div>
                               </form>

                </div>
                 
              
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                   
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row">

                      <p>Media gallery design emelents</p>

                 

                            @foreach($slider as $image)
                                 
                                         <div class="col-md-55">
					                        <div class="thumbnail">
					                          <div class="image view view-first">
					                            <img style="width: 100%; display: block;" src="../../sliderFotograflari/{{ $image->image }}" alt="Burak Dölek Diş Ticaret" />
					                            <div class="mask">
					                              <p>Slider Resimler</p>
					                              <div class="tools tools-bottom">
					                                <a href="{{ route('admin.ayarlari.slider.delete', $image->id) }}"><i class="fa fa-times"></i></a>
					                              </div>
					                            </div>
					                          </div>
					                          <div class="caption">
					                            <p>Burak Dölek Diş Ticaret</p>
					                          </div>
					                        </div>
					                      </div>

                                @endforeach
   

                    </div>
                  </div>
                </div>
              </div>
            </div>
       
        <!-- /page content -->
@endsection

@section('footer')

@endsection