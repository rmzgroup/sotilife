@extends('admin/app')

@section('pagename','Ayarlar')
 @section('head')

 @endsection
 
@section('mainBody')

  <!-- page content -->

            <div class="page-title">
              <div class="title_left">
                <h3>Web Sitenin Genel Ayarları</h3>
              </div> 
            </div>
            <div class="clearfix"></div>

		

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>İletişim Bilgileri <small>Burak Dölek Diş Ticaret</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                   <form class="form-horizontal"  method="POST" action="{{ route('admin.ayarlari.genelayarlari.save') }}">

                    	 {{ csrf_field() }}

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Firma Adı     </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="company_name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="company_name" placeholder="Şirketinin adı"  type="text" value="@if($settings) {{ $settings->company_name }} @endif">
                        </div>
                      </div>
                  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Açılış Dili      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="default_lang" class="form-control col-md-7 col-xs-12">
                          	<option value="tr" selected="true">Türkçe</option>
                          	<option value="en">İngilizce</option>
                          	<option value="fr">Fransızca</option>
                          	<option value="ar">Arapça</option>
                          	<option value="es">İspanyol</option>
                          	<option value="de">Almanca</option>
                          	<option value="ru">Rusça</option>
                          	<option value="fa">Farsça</option>
                          	<option value="pt">Portekiz</option>
                          </select>
                        </div>
                      </div>

                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Facebook Sayfası</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="facebook" class="form-control col-md-7 col-xs-12" name="facebook" placeholder="Şirketinin Facebook Sayfası" type="url" value="@if($settings) {{ $settings->facebook }} @endif">
                        </div>
                      </div>

                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">İnstagram Sayfası</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="instagram" class="form-control col-md-7 col-xs-12" name="instagram" placeholder="Şirketinin İnstagram Sayfası" type="url" value="@if($settings) {{ $settings->instagram }} @endif">
                        </div>
                      </div>

                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Twitter Sayfası</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="twitter" class="form-control col-md-7 col-xs-12" name="twitter" placeholder="Şirketinin Twitter Sayfası" type="url" value="@if($settings) {{ $settings->twitter }} @endif">
                        </div>
                      </div>

                        <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Youtube Sayfası</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="youtube" class="form-control col-md-7 col-xs-12" name="youtube" placeholder="Şirketinin youtube Sayfası" type="url"value="@if($settings) {{ $settings->youtube }} @endif">
                        </div>
                      </div>

                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Linkedin Sayfası</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="linkedin" class="form-control col-md-7 col-xs-12" name="linkedin" placeholder="Şirketinin linkedin Sayfası" type="url"value="@if($settings) {{ $settings->linkedin }} @endif">
                        </div>
                      </div>

                        <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Googleplus Sayfası</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="googleplus" class="form-control col-md-7 col-xs-12" name="googleplus" placeholder="Şirketinin googleplus Sayfası" type="url"value="@if($settings) {{ $settings->googleplus }} @endif">
                        </div>
                      </div>
                      <hr>

                   <div class="row">

                   	    <div class="page-title">
			              <div class="title_left">
			                <h3>Telefon Numaraları</h3>
			              </div> 
			            </div>


                   	    <div class="item form-group col-md-4 col-sm-4 col-xs-12 ">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="telephone">1. Telefon Numara    </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="tel" id="telephone" name="phoneone"  data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12" placeholder="Birinci Telefon Numara"value="@if($settings) {{ $settings->phoneone }} @endif">
                        </div>
                      </div>

                       <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="telephone">2. Telefon Numara    </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="tel" id="telephone" name="phonetwo"  data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12" placeholder="İkinci Telefon Numara"value="@if($settings) {{ $settings->phonetwo }} @endif">
                        </div>
                      </div>

                       <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="telephone">3. Telefon Numara    </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="tel" id="telephone" name="phonethree"  data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12" placeholder="İkinci Telefon Numara"value="@if($settings) {{ $settings->phonethree }} @endif">
                        </div>
                      </div>
                   </div>
                   <hr>

                   <div class="row">

                   	    <div class="page-title">
			              <div class="title_left">
			                <h3>SMTP Ayarları</h3>
			              </div> 
			            </div>

                   	   
	                  <div class="item form-group col-md-4 col-sm-4 col-xs-12">
	                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">SMTP Host Adı</label>
	                    <div class="col-md-8 col-sm-8 col-xs-12">
	                      <input id="smtp_host" class="form-control col-md-7 col-xs-12" name="smtp_host" placeholder="SMTP Host Adı" type="text"value="@if($settings) {{ $settings->smtp_host }} @endif">
	                    </div>
	                  </div>

	                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
	                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">SMTP Kullanıcı Adı</label>
	                    <div class="col-md-8 col-sm-8 col-xs-12">
	                      <input id="smtp_username" class="form-control col-md-7 col-xs-12" name="smtp_username" placeholder="SMTP Kullanıcı Adı" type="text"value="@if($settings) {{ $settings->smtp_username }} @endif">
	                    </div>
	                  </div>
	                
	                  <div class="item form-group col-md-4 col-sm-4 col-xs-12">
	                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">SMTP Parola</label>
	                    <div class="col-md-8 col-sm-8 col-xs-12">
	                      <input id="smtp_password" class="form-control col-md-7 col-xs-12" name="smtp_password" placeholder="SMTP Parola" type="text"value="@if($settings) {{ $settings->smtp_password }} @endif">
	                    </div>
	                  </div>

                   </div>
                  <hr>
                
       			  <div class="row">

       			  	  <div class="page-title">
			              <div class="title_left">
			                <h3>E-Posta Adres Bilgileri</h3>
			              </div> 
			            </div>

       			  <div class="item form-group col-md-3 col-sm-3 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="email">1. E-Posta</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input type="email" id="mailone_address" name="mailone_address" class="form-control col-md-7 col-xs-12" placeholder="Birinci e-posta"value="@if($settings) {{ $settings->mailone_address }} @endif">
                    </div>
                  </div>

                  <div class="item form-group col-md-3 col-sm-43 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">1. E-Posta Adı </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="mailone_name" class="form-control col-md-7 col-xs-12" data-validate-length-range="8" data-validate-words="2" name="mailone_name" placeholder="1. E-Posta Adı "  type="text"value="@if($settings) {{ $settings->mailone_name }} @endif">
                    </div>
                  </div>
              
                  
                  <div class="item form-group col-md-3 col-sm-3 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="email">2. E-Posta</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input type="email" id="mailtwo_address" name="mailtwo_address" class="form-control col-md-7 col-xs-12" placeholder="İkinci e-posta"value="@if($settings) {{ $settings->mailtwo_address }} @endif">
                    </div>


                  </div>

                   <div class="item form-group col-md-3 col-sm-3 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">2. E-Posta Adı </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="mailtwo_name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="mailtwo_name" placeholder="2. E-Posta Adı "  type="text"value="@if($settings) {{ $settings->twitter }} @endif">
                    </div>
                  </div>
              
       			  </div>

 				<hr>
                

                <div class="row">

                	  <div class="page-title">
			              <div class="title_left">
			                <h3>SEO Baslik</h3>
			              </div> 
			            </div>
                	
            	   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">TR SEO baslik</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="seo_baslik_tr" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="seo_baslik_tr" placeholder="SEO Başlığı"  type="text"value="@if($settings) {{ $settings->seo_baslik_tr }} @endif">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">EN SEO baslik</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="seo_baslik_en" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="seo_baslik_en" placeholder="SEO Başlığı"  type="text"value="@if($settings) {{ $settings->seo_baslik_en }} @endif">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">FR SEO baslik</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="seo_baslik_fr" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="seo_baslik_fr" placeholder="SEO Başlığı"  type="text"value="@if($settings) {{ $settings->seo_baslik_fr }} @endif">
                    </div>
                  </div>

				   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">AR SEO baslik</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="seo_baslik_ar" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="seo_baslik_ar" placeholder="SEO Başlığı"  type="text"value="@if($settings) {{ $settings->seo_baslik_ar }} @endif">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">ES SEO baslik</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="seo_baslik_es" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="seo_baslik_es" placeholder="SEO Başlığı"  type="text"value="@if($settings) {{ $settings->seo_baslik_es }} @endif">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">DE SEO baslik</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="seo_baslik_de" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="seo_baslik_de" placeholder="SEO Başlığı"  type="text"value="@if($settings) {{ $settings->seo_baslik_de }} @endif">
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">RU SEO baslik</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="seo_baslik_ru" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="seo_baslik_ru" placeholder="SEO Başlığı"  type="text"value="@if($settings) {{ $settings->seo_baslik_ru }} @endif">
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">FA SEO baslik</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="seo_baslik_fa" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="seo_baslik_fa" placeholder="SEO Başlığı"  type="text"value="@if($settings) {{ $settings->seo_baslik_fa }} @endif">
                    </div>
                  </div>

				<div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">PT SEO baslik</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="seo_baslik_pt" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="seo_baslik_pt" placeholder="SEO Başlığı"  type="text"value="@if($settings) {{ $settings->seo_baslik_pt }} @endif">
                    </div>
                  </div>
                </div>

				<hr>

				  <div class="row">

				  	  <div class="page-title">
			              <div class="title_left">
			                <h3>SEO Ana Kelimeler</h3>
			              </div> 
			            </div>
                	
                	
            	   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">TR SEO Ana Kelimeler</label>
                      <textarea id="seo_keyword_tr" class="form-control col-md-7 col-xs-12" name="seo_keyword_tr" placeholder="SEO Ana Kelimeler"  rows="3">@if($settings) {{ $settings->seo_keyword_tr }} @endif</textarea>
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">EN SEO Ana Kelimeler</label>
                      <textarea id="seo_keyword_en" class="form-control col-md-7 col-xs-12" name="seo_keyword_en" placeholder="SEO Ana Kelimeler"  rows="3">@if($settings) {{ $settings->seo_keyword_en }} @endif</textarea>
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">FR SEO Ana Kelimeler</label>
                      <textarea id="seo_keyword_fr" class="form-control col-md-7 col-xs-12" name="seo_keyword_fr" placeholder="SEO Ana Kelimeler"  rows="3">@if($settings) {{ $settings->seo_keyword_fr }} @endif</textarea>
                    </div>
                  </div>

				   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">AR SEO Ana Kelimeler</label>
                      <textarea id="seo_keyword_ar" class="form-control col-md-7 col-xs-12" name="seo_keyword_ar" placeholder="SEO Ana Kelimeler"  rows="3">@if($settings) {{ $settings->seo_keyword_ar }} @endif</textarea>
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">ES SEO Ana Kelimeler</label>
                      <textarea id="seo_keyword_es" class="form-control col-md-7 col-xs-12" name="seo_keyword_es" placeholder="SEO Ana Kelimeler"  rows="3">@if($settings) {{ $settings->seo_keyword_es }} @endif</textarea>
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <label class="control-label" for="name">DE SEO Ana Kelimeler</label>
                      <textarea id="seo_keyword_de" class="form-control col-md-7 col-xs-12" name="seo_keyword_de" placeholder="SEO Ana Kelimeler"  rows="3">@if($settings) {{ $settings->seo_keyword_de }} @endif</textarea>
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <label class="control-label" for="name">RU SEO Ana Kelimeler</label>
                      <textarea id="seo_keyword_ru" class="form-control col-md-7 col-xs-12" name="seo_keyword_ru" placeholder="SEO Ana Kelimeler"  rows="3">@if($settings) {{ $settings->seo_keyword_ru }} @endif</textarea>
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">FA SEO Ana Kelimeler</label>
                      <textarea id="seo_keyword_fa" class="form-control col-md-7 col-xs-12" name="seo_keyword_fa" placeholder="SEO Ana Kelimeler"  rows="3">@if($settings) {{ $settings->seo_keyword_fa }} @endif</textarea>
                    </div>
                  </div>

				<div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">PT SEO Ana Kelimeler</label>
                      <textarea id="seo_keyword_pt" class="form-control col-md-7 col-xs-12" name="seo_keyword_pt" placeholder="SEO Ana Kelimeler"  rows="3">@if($settings) {{ $settings->seo_keyword_pt }} @endif</textarea>
                    </div>
                  </div>
                </div>

                <hr>

                 <div class="row">

                 	  	  <div class="page-title">
			              <div class="title_left">
			                <h3>SEO Ana Açıklama</h3>
			              </div> 
			            </div>
                	
                	
            	   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">TR SEO Açıklama</label>
                      <textarea id="seo_description_tr" class="form-control col-md-7 col-xs-12" name="seo_description_tr" placeholder="SEO Açıklama"  rows="5">@if($settings) {{ $settings->seo_description_tr }} @endif</textarea>
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                  
                    <div class="col-md-12 col-sm-12 col-xs-12">
                       <label class="control-label" for="name">EN SEO Açıklama</label>
                        <textarea id="seo_description_en" class="form-control col-md-7 col-xs-12" name="seo_description_en" placeholder="SEO Açıklama"  rows="5">@if($settings) {{ $settings->seo_description_en }} @endif</textarea>
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                  
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <label class="control-label" for="name">FR SEO Açıklama</label>
                      <textarea id="seo_description_fr" class="form-control col-md-7 col-xs-12" name="seo_description_fr" placeholder="SEO Açıklama"  rows="5">@if($settings) {{ $settings->seo_description_fr }} @endif</textarea>
                    </div>
                  </div>

				   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                   
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <label class="control-label" for="name">AR SEO Açıklama</label>
                      <textarea id="seo_description_ar" class="form-control col-md-7 col-xs-12" name="seo_description_ar" placeholder="SEO Açıklama"  rows="5">@if($settings) {{ $settings->seo_description_ar }} @endif</textarea>
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                   
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <label class="control-label" for="name">ES SEO Açıklama</label>
                      <textarea id="seo_description_es" class="form-control col-md-7 col-xs-12" name="seo_description_es" placeholder="SEO Açıklama"  rows="5">@if($settings) {{ $settings->seo_description_es }} @endif</textarea>
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">DE SEO Açıklama</label>
                      <textarea id="seo_description_de" class="form-control col-md-7 col-xs-12" name="seo_description_de" placeholder="SEO Açıklama"  rows="5">@if($settings) {{ $settings->seo_description_de }} @endif</textarea>
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                  
                    <div class="col-md-12 col-sm-12 col-xs-12">
                       <label class="control-label" for="name">RU SEO Açıklama</label>
                      <textarea id="seo_description_ru" class="form-control col-md-7 col-xs-12" name="seo_description_ru" placeholder="SEO Açıklama"  rows="5">@if($settings) {{ $settings->seo_description_ru }} @endif</textarea>
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                   
                    <div class="col-md-12 col-sm-12 col-xs-12">
                     <label class="control-label" for="name">FA SEO Açıklama</label>
                      <textarea id="seo_description_fa" class="form-control col-md-7 col-xs-12" name="seo_description_fa" placeholder="SEO Açıklama"  rows="5">@if($settings) {{ $settings->seo_description_fa }} @endif</textarea>
                    </div>
                  </div>

				<div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    	 <label class="control-label" for="name">PT SEO Açıklama</label>
                      <textarea id="seo_description_pt" class="form-control col-md-7 col-xs-12" name="seo_description_pt" placeholder="SEO Açıklama"  rows="5">@if($settings) {{ $settings->seo_description_pt }} @endif</textarea>
                    </div>
                  </div>
                </div>

                <hr>

                <div class="row">

            		  <div class="page-title">
		              <div class="title_left">
		                <h3>Telif Hakkı</h3>
		              </div> 
		            </div>
            	
                	
            	   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">TR Telif Hakkı</label>
                      <textarea id="copyright_tr" class="form-control col-md-7 col-xs-12" name="copyright_tr" placeholder="Telif Hakkı"  rows="3">@if($settings) {{ $settings->copyright_tr }} @endif</textarea>
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">EN Telif Hakkı</label>
                      <textarea id="copyright_en" class="form-control col-md-7 col-xs-12" name="copyright_en" placeholder="Telif Hakkı"  rows="3">@if($settings) {{ $settings->copyright_en }} @endif</textarea>
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">FR Telif Hakkı</label>
                      <textarea id="copyright_fr" class="form-control col-md-7 col-xs-12" name="copyright_fr" placeholder="Telif Hakkı"  rows="3">@if($settings) {{ $settings->copyright_fr }} @endif</textarea>
                    </div>
                  </div>

				   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">AR Telif Hakkı</label>
                      <textarea id="copyright_ar" class="form-control col-md-7 col-xs-12" name="copyright_ar" placeholder="Telif Hakkı"  rows="3">@if($settings) {{ $settings->copyright_ar }} @endif</textarea>
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">ES Telif Hakkı</label>
                      <textarea id="copyright_es" class="form-control col-md-7 col-xs-12" name="copyright_es" placeholder="Telif Hakkı"  rows="3">@if($settings) {{ $settings->copyright_es }} @endif</textarea>
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <label class="control-label" for="name">DE Telif Hakkı</label>
                      <textarea id="copyright_de" class="form-control col-md-7 col-xs-12" name="copyright_de" placeholder="Telif Hakkı"  rows="3">@if($settings) {{ $settings->copyright_de }} @endif</textarea>
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <label class="control-label" for="name">RU Telif Hakkı</label>
                      <textarea id="copyright_ru" class="form-control col-md-7 col-xs-12" name="copyright_ru" placeholder="Telif Hakkı"  rows="3">@if($settings) {{ $settings->copyright_ru }} @endif</textarea>
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">FA Telif Hakkı</label>
                      <textarea id="copyright_fa" class="form-control col-md-7 col-xs-12" name="copyright_fa" placeholder="Telif Hakkı"  rows="3">@if($settings) {{ $settings->copyright_fa }} @endif</textarea>
                    </div>
                  </div>

				<div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label" for="name">PT Telif Hakkı</label>
                      <textarea id="copyright_pt" class="form-control col-md-7 col-xs-12" name="copyright_pt" placeholder="Telif Hakkı"  rows="3">@if($settings) {{ $settings->copyright_pt }} @endif</textarea>
                    </div>
                  </div>
                </div>



                      <div class="ln_solid"></div>
                      <div class="form-group row">
                        <div class="col-md-2 col-md-offset-1">
                             <a href='/admin/anasayfa' class="btn btn-danger">Geri Dön</a>
                        </div>
                         <div class="col-md-6">
                          <button type="submit" class="btn btn-success btn-block">Kaydet</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
  









  <!-- /page content -->
@endsection

@section('footer')
  
@endsection