@extends('admin/app')
@section('pagename','Ürünler')
@section('head')
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/css/prism.css') }}">


<style type="text/css" media="screen">
  .stepContainer{
    height: 105px !important;
  }
  .summit-but{
    margin-top: 10px;
    text-align: right;
  }
   .summit-but button{
    background: green;
    color: white;
    padding: 8px 15px;
  }

  .actionBar{
    margin-top: -155px;
    text-align: left;
  }
</style>


@endsection

@section('mainBody')

  <!-- page content -->
 <section class="content">
                 <!-- page content -->
        <div class="row" role="main">
         
         
            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Enter a house <small>Sotylife</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <!-- Smart Wizard -->
                    <div id="wizard" class="form_wizard wizard_horizontal">
                      <ul class="wizard_steps">
                        <li>
                          <a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">1. Step<br/><small>House General Information</small></span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">2. Step<br/><small>Chose House Images</small></span>
                          </a>
                        </li>

                        <li>
                          <a href="#step-3">
                            <span class="step_no">3</span>
                            <span class="step_descr">3. Step<br/><small>Add House Properties</small></span>
                          </a>
                        </li>

                        <li>
                          <a href="#step-4">
                            <span class="step_no">4</span>
                            <span class="step_descr">4. Step<br/><small>Address Information</small></span>
                          </a>
                        </li>

                        <li>
                          <a href="#step-5">
                            <span class="step_no">5</span>
                            <span class="step_descr">5. Step<br/><small>House Description</small></span>
                          </a>
                        </li>

                      </ul>

                        <form role="form" class="form-horizontal form-label-left" action="{{ route('admin.house.add.save') }}" method="post" enctype="multipart/form-data">

          
                        {{ csrf_field() }}

                  


                      <div id="step-1">
                          
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="site_adi">House Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="text" id="title" name="title" value="{{ old('title') }}" class="form-control col-md-7 col-xs-12" placeholder="Name of the house">
                            </div>
                          </div>

                               <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="site_adi">Price Per Day <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-4 col-xs-8">
                                  <input type="number" id="price" name="price" value="{{ old('price') }}" class="form-control col-md-7 col-xs-12" placeholder="Price of the House in USD">
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-4">
                                  <input type="text" value="USD" class="form-control col-md-7 col-xs-12" disabled="true">
                                </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dimension ">House Dimension <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8">
                              <input type="number" id="dimension" name="dimension" value="{{ old('dimension') }}" class="form-control col-md-7 col-xs-12" placeholder="Dimension (m2)">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-4">
                              <input type="text" value="m2" class="form-control col-md-7 col-xs-12" disabled="true">
                            </div>
                          </div>

                             <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_number">Number of Rooms <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                          
                               <select class="form-control" name="room_number" >
                                    <option selected="true">Room Number</option>
                                    <option value="1+1">1+1</option>
                                    <option value="2+0">2+0</option>
                                    <option value="2+1">2+1</option>
                                    <option value="2+2">2+2</option>
                                    <option value="3+1">3+1</option>
                                    <option value="3+2">3+2</option>
                                    <option value="4+1">4+1</option>
                                    <option value="4+2">4+2</option>
                                    <option value="4+3">4+3</option>
                                    <option value="4+4">4+4</option>
                                    <option value="5+1">5+1</option>
                                    <option value="5+2">5+2</option>
                                    <option value="5+3">5+3</option>
                                    <option value="5+4">5+4</option>
                                    <option value="5 and Above">5 Üzeri</option>
                                </select>
                            </div>
                          </div>


                             <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hit_system">Hit System <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="text" id="hit_system" name="hit_system" value="{{ old('hit_system') }}" class="form-control col-md-7 col-xs-12" placeholder="House Hitting System">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bathrom_number">Bathroom Number <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="number" id="bathrom_number" name="bathrom_number" value="{{ old('bathrom_number') }}" class="form-control col-md-7 col-xs-12" placeholder="Bathroom Number">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="balkon">Balcon <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                               <select name="balcon" class="form-control">
                                      <option  selected="true" value="">Is there a balcom?</option>
                                      <option value="Yes">Yes</option>
                                      <option value="No">No</option>
                                </select>
                            </div>
                          </div>

                      </div>

                          <div id="step-2">
                            

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="main_image">Main Image - (750, 540)<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="file" name="main_image" id="main_image" accept="image/x-png,image/gif,image/jpeg,image/jpg,image/svg,image/png">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="images">Other Images - (750, 540)<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="file" name="images[]" id="images" multiple="multiple" accept="image/x-png,image/gif,image/jpeg,image/jpg,image/svg,image/png">
                            </div>
                          </div>

                      
                          </div>

                       <div id="step-3">
                          <div class="row">
                                <label class="col-md-12 col-sm-12 col-xs-12"><strong style="color: green; padding-top: 7px; margin-bottom: 0">Propeties</strong></label>
                          </div>
                           <div class="form-group">
                        

                             @foreach($properties as $property)
                                  <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" class="flat" name="properties[]" value="{{ $property->id }}"> 
                                        {{ $property->title }}
                                      </label>
                                    </div>
                                  </div>
                             @endforeach  

                          </div>

                          <hr>  

                        

                          </div>

                              <div id="step-4">


                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">City<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select name="city" class="form-control">
                                      <option  selected="true" value="">Select the City</option>
                                        <option value="Avtonomna Respublika Krym">Avtonomna Respublika Krym</option>    
                                        <option value="Cherkaska">Cherkaska</option> 
                                        <option value="Chernihivska">Chernihivska</option>  
                                        <option value="Chernivetska">Chernivetska</option>  
                                        <option value="Dnipropetrovska">Dnipropetrovska</option>   
                                        <option value="Donetska">Donetska</option>  
                                        <option value="Ivano-Frankivska">Ivano-Frankivska</option>  
                                        <option value="Kharkivska">Kharkivska</option>    
                                        <option value="Khersonska">Khersonska</option>    
                                        <option value="Khmelnytska">Khmelnytska</option>   
                                        <option value="Kirovohradska">Kirovohradska</option> 
                                        <option value="Kyiv">Kyiv</option> 
                                        <option value="Kyivska">Kyivska</option>   
                                        <option value="Luhanska">Luhanska</option>  
                                        <option value="Lvivska">Lvivska</option>   
                                        <option value="Mykolaivska">Mykolaivska</option>   
                                        <option value="Odeska">Odeska</option>    
                                        <option value="Poltavska">Poltavska</option> 
                                        <option value="Rivnenska">Rivnenska</option> 
                                        <option value="Sevastopol">Sevastopol</option>   
                                        <option value="Sumska">Sumska</option>    
                                        <option value="Ternopilska">Ternopilska</option>   
                                        <option value="Vinnytska">Vinnytska</option> 
                                        <option value="Volynska">Volynska</option>  
                                        <option value="Zakarpatska">Zakarpatska</option>   
                                        <option value="Zaporizka">Zaporizka</option> 
                                        <option value="Zhytomyrska">Zhytomyrska</option>   
                                </select>
                            </div>
                          </div>

                             <div class="form-group">
                               <h2>House Address</h2>

                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">House Address<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <textarea name="address" rows="3" class="form-control col-md-7 col-xs-12" value="{{ old('address') }}" placeholder="Enter the full address of the house"></textarea>
                            </div>
                          </div>

                      </div>


                         <div id="step-5">
                             <div class="row">

                               <div class="col-md-12">
                                <div class="x_panel">
                                  <div class="x_title">
                                    <h2>House Description<small>text</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                      </li>
                                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="x_content">
                                    
                                    <div class="box-body pad">
                                     <textarea name="description" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor1"> {{ old('description') }}</textarea>
                                   </div>
                                          
                                  </div>
                                </div>
                              </div>

                       
                          </div>
                          </div>
                           
                               <div class="summit-but">
                                <button type="submit" class="btn btn-success">Save</button>
                              </div>
                                
                      </form>

                    </div>
                    <!-- End SmartWizard Content -->

                  </div>
                </div>
              </div>
            </div>
       
        </div>
        <!-- /page content -->
    
  </section>
  
@endsection

@section('footer')

  @include('ajax/il_ilce')
  @include('ajax/ilanturu')
  @include('ajax/ilan_grubu')

<!-- jQuery Smart Wizard -->
    <script src="{{ asset('admin/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js') }}"></script>


<!-- bootstrap-wysiwyg -->
<script src="{{ asset('admin/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>

<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor1');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>


@endsection