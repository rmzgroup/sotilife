@extends('admin/app')
@section('pagename','House Details')
@section('head')

@endsection

@section('mainBody')


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <h2>{{ $house->title }}</h2>
                    <div class="clearfix"></div>
                  </div>
                    <div class="x_content">


      <div class="col-md-8 col-sm-12 col-md-offset-2 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>{{ $house->title }}</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <ul class="list-unstyled timeline">
                    
                     <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>House Code</strong></a></h2><br> 
                          <p class="excerpt">{{ $house->house_no }}
                          </p>
                        </div>
                      </div>
                    </li>
                  
                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>House Name</strong></a></h2><br> 
                          <p class="excerpt">{{ $house->title }}
                          </p>
                        </div>
                      </div>
                    </li>

                     <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>House Daily Amount</strong></a></h2><br> 
                          <p class="excerpt">{{ $house->price }} USD
                          </p>
                        </div>
                      </div>
                    </li>

                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>City</strong></a></h2><br> 
                          <p class="excerpt">{{ $house->city }}
                          </p>
                        </div>
                      </div>
                    </li>

                     <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>House Address</strong></a></h2><br> 
                          <p class="excerpt">{{ $house->address }}
                          </p>
                        </div>
                      </div>
                    </li>


                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Size</strong></a></h2><br> 
                          <p class="excerpt">{{ $house->dimension }} m2
                          </p>
                        </div>
                      </div>
                    </li>

                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Room Number</strong></a></h2><br> 
                          <p class="excerpt">{{ $house->room_number }}
                          </p>
                        </div>
                      </div>
                    </li>

             

                     <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Hitting System</strong></a></h2><br> 
                          <p class="excerpt">{{ $house->hit_system }}
                          </p>
                        </div>
                      </div>
                    </li>

                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Bathroom Number</strong></a></h2><br> 
                          <p class="excerpt">{{ $house->bathrom_number }}
                          </p>
                        </div>
                      </div>
                    </li>

                      <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Is there a balcon?</strong></a></h2><br> 
                          <p class="excerpt">{{ $house->balcon }}
                          </p>
                        </div>
                      </div>
                    </li>


              
                   <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Main Picture</strong></a></h2><br> 
                          <p class="excerpt" style="padding-top: 15px">
                           <img src="../../.../../../../upload/{{ $house->main_image }}" alt="" class="img-responsive" style="padding-top: 5px">
                         
                          </p>
                        </div>
                      </div>
                    </li>

                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Other Images</strong></a></h2><br> 
                        </div>

                               <div class="row" style="padding-top: 15px">
                            
                            @foreach($houseimages as $image)

                                   <div class="col-sm-4">
                                      <img src="../../.../../../../upload/{{ $image->name }}" alt="" class="img-responsive" style="padding-top: 10px">
                                  </div>
                              
                            @endforeach


                             </div>



                      </div>
                    </li>
                    
                     <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>House Properties</strong></a></h2><br> 
                        </div>

                               <div class="row" style="padding-top: 15px">
                            
                                     <div class="form-group">
                                       @foreach($house_properties as $property)
                                            <div class="col-md-3 col-sm-3 col-xs-6">
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox" class="flat" name="properties[]" value="{{ $property->id }}" checked="true" disabled> {{ App\Model\Admin\Property::find($property->property_id)->title }}
                                                </label>
                                              </div>
                                            </div>
                                       @endforeach  
                                    </div>

                                    <hr>  

                               




                             </div>



                      </div>
                    </li>


                 
                     <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>House Description</strong></a></h2><br> 
                          <p class="excerpt">
                             {!! $house->description !!}
                          </p>
                        </div>
                      </div>
                    </li>
            
             
                  </ul>


                    <div class="col-md-12">
                    <div class="ln_solid"></div>
                          <div class="form-group row">
                            <div class="col-md-2 col-md-offset-1">
                               <a href='{{ route('admin.house.all') }}' class="btn btn-danger">Return to the List</a>
                            </div>
                          </div>
                  </div>


                </div>
              </div>
            </div>
       
           </div>
        </div>
      </div>
    </div>

@endsection

@section('footer')


@endsection