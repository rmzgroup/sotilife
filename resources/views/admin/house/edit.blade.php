@extends('admin/app')
@section('pagename','Edit the House')
@section('head')
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/css/prism.css') }}">

@endsection

@section('mainBody')

  <!-- page content -->

            <div class="page-title">
              <div class="title_left">
               
              </div> 
            </div>
            <div class="clearfix"></div>

    

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-left panel_toolbox">
                      <li> <h3>{{ $house->ilan_adi }} <strong> Edit the House</strong></h3></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                     <form role="form" action="{{ route('admin.house.duzenle.save', $house->id) }}" method="post" name="edit" enctype="multipart/form-data">

          
                      {{ csrf_field() }}
                        
                          <div id="row">
                      
                         <div class="row">
                          <label>Main Image</label>
                           <p class="excerpt" style="padding-top: 15px">
                             <img src="../../.../../../../upload/{{ $house->main_image }}" alt="" class="img-responsive" style="padding-top: 5px; border-radius: 25%" width="220px">
                            </p>
                           <label for="title">Main Image Change</label>
                          <input type="file" name="main_image" id="main_image" accept="image/x-png,image/gif,image/jpeg,image/jpg,image/svg,image/png">
                          <hr>
                        </div>

                       <div class="row">
                           <label for="title">Other Images</label>
                           <div class="row">
                             @foreach($houseimages as $image)
                               <div class="col-md-2">
                                  <p class="excerpt" style="padding-top: 15px">
                                   <img src="../../../../../../../../upload/{{ $image->name }}" alt="" class="img-responsive" style="padding-top: 5px; border-radius: 10%" width="220px">
                                  </p>
                                  <a href="{{ route('admin.house.deleteimage',$image->id) }}" class="btn btn-danger btn-block"><span class="glyphicon glyphicon-trash"></span> Delete</a>
                                </div>
                              @endforeach
                           </div>
                        
                            <label for="title">Add other images</label>
                            <input type="file" name="images[]" id="images" multiple="multiple" accept="image/x-png,image/gif,image/jpeg,image/jpg,image/svg,image/png">
                      </div>
                      
                            <hr>

                        <div class="form-group">

                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <label class="control-label" for="ulke">House ID<span class="required">*</span></label>
                              <input type="text"  class="form-control col-md-7 col-xs-12" value="{{ $house->house_no }}"  disabled="true">
                            </div>
                          </div>


                            <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">City<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select name="city" class="form-control">
                                      <option  selected="true" value="{{ $house->city }}">{{ $house->city }}</option>
                                        <option value="Avtonomna Respublika Krym">Avtonomna Respublika Krym</option>    
                                        <option value="Cherkaska">Cherkaska</option> 
                                        <option value="Chernihivska">Chernihivska</option>  
                                        <option value="Chernivetska">Chernivetska</option>  
                                        <option value="Dnipropetrovska">Dnipropetrovska</option>   
                                        <option value="Donetska">Donetska</option>  
                                        <option value="Ivano-Frankivska">Ivano-Frankivska</option>  
                                        <option value="Kharkivska">Kharkivska</option>    
                                        <option value="Khersonska">Khersonska</option>    
                                        <option value="Khmelnytska">Khmelnytska</option>   
                                        <option value="Kirovohradska">Kirovohradska</option> 
                                        <option value="Kyiv">Kyiv</option> 
                                        <option value="Kyivska">Kyivska</option>   
                                        <option value="Luhanska">Luhanska</option>  
                                        <option value="Lvivska">Lvivska</option>   
                                        <option value="Mykolaivska">Mykolaivska</option>   
                                        <option value="Odeska">Odeska</option>    
                                        <option value="Poltavska">Poltavska</option> 
                                        <option value="Rivnenska">Rivnenska</option> 
                                        <option value="Sevastopol">Sevastopol</option>   
                                        <option value="Sumska">Sumska</option>    
                                        <option value="Ternopilska">Ternopilska</option>   
                                        <option value="Vinnytska">Vinnytska</option> 
                                        <option value="Volynska">Volynska</option>  
                                        <option value="Zakarpatska">Zakarpatska</option>   
                                        <option value="Zaporizka">Zaporizka</option> 
                                        <option value="Zhytomyrska">Zhytomyrska</option>   
                                </select>
                            </div>
                          </div>


                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <label class="control-label" for="address">Address<span class="required">*</span></label>
                              <textarea name="address" rows="3" class="form-control col-md-7 col-xs-12"  placeholder="House Address">{{ $house->address }}</textarea>
                            </div>
                          </div>


                      </div>

                      <hr>

                      <div class="row">
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <label class="control-label" for="title">Name <span class="required">*</span></label>
                              <input type="text" id="title" name="title" value="{{ $house->title }}" class="form-control col-md-7 col-xs-12" placeholder="House Name">
                            </div>
                          </div>

                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-8">
                               <label class="control-label" for="price">House Price (PER DAY) USD<span class="required">*</span></label>
                              <input type="number" id="price" name="price" value="{{ $house->price }}" class="form-control col-md-7 col-xs-12" placeholder="House Price">
                            </div>
                         </div>


                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-8">
                              <label class="control-label" for="dimension">House Dimension <span class="required">*</span></label>
                              <input type="number" id="dimension" name="dimension" value="{{ $house->dimension }}" class="form-control col-md-7 col-xs-12" placeholder="dimension(m2)">
                            </div>
                          </div>

                             <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <label class="control-label" for="room_number">Room Number<span class="required">*</span></label>

                                 <select class="form-control" name="room_number" >
                                    <option selected="true" value="{{ $house->room_number }}">{{ $house->room_number }}</option>
                                    <option value="1+1">1+1</option>
                                    <option value="2+0">2+0</option>
                                    <option value="2+1">2+1</option>
                                    <option value="2+2">2+2</option>
                                    <option value="3+1">3+1</option>
                                    <option value="3+2">3+2</option>
                                    <option value="4+1">4+1</option>
                                    <option value="4+2">4+2</option>
                                    <option value="4+3">4+3</option>
                                    <option value="4+4">4+4</option>
                                    <option value="5+1">5+1</option>
                                    <option value="5+2">5+2</option>
                                    <option value="5+3">5+3</option>
                                    <option value="5+4">5+4</option>
                                    <option value="6+1">6+1</option>
                                    <option value="6+2">6+2</option>
                                    <option value="6+3">6+3</option>
                                    <option value="6 + Above">6 + Above</option>
                                </select>


                            </div>
                          </div>

                           
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <label class="control-label" for="hit_system">Hiting System <span class="required">*</span></label>
                              <input type="text" id="hit_system" name="hit_system" value="{{ $house->hit_system }}" class="form-control col-md-7 col-xs-12" placeholder="Hiting System">
                            </div>
                          </div>

                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <label class="control-label" for="bathrom_number">Number of Bathrooms <span class="required">*</span></label>
                              <input type="number" id="bathrom_number" name="bathrom_number" value="{{ $house->bathrom_number }}" class="form-control col-md-7 col-xs-12" placeholder="Number of Bathrooms">
                            </div>
                          </div>



                             <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                 <label class="control-label" for="balcon">Balcon <span class="required">*</span></label>
                                 <select name="balcon" class="form-control">
                                        <option  selected="true" value="{{ $house->balcon }}">{{ $house->balcon }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                  </select>
                              </div>
                            </div>

                      </div>
                     




                      <div class="row">
                        

                        <div class="block">
                              <div class="block_content">
                                <h2 class="title"><a><strong>House Properties</strong></a></h2><br> 
                              </div>

                                     
                                       <div class="form-group">
                                    
                                         @foreach($all_house_properties  as $property)

                                        

                                              <div class="col-md-2 col-sm-3 col-xs-6">
                                                <div class="checkbox">
                                                  <label>
                                                    
                                                     <input type="checkbox" class="flat" name="properties[]" value="{{ $property->id }}"
                                                          @foreach($house_properties as $prop)

                                                           {{ $property }}

                                                            @if(App\Model\Admin\Property::find($prop->property_id)->id == $property->id)
                                                              checked="true"
                                                            @endif 
                                                          @endforeach>

                                                       {{ $property->title }}

                                                  </label>
                    
                                                </div>
                                              </div>
                                         @endforeach  

                                      </div>

                                      <hr>  



                            </div>


                      </div>

                     <br><br>
                     
                         <div class="row">

                           <div class="col-md-8 col-md-offset-2 col-xs-12">
                            <div class="x_panel">
                              <div class="x_title">
                                <h2>House Description<small>Text</small></h2>
                               
                                <div class="clearfix"></div>
                              </div>
                              <div class="x_content">
                                
                                <div class="box-body pad">
                                 <textarea name="description" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor1"> {{ $house->description }}</textarea>
                               </div>
                                      
                              </div>
                            </div>
                          </div>


                          <div class="col-md-7">
                           <div class="ln_solid"></div>
                              <div class="form-group row">
                                <div class="col-md-2 col-md-offset-1">
                                   <a href='{{ route('admin.house.all') }}' class="btn btn-danger">Cancel</a>
                                </div>
                                 <div class="col-md-6">
                                  <button type="submit" class="btn btn-success btn-block">Save</button>
                                </div>
                              </div>
                          </div>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
  

          
  
  
@endsection

@section('footer')
  @include('ajax/il_ilce')
  @include('ajax/ilanturu')
  @include('ajax/ilan_grubu')

<!-- bootstrap-wysiwyg -->
<script src="{{ asset('admin/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>

<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor1');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>


{{-- <script>
  $(document).ready(function() {
    $(".select2").select2();
  });
</script> --}}
@endsection