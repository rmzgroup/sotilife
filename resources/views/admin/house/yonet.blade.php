@extends('admin/app')
@section('pagename','Houses')
@section('head')

<!-- Datatables -->
    <link href="../../../../admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

@endsection


@section('mainBody')
<!-- Content Wrapper. Contains page content -->

  <!-- Content Header (Page header) -->
  <!-- Main content -->
  <section class="content">
         <div class="x_panel" style="background-color: #2a3f54; min-height: 100px; color: white">
                   <div class="x_title">
                    <h2>All Houses</h2>
                    <div class="clearfix"></div>
                  </div>

                  <div class="col-md-3">
                    <a href="{{ route('admin.house.add') }}" title="Yeni bilgi ekle" class="btn btn-block btn-success"> Add a new House</a>
                  </div>
                
            </div>

           <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Houses List<small>Sotylife Real Estate</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-buttons" data-page-length='200' class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>S.No</th>
                          <th>Main Image</th>
                          <th>House No</th>
                          <th>City</th>
                          <th>House Name</th>
                          <th>Price Per Date</th>
                          <th>Address</th>
                          <th>Date Added</th>
                          <th>Details</th>
                          <th>Edit</th>
                          <th>Delete</th>
                        </tr>
                      </thead>


                      <tbody>
                          @foreach ($houses as $house)
                            <tr>
                              <td>{{ $loop->index + 1 }}</td>
                               
                              <td>
                                  @if($house->main_image)
                                <img src="../../../../../../../../upload/{{ $house->main_image }}" alt="" width="70px">
                                  @else
                                  <strong>Resim Yoktur</strong>   
                                @endif
                              </td>

                              <td>{{ $house->house_no }}</td>
                            
                              <td>{{ $house->city }}</td>
                              <td>{{ $house->title }}</td>
                              <td>{{ $house->price }}</td>
                              <td>{{ $house->address }}</td>
                              
                               

                              <td>{{ Carbon\Carbon::parse($house->created_at)->format('d / m / Y') }}</td>
                              <td><a href="{{ route('admin.house.detay', $house->id) }}" class="btn btn-block" style="background: orange; color: white"><i class="fa fa-eye" aria-hidden="true" title="Details about the house"></i> Details</a></td>
                            
                             <td><a href="{{ route('admin.house.duzenle', $house->id) }}" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-edit"></span> Edit</a></td>
                             
                               <td>
                                  <form id="delete-form-{{ $house->id }}" method="get" action="{{ route('admin.house.delete', $house->id) }}" style="display: none">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                  </form>
                                  <a href="" onclick="
                                  if(confirm('Are you sure you want to delete this house?'))
                                      {
                                        event.preventDefault();
                                        document.getElementById('delete-form-{{ $house->id }}').submit();
                                      }
                                      else{
                                        event.preventDefault();
                                      }" class="btn btn-danger btn-block"><span class="glyphicon glyphicon-trash"></span> Delete</a>
                                </td>
                              </tr>

                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
  </section>
  <!-- /.content -->

<!-- /.content-wrapper -->

   





@endsection

@section('footer')

 <!-- Datatables -->
    <script src="../../../../admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../../../admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../../../../admin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../../../admin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../../../admin/vendors/pdfmake/build/vfs_fonts.js"></script>
@endsection

