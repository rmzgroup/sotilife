@extends('admin/app')
@section('pagename','House Reservations')
@section('head')

<!-- Datatables -->
    <link href="/admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="/admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="/admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="/admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

@endsection


@section('mainBody')
<!-- Content Wrapper. Contains page content -->

  <!-- Content Header (Page header) -->
  <!-- Main content -->
  <section class="content">
         <div class="x_panel" style="background-color: #2a3f54; min-height: 100px; color: white">
                   <div class="x_title">
                    <h2>House Reservations</h2>
                    <div class="clearfix"></div>
                  </div>
            </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Reservations List<small>Sotylife</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-buttons" data-page-length='100' class="table table-striped table-bordered">
                      <thead>
                        <tr>
                           <th>S.No</th>
                           <th>House ID</th>
                           <th>House Requested</th>
                           <th>Name</th>
                           <th>Email</th>
                           <th>Phone Number</th>
                           <th>Entry Date</th>
                           <th>Outing Date</th>
                           <th>Reservation Date</th>
                           {{-- <th>Delete</th> --}}
                        </tr>
                      </thead>


                      <tbody>
                          @foreach ($reservations as $reservation)
                            <tr>
                              <td>{{ $loop->index + 1 }}</td>
                            
                              <td>#{{ App\Model\Admin\House::find($reservation->house_id)->house_no }}</td>
                              <td>{{ App\Model\Admin\House::find($reservation->house_id)->title }}</td>
                              <td>{{ $reservation->name }}</td>
                              <td>{{ $reservation->email }}</td>
                              <td>{{ $reservation->phone }}</td>
                              <td>{{ $reservation->entry }}</td>
                              <td>{{ $reservation->out }}</td>
                               <td>
                                    {{ $reservation->created_at }}
                              </td>
                            {{--   <td>
                              <form id="delete-form-{{ $reservation->id }}" method="get" action="{{ route('admin.reservation.delete', $reservation->id) }}" style="display: none">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                              </form>
                              <a href="" onclick="
                              if(confirm('Are you sure you want to delete this reservation?'))
                                  {
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $reservation->id }}').submit();
                                  }
                                  else{
                                    event.preventDefault();
                                  }" class="btn btn-danger btn-block"><span class="glyphicon glyphicon-trash"></span> Delete</a>
                            </td> --}}
                              </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


  </section>
  <!-- /.content -->

<!-- /.content-wrapper -->

@endsection

@section('footer')

 <!-- Datatables -->
    <script src="/admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="/admin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/admin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/admin/vendors/pdfmake/build/vfs_fonts.js"></script>
@endsection

