@extends('admin/app')
@section('pagename','Haberler')
@section('head')

@endsection

@section('mainBody')

<!-- page content -->

            <div class="page-title">
              <div class="title_left">
                <h3>{{ $update->name }} - Profili Güncele</h3>
              </div> 
            </div>
            <div class="clearfix"></div>

    

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                     <form class="form-horizontal" method="POST" action="{{ route('admin.kurumsal.firmaelemanduzenle.save', $update->id) }}" enctype="multipart/form-data">
                       
                        {{ csrf_field() }}


                      <span class="section">Resim Şeç</span>

                      <div class="item form-group"  style="padding-bottom: 15px;">
                               <div class="box-body col-md-2">
                                 <img src="../../../../../../PersonelFotograflari/{{ $update->image }}" alt="">
                               </div>
                     
                                 <div class="box-body col-md-4">
                                  <label for="title">Resim Şeç - (220, 220)</label>
                                    <input type="file" name="image" id="image" accept="image/x-png,image/gif,image/jpeg,image/jpg,image/svg,image/png">
                                </div>
                       
                      </div>


                      <span class="section">Kullanıcı bilgileri Günceleyin</span>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Ad ve Soyad <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="both name(s) e.g Jon Doe" type="text" value="{{ $update->name }}">
                        </div>
                      </div>

                             <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="country">Ülke <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="country" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="country" placeholder="Türkiye" type="text" value="{{ $update->country }}">
                        </div>
                      </div>

                
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">E-posta <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" id="email" name="email" class="form-control col-md-7 col-xs-12" placeholder="myemail@hotmail.com"  value="{{ $update->email }}">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phonenumber">Telefon <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="phonenumber" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="phonenumber" placeholder="+90 539 000 000" type="number" value="{{ $update->phonenumber }}">
                        </div>
                      </div>

               
              <span class="section">Görev Baslığı</span>

                <div class="row">

                    <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">TR Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_tr" placeholder="Görev Başlığı"  type="text" value="{{ $update->position_tr }} ">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">EN Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_en" placeholder="Görev Başlığı"  type="text" value="{{ $update->position_en }}">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">FR Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_fr" placeholder="Görev Başlığı"  type="text" value="{{ $update->position_fr }}">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">AR Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_ar" placeholder="Görev Başlığı"  type="text" value="{{ $update->position_ar }}">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">ES Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_es" placeholder="Görev Başlığı"  type="text" value="{{ $update->position_es }}">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">DE Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_de" placeholder="Görev Başlığı"  type="text" value="{{ $update->position_de }}">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">RU Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_ru" placeholder="Görev Başlığı"  type="text" value="{{ $update->position_ru }}">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">FA Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_fa" placeholder="Görev Başlığı"  type="text" value="{{ $update->position_fa }}">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">PT Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_pt" placeholder="Görev Başlığı"  type="text" value="{{ $update->position_pt }}">
                    </div>

                </div>

                       <div class="ln_solid"></div>
                      <div class="form-group">
                             <div class="col-md-3">
                          <a href="{{ route('admin.kurumsal.firmayapisi') }}" title="Geri dön" type="button" class="btn btn-primary btn-block"> Geri</a>
                        </div>
                        <div class="col-md-6 col-md-offset-1">
                          <button id="send1" type="submit" class="btn btn-success btn-block"><i class="fa fa-floppy-o" aria-hidden="true"></i> Kaydet</button>
                        </div>
                      </div>

         

              
                    </form>

                  </div>
                </div>
              </div>
            </div>
  
@endsection

@section('footer')

@endsection