@extends('admin/app')
@section('pagename','Haberler')
@section('head')

<style type="text/css" media="screen">
  .profil-pic{
    border-radius: 50%;
    border: 1px solid #F7F7F7;
  }
</style>

@endsection

@section('mainBody')

 <!-- page content -->
        
            <div class="page-title">
              <div class="title_left">
                <h3>Fırma Çalısan Personelleri</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                
                    <span class="input-group-btn" >
                     
                        <a data-toggle="modal" data-placement="top" title="Settings" data-target=".personel-ekle" class="btn btn-primary btn-block" type="button">Yeni Personel Ekle</a>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mevcut Personel Listesi</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   
                    @foreach($personels as $personel)

                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left" style="padding-top: 15px;">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view profil-pic" src="../../../PersonelFotograflari/{{ $personel->image }}" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      <h3>{{ $personel->name }}</h3>

                      <ul class="list-unstyled user_data">

                        <li>
                          <i class="fa fa-briefcase user-profile-icon"></i> {{ $personel->position_tr }}
                        </li>

                        <li>
                          <i class="fa fa-globe user-profile-icon"></i> {{ $personel->country }}
                        </li>

                         <li>
                          <i class="fa fa-envelope-o user-profile-icon"></i> {{ $personel->email }}
                        </li>

                         <li>
                          <i class="fa fa-phone user-profile-icon"></i> {{ $personel->phonenumber }}
                        </li>

                      </ul>

                      <a class="btn btn-success" href="{{ route('admin.kurumsal.firmaelemandelete.duzenle', $personel->id) }}"><i class="fa fa-edit m-right-xs"></i> Profili Düzenle</a>
                      <br />

                        <a class="btn btn-danger" href="{{ route('admin.kurumsal.firmaelemandelete', $personel->id) }}"><i class="fa fa-edit m-right-xs"></i> Profili Sil</a>
                    </div>
                    
                    @endforeach

                     
                  </div>
                </div>
              </div>
            </div>
        
        <!-- /page content -->


        {{-- Yeni Personel Ekle --}}


         <div class="modal fade personel-ekle" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">



              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Kullanıcı bilgileri</h4>
              </div>
              <div class="modal-body">
                 <form class="form-horizontal" method="POST" action="{{ route('admin.kurumsal.firmaelemandelete.save') }}" enctype="multipart/form-data">
                       
                        {{ csrf_field() }}

                      <span class="section">Kullanıcı bilgileri Günceleyin</span>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Ad ve Soyad <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="both name(s) e.g Jon Doe" type="text" required="true">
                        </div>
                      </div>

                  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="country">Ülke <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="country" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="country" placeholder="Türkiye" type="text">
                        </div>
                      </div>

                
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">E-posta <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" id="email" name="email" class="form-control col-md-7 col-xs-12" placeholder="myemail@hotmail.com" >
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phonenumber">Telefon <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="phonenumber" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="phonenumber" placeholder="+90 539 000 000" type="number">
                        </div>
                      </div>

              <span class="section">Görev Baslığı</span>

                <div class="row">

                    <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">TR Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_tr" placeholder="Görev Başlığı"  type="text">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">EN Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_en" placeholder="Görev Başlığı"  type="text">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">FR Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_fr" placeholder="Görev Başlığı"  type="text">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">AR Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_ar" placeholder="Görev Başlığı"  type="text">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">ES Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_es" placeholder="Görev Başlığı"  type="text">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">DE Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_de" placeholder="Görev Başlığı"  type="text">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">RU Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_ru" placeholder="Görev Başlığı"  type="text">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">FA Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_fa" placeholder="Görev Başlığı"  type="text">
                    </div>

                        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label" for="name" class="pull-left">PT Görev Baslığı <span class="required">*</span></label>
                      <input class="form-control" data-validate-length-range="6" name="position_pt" placeholder="Görev Başlığı"  type="text">
                    </div>

                </div>

              
     

          

                   <span class="section">Resim Şeç</span>

                      <div class="item form-group  ">
                     
                                 <div class="box-body col-md-6">
                                  <label for="title">Resim Şeç - (220, 220)</label>
                                    <input type="file" name="image" id="image" accept="image/x-png,image/gif,image/jpeg,image/jpg,image/svg,image/png" required="true">
                                </div>
                       
                      </div>

                

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                         <button id="send1" type="submit" class="btn btn-success btn-block"><i class="fa fa-floppy-o" aria-hidden="true"></i> Kaydet</button>
                        </div>
                      </div>
                    </form>
                 </div>


              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>

            </div>
          </div>
        </div>


  
@endsection

@section('footer')

@endsection