@extends('admin/app')
@section('pagename','Ürünler')
@section('head')

<!-- Datatables -->
    <link href="../../../../admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

@endsection


@section('mainBody')
<!-- Content Wrapper. Contains page content -->

  <!-- Content Header (Page header) -->
  <!-- Main content -->
  <section class="content">
         <div class="x_panel" style="background-color: #2a3f54; min-height: 100px; color: white">
                   <div class="x_title">
                    <h2>Ürünler Yönet</h2>
                    <div class="clearfix"></div>
                  </div>

                  <div class="col-md-3">
                    <a href="/admin/urunler/yonet" title="Yeni bilgi ekle" class="btn btn-block btn-info"> Tüm Ürünler</a>
                  </div>
                  <div class="col-md-3">
                    <a href="/admin/urun/ekle" title="Yeni bilgi ekle" class="btn btn-block btn-success"> Yeni Ürünü Ekle</a>
                  </div>

                 <div class="col-md-12 x_title">
                    <h2>Kategöriye göre Ürün filtrele</h2>
                    <div class="clearfix"></div>
                  </div>

                    <form role="form" action="{{ route('admin.urun.kategori.detay.post') }}" method="post">

          
                    {{ csrf_field() }}


                          <div class="col-md-6">
                             <select name="category_id" class="form-control" required="true">
                  
                               <option  selected="true" value="">Ürün Kategori şeç</option>

                                      @foreach($categories as $category)

                                        
                                            <option value="{{ $category->id }}"> -- {{ $category->kategori_ad_tr }} </option>
                                        
                                               
                                      @endforeach



                            </select>


                        </div>

                        <div class="col-md-3">
                            <button type="submit" class="btn btn-success btn-block">Filtrele</button>
                        </div>
                      </form>

                
            </div>

           <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Ürün Listesi <small>Burak Dölek Diş Ticaret</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-buttons" data-page-length='200' class="table table-striped table-bordered">
                      <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Ürün Kategori</th>
                          <th>Ürün Adı</th>
                          <th>Ürün Türü</th>
                          <th>Resim</th>
                          <th>Ekleme Tariği</th>
                          <th>Ürünü İncele</th>
                          <th>Ürün Durumu</th>
                          <th>Düzenle</th>
                          <th>Sil</th>
                        </tr>
                      </thead>


                      <tbody>
                          @foreach ($products as $product)
                            <tr>
                              <td>{{ $loop->index + 1 }}</td>
                              <td>
                                  @if($product->category_id == 0) Ana Kategori 
                                     @elseif(App\Model\Admin\category::find($product->category_id))
                                     {{ App\Model\Admin\category::find($product->category_id)->kategori_ad_tr }}
                                  @endif
                              </td>
                            
                              <td>{{ $product->urun_ad_tr }}</td>
                            
                              <td>
                                   @if($product->ikinci_el == 1) 

                                    <i class="fa fa-scribd" aria-hidden="true"></i> Sıfır

                                     @else

                                    <i class="fa fa-hand-peace-o" aria-hidden="true"></i> İkinci El

                                   @endif
                              </td>

                                <td>

                                @if($product->resim_kucuk)
                                <img src="../../../../../../../../upload/{{ $product->resim_kucuk }}" alt="" width="70px">
                                  @else
                                  <strong>Resim Yoktur</strong>   
                                @endif

                              </td>

                              <td>{{ $product->created_at }}</td>
                              <td><a href="{{ route('admin.urun.detay', $product->id) }}" class="btn btn-block" style="background: orange; color: white"><i class="fa fa-eye" aria-hidden="true" title="Aktif haline getir"></i> Detaylar</a></td>
                              <td>

 

                                   @if($product->urun_aktif == 1) 

                                    <a href="{{ route('admin.urun.deactivate', $product->id) }}" class="btn btn-info btn-block" title="Etkisiz haline getir"><i class="fa fa-level-up" aria-hidden="true"></i> Aktif</a>

                                     @else

                                     <a href="{{ route('admin.urun.activate', $product->id) }}" class="btn btn-danger btn-block"><i class="fa fa-level-down" aria-hidden="true" title="Aktif haline getir"></i> Aktif Değil</a>

                                   @endif
                              </td>
                        
                             <td><a href="{{ route('admin.urun.duzenle', $product->id) }}" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-edit"></span> Düzelt</a></td>
                             
                              <td>
                              <form id="delete-form-{{ $product->id }}" method="get" action="{{ route('admin.urun.delete', $product->id) }}" style="display: none">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                              </form>
                              <a href="" onclick="
                              if(confirm('Bu haberi silmek istediğinizden emin misiniz?'))
                                  {
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $product->id }}').submit();
                                  }
                                  else{
                                    event.preventDefault();
                                  }" class="btn btn-danger btn-block"><span class="glyphicon glyphicon-trash"></span> Sil</a>
                            </td>
                              </tr>

                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
  </section>
  <!-- /.content -->

<!-- /.content-wrapper -->

   





@endsection

@section('footer')

 <!-- Datatables -->
    <script src="../../../../admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../../../admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../../../../admin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../../../admin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../../../admin/vendors/pdfmake/build/vfs_fonts.js"></script>
@endsection

