@extends('admin/app')
@section('pagename','Ürünler Detyları')
@section('head')

@endsection

@section('mainBody')


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                    <div class="x_content">






      <div class="col-md-8 col-sm-12 col-md-offset-2 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><strong>{{ $product->urun_ad_tr }} </strong>Ürünün Detayları <small>Burak Dölek Diş Ticaret</small></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <ul class="list-unstyled timeline">
                   

                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Ürünün Categorisi</strong></a></h2> 
                          <p class="excerpt">{{ App\Model\Admin\category::find($product->category_id)->kategori_ad_tr }}
                          </p>
                        </div>
                      </div>
                    </li>

                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Ürününün Adı</strong></a></h2> 
                          <p class="excerpt">{{ $product->urun_ad_tr }}
                          </p>
                        </div>
                      </div>
                    </li>



                   <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Ürün Türü</strong></a></h2> 
                          <p class="excerpt">
                               @if($product->ikinci_el == 1) 

                                    <i class="fa fa-scribd" aria-hidden="true"></i> Sıfır

                                     @else

                                    <i class="fa fa-hand-peace-o" aria-hidden="true"></i> İkinci El

                                   @endif
                          </a>
                          </p>
                        </div>
                      </div>
                    </li>
           

                 @if($product->urun_icerik_tr)
                     <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Ürünün Acıklamaları</strong></a></h2> 
                          <p class="excerpt">
                             {!! $product->urun_icerik_tr !!}
                          </p>
                        </div>
                      </div>
                    </li>
                @endif
            

                   <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Ana Ürünün Fotografı</strong></a></h2> 
                          <p class="excerpt" style="padding-top: 15px">
                           <img src="../../.../../../../upload/{{ $product->resim_kucuk }}" alt="" class="img-responsive" style="padding-top: 5px">
                         
                          </p>
                        </div>
                      </div>
                    </li>

                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Diğer Ürünün Fotografı</strong></a></h2> 
                        </div>

                               <div class="row" style="padding-top: 15px">
                            
                            @foreach($productimages as $image)

                                   <div class="col-sm-4">
                                      <img src="../../.../../../../upload/{{ $image->thumb }}" alt="" class="img-responsive" style="padding-top: 10px">
                                  </div>
                              
                            @endforeach


                             </div>



                      </div>
                    </li>

             
                  </ul>


                    <div class="col-md-12">
                    <div class="ln_solid"></div>
                          <div class="form-group row">
                            <div class="col-md-2 col-md-offset-1">
                               <a href='/admin/urunler/yonet' class="btn btn-danger">Geri Dön</a>
                            </div>
                          </div>
                  </div>


                </div>
              </div>
            </div>
       
           </div>
        </div>
      </div>
    </div>

@endsection

@section('footer')


@endsection