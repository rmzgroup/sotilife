@extends('admin/app')
@section('pagename','Ürünler')
@section('head')
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/css/prism.css') }}">

@endsection

@section('mainBody')

  <!-- page content -->

            <div class="page-title">
              <div class="title_left">
                <h3>Ürün Ekle</h3>
              </div> 
            </div>
            <div class="clearfix"></div>

    

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                <form role="form" action="{{ route('admin.urun.ekle.save') }}" method="post" name="edit" enctype="multipart/form-data">

          
          {{ csrf_field() }}
  
               <div class="row">

                  
                      <div class="item form-group col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-1 col-md-offset-2">
                          <label>Ürün Kategori</label>
                        </div>

                        <div class="col-md-6">
                             <select name="category_id" class="form-control" required="true">
                  
                               <option  selected="true" value="">Ürün Kategori şeç</option>

                               @foreach($maincategories as $maincategory)
                                     
                                      <option value="" disabled>{{ $maincategory->kategori_ad_tr }}</option>
                                      

                                      @foreach($categories as $category)

                                         @if($maincategory->id == $category->maincategory_id) 
                                            <option value="{{ $category->id }}"> ___| {{ $category->kategori_ad_tr }} </option>
                                         @endif
                                               
                                      @endforeach


                               @endforeach


                            </select>


                        </div>
                     
                      </div>
                  
              
                </div>
                <div class="row">

                 <div class="page-title">
                    <div class="urun_ad_left">
                      <h3>Ürün Başlığı</h3>
                    </div> 
                  </div>
                 <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">TR Ürün Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="urun_ad_tr" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="urun_ad_tr" placeholder="Ürün Başlığı"  type="text">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">EN Ürün Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="urun_ad_en" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="urun_ad_en" placeholder="Ürün Başlığı"  type="text">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">FR Ürün Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="urun_ad_fr" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="urun_ad_fr" placeholder="Ürün Başlığı"  type="text">
                    </div>
                  </div>

                <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">AR Ürün Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="urun_ad_ar" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="urun_ad_ar" placeholder="Ürün Başlığı"  type="text">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">ES Ürün Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="urun_ad_es" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="urun_ad_es" placeholder="Ürün Başlığı"  type="text">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">DE Ürün Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="urun_ad_de" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="urun_ad_de" placeholder="Ürün Başlığı"  type="text">
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">RU Ürün Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="urun_ad_ru" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="urun_ad_ru" placeholder="Ürün Başlığı"  type="text">
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">FA Ürün Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="urun_ad_fa" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="urun_ad_fa" placeholder="Ürün Başlığı"  type="text">
                    </div>
                  </div>

                  <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">PT Ürün Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="urun_ad_pt" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="urun_ad_pt" placeholder="Ürün Başlığı"  type="text" >
                    </div>
                  </div>
                </div>

                <hr>

                <div class="row">
                    
                          <div class="col-md-1">
                            <label>Ürün Türü</label>
                          </div> 

                          <div class="col-md-3">
                             <select name="ikinci_el" class="form-control">
                                      <option value="1" selected="true"> Sıfır </option>
                                      <option value="2"> İkici El</option>
                               </select>
                          </div>

                         <div class="col-md-1 col-md-offset-1">
                            <label>Ana Resim Şeç</label>
                          </div> 

                           <div class="box-body col-md-2">
                            <label for="title">Resim Şeç - (220, 220)</label>
                              <input type="file" name="resim_buyuk" id="resim_buyuk" accept="image/x-png,image/gif,image/jpeg,image/jpg,image/svg,image/png">
                          </div>

                          <div class="box-body col-md-2">
                            <label for="title">Diğer Resimleri Şeç - (220, 220)</label>
                              <input type="file" name="images[]" id="resim_kucuk" multiple="multiple" accept="image/x-png,image/gif,image/jpeg,image/jpg,image/svg,image/png">
                          </div>


                           <div class="box-body col-md-2">
                           <label>Pdf Şeç</label>
                              <input type="file" name="pdf" accept="application/pdf,application/vnd.ms-excel" id="pdf">
                          </div>




                </div>

                <hr>

                <div class="row">
                     <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>TR ürünün acıklaması<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="urun_icerik_tr" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor1"> </textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                 <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>EN ürünün acıklaması<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="urun_icerik_en" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor2"> </textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                <hr>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>FR ürünün acıklaması<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="urun_icerik_fr" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor3"> </textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>AR ürünün acıklaması<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="urun_icerik_ar" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor4"> </textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                 <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>ES ürünün acıklaması<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="urun_icerik_es" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor5"> </textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>DE ürünün acıklaması<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="urun_icerik_de" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor6"> </textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>RU ürünün acıklaması<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="urun_icerik_ru" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor7"> </textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                      <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>FA ürünün acıklaması<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="urun_icerik_fa" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor8"> </textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                      <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>PT ürünün acıklaması<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="urun_icerik_pt" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor9"> </textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>
s
                </div>


                  <div class="col-md-7">
                    <div class="ln_solid"></div>
                          <div class="form-group row">
                            <div class="col-md-2 col-md-offset-1">
                              <a href='/admin/urunler/yonet' class="btn btn-danger">Geri Dön</a>
                            </div>
                             <div class="col-md-6">
                              <button type="submit" class="btn btn-success btn-block">Kaydet</button>
                            </div>
                          </div>
                  </div>



                    
                    </form>
                  </div>
                </div>
              </div>
            </div>
  

           
  
  
@endsection

@section('footer')

<!-- bootstrap-wysiwyg -->
<script src="{{ asset('admin/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>

<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor1');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor2');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor3');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor4');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor5');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor6');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor7');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor8');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>

<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor9');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>

{{-- <script>
  $(document).ready(function() {
    $(".select2").select2();
  });
</script> --}}

@endsection