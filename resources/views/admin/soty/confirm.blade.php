@extends('admin/app')
@section('pagename','Investment List')
@section('head')

<!-- Datatables -->
    <link href="../../../../admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <style type="text/css" media="screen">
      .confirmed-payment{
        text-align: center;
        font-size: 14px;
        font-weight: 700;
        color: green;
      }
    </style>
@endsection


@section('mainBody')
<!-- Content Wrapper. Contains page content -->

  <!-- Content Header (Page header) -->
  <!-- Main content -->
  <section class="content">
         <div class="x_panel" style="background-color: #2a3f54; min-height: 100px; color: white">
                   <div class="x_title">
                    <h2>All Confirmed Investment List</h2>
                    <div class="clearfix"></div>
                  </div>
            </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>S.No</th>
                          <th>Name and Surname</th>
                          <th>Reference Code</th>
                          <th>Reference Owner</th>
                          <th>Coin Type</th>
                          <th>USD Amount</th>
                          <th>Crypto Amount</th>
                          <th>Crypto Address</th>
                          <th>Application Date</th>
                          <th>Confirmation Date</th>
                          <th>Cancel Confirmation</th>
                        </tr>
                      </thead>


                      <tbody>
                         @foreach ($confirmedList as $payment)
                            <tr>
                              <td>{{ $loop->index + 1 }}</td>
                              <td>{{ $payment->user->name }}</td>
                              <td>{{ $payment->user->code }}</td>
                              <td>@if ($payment->user->user)
                                {{ $payment->user->user->name }}
                              @endif</td>
                              <td>{{ $payment->coin_type }}</td>
                              <td>{{ $payment->amount_usd }} USD</td>
                              <td>{{ $payment->amount_crypto }}</td>
                              <td>{{ $payment->user->address }}</td>
                              <td>{{ \Carbon\Carbon::parse($payment->created_at)->format('d - m - Y')}}</td>
                              <td>{{ \Carbon\Carbon::parse($payment->update_at)->format('d - m - Y')}}</td>
                             
                              <td>
                              <form id="delete-form-{{ $payment->id }}" method="get" action="{{ route('admin.delete.application', $payment->id) }}" style="display: none">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                              </form>
                              <a href="" onclick="
                              if(confirm('Are you sure you want to delete this application?'))
                                  {
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $payment->id }}').submit();
                                  }
                                  else{
                                    event.preventDefault();
                                  }" class="btn btn-block btn-danger"><span class="glyphicon glyphicon-trash"></span> Cancel Confirmation</a>
                            </td>
                              </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
  </section>
  <!-- /.content -->

<!-- /.content-wrapper -->

@endsection

@section('footer')

 <!-- Datatables -->
    <script src="../../../../admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../../../admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../../../../admin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../../../admin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../../../admin/vendors/pdfmake/build/vfs_fonts.js"></script>
@endsection

