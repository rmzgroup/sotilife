<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sotylife | Dashboard </title>

    <!-- Bootstrap -->
    <link href="../../admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../admin/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../admin/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../../admin/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../../admin/build/css/custom.min.css" rel="stylesheet">

    <link rel="shortcut icon" href="../../../user/img/favicon.ico"> 

    <style type="text/css" media="screen">

      .login_content h1{
        font-weight: 500;
       letter-spacing: 1px;
      }

      .login{
         background-image: url(/login.jpg);
      }

    </style>
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">

            <div class="logo">
                <img src="/logo.png" alt="Sotylife" class="img-responsive" style="margin: 10px auto;">
            </div>

              <form class="form-horizontal" method="POST" action="{{ route('admin.auth.loginAdmin') }}">
                  {{ csrf_field() }}

                     <h1>Admin login</h1>

                  @if (session('approve'))
                      <div class="row">
                          <div class="col-md-12">
                              <div class="alert alert-danger alert-dismissible">
                                  {{ session('approve') }}
                              </div>
                          </div>
                      </div>
                  @endif

              <div>

                  @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif

                <input id="email" type="text" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="E-mail" required="true" value="{{ old('email') }}"/>
                 

              </div>
              <div>
                 @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                
                <input type="password" id="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" required="true" />

               

                 <div class="clearfix"></div>
               

              </div>

               <div class="form-group row">
                <div class="col-md-6 offset-md-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
            </div>

              <div>
                <button type="submit" class="btn btn-primary btn-block">
                    Log in
                </button>

              </div>

              <div class="clearfix"></div>
                {{-- <a class="reset_pass" href="{{ route('password.request') }}">Şifreyi unuttun mu?</a> --}}

              <div class="separator">
                <div class="clearfix"></div>
                <br/>

                <div>
                  <h1><i class="fa fa-lock"></i> SOTYLIFE</h1>
                  <p>Copyright © 2018 Sotylife. <br> All rights reserved. </p>
                </div>
              </div>
                    </form>
           
          </section>
        </div>
      </div>
    </div>
  </body>
</html>

