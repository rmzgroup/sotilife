@extends('admin/app')
@section('pagename','House Properties')
@section('head')

<!-- Datatables -->
    <link href="/admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="/admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="/admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="/admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

@endsection


@section('mainBody')
<!-- Content Wrapper. Contains page content -->

  <!-- Content Header (Page header) -->
  <!-- Main content -->
  <section class="content">
         <div class="x_panel" style="background-color: #2a3f54; min-height: 100px; color: white">
                   <div class="x_title">
                    <h2>House Properties</h2>
                    <div class="clearfix"></div>
                  </div>

                  <div class="col-md-3">
                    <a href="javascript:void(0)" data-toggle="modal" data-placement="top" title="Settings" data-target=".new-prop" title="Yeni bilgi ekle" class="btn btn-block btn-success"> Add a new Property</a>
                   
                  </div>
            </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Property List<small>Sotylife</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-buttons" data-page-length='100' class="table table-striped table-bordered">
                      <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Property Name</th>
                           <th>Creation Date</th>
                           <th>Delete</th>
                        </tr>
                      </thead>


                      <tbody>
                          @foreach ($properties as $property)
                            <tr>
                              <td>{{ $loop->index + 1 }}</td>
                       
                              <td>
                                     {{ $property->title }}
                              </td>
                               <td>
                                    {{ $property->created_at }}
                              </td>
                              <td>
                              <form id="delete-form-{{ $property->id }}" method="get" action="{{ route('admin.property.delete', $property->id) }}" style="display: none">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                              </form>
                              <a href="" onclick="
                              if(confirm('Are you sure you want to delete this Property?'))
                                  {
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $property->id }}').submit();
                                  }
                                  else{
                                    event.preventDefault();
                                  }" class="btn btn-danger btn-block"><span class="glyphicon glyphicon-trash"></span> Delete</a>
                            </td>
                              </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


                   <div class="modal fade new-prop" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">



                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel">Add a new Property</h4>
                </div>
                <div class="modal-body">

                    <form class="form-horizontal" method="POST" action="{{ route('admin.property.ekle.save') }}">
                      
                          @csrf

                        <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Property Name<span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="title" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="title" placeholder=" e.g Air Conditioning" required="required" type="text">
                          </div>
                        </div>


                        <div class="form-group">
                          <div class="col-md-6 col-md-offset-3">
                            <button type="submit" class="btn btn-success btn-block">Submit</button>
                          </div>
                        </div>
                      </form>
                </div>


                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

              </div>
            </div>
          </div>



  </section>
  <!-- /.content -->

<!-- /.content-wrapper -->

@endsection

@section('footer')

 <!-- Datatables -->
    <script src="/admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="/admin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/admin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/admin/vendors/pdfmake/build/vfs_fonts.js"></script>
@endsection

