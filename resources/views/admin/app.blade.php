
<!DOCTYPE html>
<html lang="tr">

<head>

@include('admin/layouts/head')

</head>

<body class="nav-md">
	<div class="container body">
    
    <div class="main_container">
      	@include('admin/layouts/navbar') 
			 @include('admin/layouts/sidebar')


		<div class="right_col" role="main">
             <div class="">
	         
	         @include('admin/layouts/notifications')  

              @section('mainBody')

			  @show

             </div>
        </div>


                  
			


		    @include('admin/layouts/footer')    
      	</div>
     </div>		 	
 	
    @include('admin/layouts/scripts') 

</body>

</html>
