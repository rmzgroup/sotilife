    @extends('admin/app')
@section('pagename','Başvuru')
@section('head')



@endsection


@section('mainBody')
<!-- Content Wrapper. Contains page content -->

  <!-- Content Header (Page header) -->
  <!-- Main content -->
  <section class="content">
         <div class="x_panel" style="background-color: #2a3f54; min-height: 100px; color: white">
                   <div class="x_title">
                    <h2>{{ $details->adsoyad }}  Başvurunun Özeti</h2>
                    <div class="clearfix"></div>
                  </div>
            </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Başvuruları <small>Burak Dölek Diş Ticaret</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

         
           <div class="col-md-8 col-sm-12 col-md-offset-2 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><strong>{{ $details->adsoyad }} </strong>Başvurunun Detayları <small>Burak Dölek Diş Ticaret</small></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <ul class="list-unstyled timeline">
                   
                        <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Adayının Fotografı</strong></a></h2> 
                          <p class="excerpt" style="padding-top: 15px">
                           <img src="../../.../../../../upload/{{ $details->image }}" alt="" class="img-responsive" style="padding-top: 5px">
                         
                          </p>
                        </div>

                         <div class="block_content">
                          <h2 class="title"><a><strong>Ad ve Soyad</strong></a></h2> 
                                {{ $details->adsoyad }}
                          </p>
                        </div>

                      </div>
                    </li>

              
                  

                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Doğum Tariği ve Yeri</strong></a></h2> 
                               {{ $details->dogumtarihi }} |  {{ $details->dogumyeri }}
                          </p>
                        </div>
                      </div>
                    </li>

                       <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Cinsiyet</strong></a></h2> 
                               {{ $details->cinsiyet }}
                          </p>
                        </div>
                      </div>
                    </li>

                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Medeni Durumu</strong></a></h2> 
                               {{ $details->medeni_hal }}
                          </p>
                        </div>
                      </div>
                    </li>

                      <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Ehliyet</strong></a></h2> 
                               {{ $details->ehliyet }}
                          </p>
                        </div>
                      </div>
                    </li>


                      <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Askerlik Durumu</strong></a></h2> 
                               {{ $details->askerlik_durumu }}
                          </p>
                        </div>
                      </div>
                    </li>


                      <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Askerlik Tercih Tariği</strong></a></h2> 
                               {{ $details->askerlik_tecil_tarihi }}
                          </p>
                        </div>
                      </div>
                    </li>

                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Ev Telefon</strong></a></h2> 
                               {{ $details->ev_telefonu }}
                          </p>
                        </div>
                      </div>
                    </li>

                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Cep Telefon</strong></a></h2> 
                               {{ $details->cep_telefonu }}
                          </p>
                        </div>
                      </div>
                    </li>


                      <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>E-Posta Adresi</strong></a></h2> 
                               {{ $details->email_adresi }}
                          </p>
                        </div>
                      </div>
                    </li>

                       <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>İl-İlçe</strong></a></h2> 
                               {{ $details->il_ilce }}
                          </p>
                        </div>
                      </div>
                    </li>


                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Adres</strong></a></h2> 
                               {{ $details->adres }}
                          </p>
                        </div>
                      </div>
                    </li>

                     <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Dil Yetenekleri</strong></a></h2> 
                               <strong>İngilizce</strong>: {{ $details->ingilizce }} | <strong>Almanca</strong>: {{ $details->almanca }} | <strong>Rusca</strong>: {{ $details->rusca }} | <strong>Arapca</strong>: {{ $details->arapca }} | <strong>Diğer Diller</strong>: {{ $details->diger_diller }}
                          </p>
                        </div>
                      </div>
                    </li>


                   

                    
                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Lisan Üstü</strong></a></h2> 
                               {{ $details->lisans_ustu }}
                          </p>
                        </div>
                      </div>
                    </li>

                       <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Lisan Üstü Giriş ve Mezuniyet Tarihleri</strong></a></h2> 
                               {{ $details->lisans_ustu_giris_mezuniyet }}
                          </p>
                        </div>
                      </div>
                    </li>

                      <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Lisan</strong></a></h2> 
                               {{ $details->lisans_ustu }}
                          </p>
                        </div>
                      </div>
                    </li>

                       <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Lisan Giriş ve Mezuniyet Tarihleri</strong></a></h2> 
                               {{ $details->lisans_giris_mezuniyet }}
                          </p>
                        </div>
                      </div>
                    </li>

                      <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Lise</strong></a></h2> 
                               {{ $details->lise }}
                          </p>
                        </div>
                      </div>
                    </li>

                       <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Lise Giriş ve Mezuniyet Tarihleri</strong></a></h2> 
                               {{ $details->lise_giris_mezuniyet }}
                          </p>
                        </div>
                      </div>
                    </li>

                       <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>1. İş Tecrübe</strong></a></h2> 
                               {{ $details->is_tecrube_1 }}
                          </p>
                        </div>
                      </div>
                    </li>

                      <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>2. İş Tecrübe</strong></a></h2> 
                               {{ $details->is_tecrube_2 }}
                          </p>
                        </div>
                      </div>
                    </li>

                         <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>3. İş Tecrübe</strong></a></h2> 
                               {{ $details->is_tecrube_3 }}
                          </p>
                        </div>
                      </div>
                    </li>

                       <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Bilgisayar</strong></a></h2> 
                               {{ $details->bilgisayar }}
                          </p>
                        </div>
                      </div>
                    </li>

                       <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Hobiler</strong></a></h2> 
                               {{ $details->hobi }}
                          </p>
                        </div>
                      </div>
                    </li>

                       <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Referanslar</strong></a></h2> 
                               {{ $details->referans }}
                          </p>
                        </div>
                      </div>
                    </li>

                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Pozisyon</strong></a></h2> 
                               {{ $details->pozisyon }}
                          </p>
                        </div>
                      </div>
                    </li>


                      <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title"><a><strong>Ücret Beklenti</strong></a></h2> 
                               {{ $details->ucret_beklentiniz }}
                          </p>
                        </div>
                      </div>
                    </li>







               
                  </ul>


                    <div class="col-md-12">
                    <div class="ln_solid"></div>
                          <div class="form-group row">
                            <div class="col-md-2 col-md-offset-1">
                               <a href='{{ URL::previous() }}' class="btn btn-danger btn-block">Geri Dön</a>
                            </div>

                            <div class="col-md-2">
                               <a href="{{ route('admin.basvurulari.irtibatagec', $details->id) }}" class="btn btn-success btn-block">İrtibata Geç</a>
                            </div>
                          </div>
                  </div>


                </div>
              </div>
            </div>
       
                  </div>
                </div>
              </div>
  </section>
  <!-- /.content -->

<!-- /.content-wrapper -->

@endsection

@section('footer')

@endsection


