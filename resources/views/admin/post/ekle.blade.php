@extends('admin/app')
@section('pagename','İlan Türü Ekle')
@section('head')

@endsection

@section('mainBody')

  <!-- page content -->

            <div class="page-title">
              <div class="title_left">
                <h3>İlan Türü Ekle</h3>
              </div> 
            </div>
            <div class="clearfix"></div>

    

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                <form role="form" action="{{ route('admin.turu.ekle.save') }}" method="post">

          
          {{ csrf_field() }}
  
                <div class="row">

                 <div class="page-title">
                    <div class="title_left">
                      <h3>İlan Türü Başlığı</h3>
                    </div> 
                  </div>
                 <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">TR İlan Türü Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_tr" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_tr" placeholder="İlan Türü Başlığı"  type="text">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">EN İlan Türü Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_en" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_en" placeholder="İlan Türü Başlığı"  type="text">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">FR İlan Türü Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_fr" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_fr" placeholder="İlan Türü Başlığı"  type="text">
                    </div>
                  </div>

           <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">AR İlan Türü Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_ar" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_ar" placeholder="İlan Türü Başlığı"  type="text">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">ES İlan Türü Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_es" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_es" placeholder="İlan Türü Başlığı"  type="text">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">DE İlan Türü Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_de" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_de" placeholder="İlan Türü Başlığı"  type="text">
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">RU İlan Türü Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_ru" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_ru" placeholder="İlan Türü Başlığı"  type="text">
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">FA İlan Türü Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_fa" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_fa" placeholder="İlan Türü Başlığı"  type="text">
                    </div>
                  </div>

        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">PT İlan Türü Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_pt" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_pt" placeholder="İlan Türü Başlığı"  type="text" >
                    </div>
                  </div>
                </div>

           <hr>
                      <div class="col-md-7">
                        <div class="ln_solid"></div>
                              <div class="form-group row">
                                <div class="col-md-2 col-md-offset-1">
                                   <a href='/admin/turu/yonet' class="btn btn-danger">Geri Dön</a>
                                </div>
                                 <div class="col-md-6">
                                  <button type="submit" class="btn btn-success btn-block">Kaydet</button>
                                </div>
                              </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
  

           
  
  
@endsection

@section('footer')


@endsection