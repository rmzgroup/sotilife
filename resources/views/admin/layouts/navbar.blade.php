<!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.jpg" alt="">{{ Auth::user()->name }}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                 
                    <li><a data-toggle="modal" data-placement="top" title="Settings" data-target=".bs-one-modal-lg"><span>Change Your Password</span></a></li>

                    <li><a href="#"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();" ><i class="fa fa-sign-out pull-right"></i> Logout</a></li>

                     <form id="logout-form" action="{{ route('admin.auth.logout') }}" method="POST" style="display: none;">
                          @csrf
                           {{ csrf_field() }}
                      </form>

                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user"></i>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->


             <div class="modal fade bs-one-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">



              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Settings</h4>
              </div>
              <div class="modal-body">

                  <form class="form-horizontal" method="POST" action="{{ route('admin.changeAdminPassword') }}">
                    
                   

                        @csrf
                      <span class="section">Admin User Information</span>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name and Surname <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="both name(s) e.g Jon Doe" required="required" type="text" value="{{ Auth::user()->name }}" readonly="true">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12"  value="{{ Auth::user()->email }}" readonly="true">
                        </div>
                      </div>
                  <hr>

                  <span class="section">Change the Password</span>
                 
                      <div class="item form-group {{ $errors->has('current-password') ? ' has-error' : '' }}">
                        <label for="current-password" class="control-label col-md-3">Present Password</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="current-password" type="password" name="current-password" data-validate-length="5,8" class="form-control col-md-7 col-xs-12" required>
                        </div>
                      </div>
      
                      <div class="item form-group">
                        <label for="new-password" class="control-label col-md-3">New Password</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="new-password" type="password" name="password" class="form-control col-md-7 col-xs-12" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif

                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="new-password-confirm" class="control-label col-md-3 col-sm-3 col-xs-12">New Password Again</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="password-confirm" type="password" name="password_confirmation" class="form-control col-md-7 col-xs-12" required>
                        </div>
                      </div>



                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="submit" class="btn btn-success btn-block">Submit</button>
                        </div>
                      </div>
                    </form>
              </div>


              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>

            </div>
          </div>
        </div>
