 <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="/cpadmin/dashboard" class="site_title"><i class="fa fa-unlock"></i> <span>SOTYLIFE</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="/logo.png" alt="..." class="img-circle profile_img" height="50px;">
              </div>
              <div class="profile_info">
                <span>Welcome</span>
                <h2>{{ Auth::user()->name }}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

              <div class="menu_section">
                <h3>SOTYLIFE GAME</h3>
                <ul class="nav side-menu">
                  <li {{ str_contains(request()->url(), '/dashboard') ? 'active' : '' }}><a href="/cpadmin/dashboard"><i class="fa fa-home"></i> Dashboard</a></li>
                 

                 
                  <li>
                   <a><i class="fa fa-users"></i> Payment Notification <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="{{ route('admin.payment.list') }}">Payment Request List</a></li>
                        <li><a href="{{ route('admin.payment.confirmed') }}">Confirmed Payment List</a></li>

                    </ul>
                  </li>

                  <li>
                   <a><i class="fa fa-users"></i> Withdrawal Requests <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="{{ route('admin.withdrawal.list') }}">Pending Withdrawal Requests</a></li>   
                        <li><a href="{{ route('admin.payment.month') }}"> End of Month Payments</a></li>
                        <li><a href="{{ route('admin.withdrawal.confirmed') }}">Confirmed Withdrawal Requests</a></li>
                    </ul>
                  </li>

                 
                  <li><a href="{{ route('admin.getAllUsers') }}"><i class="fa fa-users"></i> All Users</a></li>

                </ul>
              </div>
              
              <div class="menu_section">
                <h3>House Rent Operations</h3>
                <ul class="nav side-menu">
                  
                   <li>
                     <a><i class="fa fa-cubes"></i>Rent Customers<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="{{ route('admin.house.reservation.list') }}">House Rent Application List</a></li>
                      </ul>
                  </li>

                  <li>
                     <a><i class="fa fa-th"></i> House Rent<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="{{ route('admin.house.all') }}">All Houses</a></li>
                          <li><a href="{{ route('admin.house.add') }}">Add a New House</a></li>
                          <li><a href="{{ route('admin.property.all') }}">House Properties</a></li>
                      </ul>
                    </li>

                </ul>
              </div> 


          
               <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">

                   <li><a><i class="fa fa-newspaper-o"></i> Information and News<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('admin.information.list') }}">All Information List</a></li>
                    </ul>
                  </li>

                </ul>
              </div>

           

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="modal" data-placement="top" title="Şifreyi Değiştir" data-target=".bs-one-modal-lg">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
           
              <a data-toggle="tooltip" data-placement="top" title="Çıkış Yap" href="#"  onclick="event.preventDefault(); document.getElementById('logout-form2').submit();">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->

               <form id="logout-form2" action="{{ route('admin.auth.logout') }}" method="POST" style="display: none;">
                    @csrf
                     {{ csrf_field() }}
                </form>



          </div>
        </div>

