@extends('admin/app')
@section('pagename','Kullanıcı Ekle')
@section('head')

@endsection

@section('mainBody')

  <!-- page content -->

            <div class="page-title">
              <div class="title_left">
                <h3>Kullanıcı Ekle</h3>
              </div> 
            </div>
            <div class="clearfix"></div>

    

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                <form role="form" action="{{ route('admin.kullanici.ekle.save') }}" method="post" >

          
               {{ csrf_field() }}
  
                <div class="row">

                 <div class="page-title">
                    <div class="kategori_ad_left">
                      <h3>Kullanıcı Bilgileri Girin</h3>
                    </div> 
                  </div>


                 <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">Ad Soyad</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="name" class="form-control col-md-7 col-xs-12"  name="name" placeholder="Ad Soyad"  type="text" required="true">
                    </div>
                  </div>

                    <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="email">E-Posta</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="email" class="form-control col-md-7 col-xs-12"  name="email" placeholder="Anakategori Başlığı"  type="email" required="true">
                    </div>
                  </div>


                  <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="password">Paralo</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="password" class="form-control col-md-7 col-xs-12"  name="password" placeholder="Ad Soyad"  type="password" required="true">
                    </div>
                  </div>

                </div>

        <hr>


                  <div class="col-md-7">
                    <div class="ln_solid"></div>
                          <div class="form-group row">
                            <div class="col-md-2 col-md-offset-1">
                               <a href='/admin/anakategori/yonet' class="btn btn-danger">Geri Dön</a>
                            </div>
                             <div class="col-md-6">
                              <button type="submit" class="btn btn-success btn-block">Kaydet</button>
                            </div>
                          </div>
                  </div>



                    
                    </form>
                  </div>
                </div>
              </div>
            </div>
  

           
  
  
@endsection

@section('footer')


@endsection