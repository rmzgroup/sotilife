@extends('admin/app')
@section('pagename','Kullanıcı Ulaş')
@section('head')



@endsection


@section('mainBody')
<!-- Content Wrapper. Contains page content -->

  <!-- Content Header (Page header) -->
  <!-- Main content -->
  <section class="content">
         <div class="x_panel" style="background-color: #2a3f54; min-height: 100px; color: white">
                   <div class="x_title">
                    <h2>{{ $contact->adsoyad }}  ile irtibata geçiniz</h2>
                    <div class="clearfix"></div>
                  </div>
            </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Başvuruları <small>Burak Dölek Diş Ticaret</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                <form class="form-horizontal"  method="POST" action="{{ route('admin.kullanici.email.gonder') }}">


                {{ csrf_field() }}
                
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Adı ve Soyad <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name"  required="required" type="text" value="{{ $contact->name }}">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">E-Posta <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="{{ $contact->email }}">
                        </div>
                      </div>
          
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Mesajınız <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="textarea" required="required" name="message" class="form-control col-md-7 col-xs-12" rows="10"></textarea>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <a href='/admin/kullanici/yonet' class="btn btn-danger">Geri Dön</a>
                          <button id="send" type="submit" class="btn btn-success"><i class="fa fa-paper-plane" aria-hidden="true"></i> GÖNDER</button>
                        </div>
                      </div>
                    </form>

                  </div>
                </div>
              </div>
  </section>
  <!-- /.content -->

<!-- /.content-wrapper -->

@endsection

@section('footer')

@endsection

