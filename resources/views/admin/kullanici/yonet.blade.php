@extends('admin/app')
@section('pagename','Users List')
@section('head')

<!-- Datatables -->
    <link href="../../../../admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

@endsection


@section('mainBody')
<!-- Content Wrapper. Contains page content -->

  <!-- Content Header (Page header) -->
  <!-- Main content -->
  <section class="content">
         <div class="x_panel" style="background-color: #2a3f54; min-height: 100px; color: white">
                   <div class="x_title">
                    <h2>All Users List</h2>
                    <div class="clearfix"></div>
                  </div>
            </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>S.No</th>
                          <th>Image</th>
                          <th>Name and Surname</th>
                          <th>Reference Code</th>
                          <th>Team Owner</th>
                          <th>Phone Number</th>
                          <th>City</th>
                          <th>Country</th>
                          <th>Member Since</th>
                          <th>User Details</th>
                          {{-- <th>Contact User</th> --}}
                          <th>Delete User</th>
                        </tr>
                      </thead>


                      <tbody>
                         @foreach ($users as $user)
                            <tr>
                              <td>{{ $loop->index + 1 }}</td>
                              <td><img src="{{ $user->avatar == '' ? $user->sex == "Male"  ? "/avatar-male.png" : "/avatar-female.png" : Storage::url($user->avatar) }}" alt="" height="35px;" style="border-radius: 50%"></td>
                              <td>{{ $user->name }}</td>
                              <td>{{ $user->code }}</td>
                              <td>@if ($user->user)
                                {{ $user->user->name }}
                                 @else
                                 ------
                                 @endif
                              </td>
                              <td>{{ $user->phone }}</td>
                              <td>{{ $user->city }}</td>
                              <td>{{ $user->country }}</td>
                              <td>{{ \Carbon\Carbon::parse($user->created_at)->format('d - M - Y')}}</td>
                        
                              <td><a href="{{ route('admin.user.checkDetails', $user->id) }}" class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> User Details</a></td>
                              
                              {{-- <td><a href="#" class="btn btn-block btn-success"><i class="fa fa-envelope" aria-hidden="true"></i> İrtitaba Geç</a></td> --}}
                             
                              <td>
                              <form id="delete-form-{{ $user->id }}" method="get" action="{{ route('admin.delete.user',$user->id) }}" style="display: none">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                              </form>
                              <a href="" onclick="
                              if(confirm('Bu başvuruyu silmek istediğinizden emin misiniz?'))
                                  {
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $user->id }}').submit();
                                  }
                                  else{
                                    event.preventDefault();
                                  }" class="btn btn-block btn-danger"><span class="glyphicon glyphicon-trash"></span> Sil</a>
                            </td>
                              </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
  </section>
  <!-- /.content -->

<!-- /.content-wrapper -->

@endsection

@section('footer')

 <!-- Datatables -->
    <script src="../../../../admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../../../admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../../../../admin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../../../admin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../../../admin/vendors/pdfmake/build/vfs_fonts.js"></script>
@endsection

