@extends('admin/app')
@section('pagename','User Details')
@section('head')

@endsection

@section('mainBody')


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                    <div class="x_content">






      <div class="col-md-8 col-sm-12 col-md-offset-2 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Details About: <strong>{{ $user->name }} </strong> <small> | Sotilife User</small></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <ul class="list-unstyled timeline">
                   
                  <div class="profile">
                  		<img src="{{ $user->avatar == '' ? $user->sex == "Male"  ? "/avatar-male.png" : "/avatar-female.png" : Storage::url($user->avatar) }}" alt="" height="200px;" style="border-radius: 50%">
                  </div>

                    <li>
                      <div class="block">
                        <div class="block_content">
                          <p class="excerpt">Name and Surname </p>
                          <h2 class="title"><a><strong>{{ $user->name }}</strong></a></h2> 
                        </div>
                      </div>
                    </li>

                  
                     <li>
                      <div class="block">
                        <div class="block_content">
                          <p class="excerpt">Team Owner </p>
                          <h2 class="title"><a><strong>{{ $user->user->name }}</strong></a></h2> 
                        </div>
                      </div>
                    </li>


                     <li>
                      <div class="block">
                        <div class="block_content">
                          <p class="excerpt">Reference Code</p>
                          <h2 class="title"><a><strong>{{ $user->code }}</strong></a></h2> 
                        </div>
                      </div>
                    </li>



                     <li>
                      <div class="block">
                        <div class="block_content">
                          <p class="excerpt">Team Users</p>

                          @foreach ($teamUsers as $team)
                        	  <h2 class="title"><a><strong>{{ $team->name }}</strong></a></h2> 
                          @endforeach
                        </div>
                      </div>
                    </li>

                     <li>
                      <div class="block">
                        <div class="block_content">
                          <p class="excerpt">Phone </p>
                          <h2 class="title"><a><strong>{{ $user->phone }}</strong></a></h2> 
                        </div>
                      </div>
                    </li>

                     <li>
                      <div class="block">
                        <div class="block_content">
                          <p class="excerpt">City </p>
                          <h2 class="title"><a><strong>{{ $user->city }}</strong></a></h2> 
                        </div>
                      </div>
                    </li>

                     <li>
                      <div class="block">
                        <div class="block_content">
                          <p class="excerpt">Country </p>
                          <h2 class="title"><a><strong>{{ $user->country }}</strong></a></h2> 
                        </div>
                      </div>
                    </li>

                     <li>
                      <div class="block">
                        <div class="block_content">
                          <p class="excerpt">Email </p>
                          <h2 class="title"><a><strong>{{ $user->email }}</strong></a></h2> 
                        </div>
                      </div>
                    </li>

                     <li>
                      <div class="block">
                        <div class="block_content">
                          <p class="excerpt">Present Team Investment </p>
                          <h2 class="title"><a><strong>{{ $user->soties_number }}</strong></a></h2> 
                        </div>
                      </div>
                    </li>


                     <li>
                      <div class="block">
                        <div class="block_content">
                          <p class="excerpt">Present User Investment Number </p>
                          <h2 class="title"><a><strong>{{ $user->petek_number }}</strong></a></h2> 
                        </div>
                      </div>
                    </li>

                     <li>
                      <div class="block">
                        <div class="block_content">
                          <p class="excerpt">Coin Address</p>
                          <h2 class="title"><a><strong>{{ $user->type }} | {{ $user->address }}</strong></a></h2> 
                        </div>
                      </div>
                    </li>

                     <li>
                      <div class="block">
                        <div class="block_content">
                          <h2><strong>User Investments</strong></h2> <br>

                          	@foreach ($userInvestments as $investment)
                         		 <h2 class="title"><a>
                         		 	<strong>Amount: {{ $investment->amount_usd }} USD</strong><br><br>
                         		 	<strong>Coin Amount: {{ $investment->amount_crypto }} {{ $investment->coin_type }}</strong><br><br>
                         		 </a></h2> 
                         				<br><br>	
                          	@endforeach

                        </div>
                      </div>
                    </li>


                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2><strong>User Withdraws</strong></h2> <br>

                          	@foreach ($userWithdraws as $withdraw)
                         		 <h2 class="title"><a>
                         		 	<strong>Amount: {{ $withdraw->amount_usd }} USD</strong><br><br>
                         		 	<strong>Withdraw Date: {{ $withdraw->updated_at }}</strong><br><br>
                         		 </a></h2> 
                         		<br><br>
                          	@endforeach

                        </div>
                      </div>
                    </li>

                     <li>
                      <div class="block">
                        <div class="block_content">
                          <p class="excerpt">Member Since: </p>
                          <h2 class="title"><a><strong>{{ \Carbon\Carbon::parse($user->created_at)->format('d - m - Y')}}</strong></a></h2> 
                        </div>
                      </div>
                    </li>



             
                  </ul>


                    <div class="col-md-12">
                    <div class="ln_solid"></div>
                          <div class="form-group row">
                            <div class="col-md-2 col-md-offset-1">
                               <a href='{{ route('admin.getAllUsers') }}' class="btn btn-danger">Back</a>
                            </div>
                          </div>
                  </div>


                </div>
              </div>
            </div>
       
           </div>
        </div>
      </div>
    </div>

@endsection

@section('footer')


@endsection