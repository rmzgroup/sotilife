@extends('admin/app')
@section('pagename','Haberler')
@section('head')
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/css/prism.css') }}">

@endsection

@section('mainBody')


  <!-- page content -->

            <div class="page-title">
              <div class="title_left">
                <h3>Haber Düzenle</h3>
              </div> 
            </div>
            <div class="clearfix"></div>

    

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                <form role="form" action="{{ route('admin.haber.duzenle.save', $id ) }}" method="post" enctype="multipart/form-data" name="edit">

          
          {{ csrf_field() }}
  

                <div class="row">

                    <div class="page-title">
                    <div class="title_left">
                      <h3>HABER BAŞLIĞI</h3>
                    </div> 
                  </div>
                  
                 <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">TR Haber Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_tr" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_tr" placeholder="Haber Başlığı"  type="text" value="{{ $update->title_tr }}">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">EN Haber Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_en" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_en" placeholder="Haber Başlığı"  type="text" value="{{ $update->title_en }}">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">FR Haber Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_fr" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_fr" placeholder="Haber Başlığı"  type="text" value="{{ $update->title_fr }}">
                    </div>
                  </div>

           <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">AR Haber Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_ar" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_ar" placeholder="Haber Başlığı"  type="text" value="{{ $update->title_ar }}">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">ES Haber Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_es" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_es" placeholder="Haber Başlığı"  type="text" value="{{ $update->title_es }}">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">DE Haber Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_de" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_de" placeholder="Haber Başlığı"  type="text" value="{{ $update->title_de }}">
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">RU Haber Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_ru" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_ru" placeholder="Haber Başlığı"  type="text" value="{{ $update->title_ru }}">
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">FA Haber Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_fa" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_fa" placeholder="Haber Başlığı"  type="text" value="{{ $update->title_fa }}">
                    </div>
                  </div>

        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">PT Haber Baslığı</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="title_pt" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title_pt" placeholder="Haber Başlığı"  type="text" value="{{ $update->title_pt }}" >
                    </div>
                  </div>
                </div>

        <hr>


                <div class="row">

                    <div class="page-title">
                    <div class="title_left">
                      <h3>HABER SLUG</h3>
                    </div> 
                  </div>
                  
                 <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">TR Haber Slug</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="slug_tr" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="slug_tr" placeholder="Haber Başlığı"  type="text" value="{{ $update->slug_tr }}">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">EN Haber Slug</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="slug_en" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="slug_en" placeholder="Haber Başlığı"  type="text" value="{{ $update->slug_en }}">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">FR Haber Slug</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="slug_fr" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="slug_fr" placeholder="Haber Başlığı"  type="text" value="{{ $update->slug_fr }}">
                    </div>
                  </div>

           <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">AR Haber Slug</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="slug_ar" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="slug_ar" placeholder="Haber Başlığı"  type="text" value="{{ $update->slug_ar }}">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">ES Haber Slug</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="slug_es" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="slug_es" placeholder="Haber Başlığı"  type="text" value="{{ $update->slug_es }}">
                    </div>
                  </div>

                     <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">DE Haber Slug</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="slug_de" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="slug_de" placeholder="Haber Başlığı"  type="text" value="{{ $update->slug_de }}">
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">RU Haber Slug</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="slug_ru" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="slug_ru" placeholder="Haber Başlığı"  type="text" value="{{ $update->slug_ru }}">
                    </div>
                  </div>

                   <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">FA Haber Slug</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="slug_fa" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="slug_fa" placeholder="Haber Başlığı"  type="text" value="{{ $update->slug_fa }}">
                    </div>
                  </div>

        <div class="item form-group col-md-4 col-sm-4 col-xs-12">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">PT Haber Slug</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <input id="slug_pt" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="slug_pt" placeholder="Haber Başlığı"  type="text" value="{{ $update->slug_pt }}" >
                    </div>
                  </div>
                </div>

                </div>

                <hr> 

                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>TR Haber<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="text_tr" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor1" required> {{ $update->text_tr }}</textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                 <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>EN Haber<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="text_en" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor2" required> {{ $update->text_en }}</textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                <hr>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>FR Haber<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="text_fr" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor3" required> {{ $update->text_fr }}</textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>AR Haber<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="text_ar" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor4" required> {{ $update->text_ar }}</textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                 <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>ES Haber<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="text_es" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor5" required> {{ $update->text_es }}</textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>DE Haber<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="text_de" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor6" required> {{ $update->text_de }}</textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>RU Haber<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="text_ru" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor7" required> {{ $update->text_ru }}</textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                      <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>FA Haber<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="text_fa" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor8" required> {{ $update->text_fa }}</textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>

                      <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>PT Haber<small>metni</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="box-body pad">
                       <textarea name="text_pt" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor9" required> {{ $update->text_pt }}</textarea>
                     </div>
                            
                    </div>
                  </div>
                </div>



            <div class="col-md-6">
                <div class="row" style="background-color: #2a3f54; margin:20px; padding:30px; margin-top: 150px;">
                     <div class="col-md-6">
                      <img src="@if($update->image != "") ../../../haberFotograflari/{{ $update->image }} @endif" style="width: 200px">
                    </div>
                     <div class="box-body col-md-6">
                        <label for="title">Resim Değiştir - (800, 600)</label>
                        <input type="file" name="image" id="image" accept="image/x-png,image/gif,image/jpeg,image/jpg,image/svg,image/png" >
                    </div>
               </div>
            </div>


                 



                  <div class="col-md-7">
                    <div class="ln_solid"></div>
                          <div class="form-group row">
                            <div class="col-md-2 col-md-offset-1">
                               <a href='/admin/anasayfa' class="btn btn-danger">Geri Dön</a>
                            </div>
                             <div class="col-md-6">
                              <button type="submit" class="btn btn-success btn-block">Kaydet</button>
                            </div>
                          </div>
                  </div>



                    
                    </form>
                  </div>
                </div>
              </div>
            </div>
  
  
@endsection

@section('footer')

 <!-- bootstrap-wysiwyg -->
<script src="{{ asset('admin/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>

<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor1');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor2');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor3');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor4');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor5');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor6');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor7');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor8');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>

<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor9');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>

{{-- <script>
  $(document).ready(function() {
    $(".select2").select2();
  });
</script> --}}



@endsection