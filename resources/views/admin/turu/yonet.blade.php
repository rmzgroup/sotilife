@extends('admin/app')
@section('pagename','İlan Türü')
@section('head')

<!-- Datatables -->
    <link href="../../../../admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../../../admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

@endsection


@section('mainBody')
<!-- Content Wrapper. Contains page content -->

  <!-- Content Header (Page header) -->
  <!-- Main content -->
  <section class="content">
         <div class="x_panel" style="background-color: #2a3f54; min-height: 100px; color: white">
                   <div class="x_title">
                    <h2>İlan Türü Yönetimi</h2>
                    <div class="clearfix"></div>
                  </div>

                  <div class="col-md-3">
                    <a href="/admin/turu/ekle" title="Yeni turu ekle" class="btn btn-block btn-success"> Yeni İlan Türü Ekle</a>
                  </div>
            </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>İlan Türü Listesi <small>Bd Estate</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-buttons" data-page-length='50' class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>S.No</th>
                          <th>İlan Türü Adı</th>
                          <th>Oluşturma Tariği</th>
                          <th>Durum</th>
                          <th>Düzenle</th>
                          <th>Sil</th>
                        </tr>
                      </thead>


                      <tbody>
                          @foreach ($types as $type)
                            <tr>
                              <td>{{ $loop->index + 1 }}</td>
                       
                              <td>{{ $type->title_tr }}</td>

                              <td>{{ $type->created_at }}</td>

               

                              <td>
                                   @if($type->type_active == 1) 

                                    <a href="{{ route('admin.turu.deactivate', $type->id) }}" class="btn btn-info btn-block" title="Etkisiz haline getir"><i class="fa fa-level-up" aria-hidden="true"></i> Aktif</a>

                                     @else

                                     <a href="{{ route('admin.turu.activate', $type->id) }}" class="btn btn-danger btn-block"><i class="fa fa-level-down" aria-hidden="true" title="Aktif haline getir"></i> Aktif Değil</a>

                                   @endif
                              </td>
                        
                             <td><a href="{{ route('admin.turu.duzenle', $type->id) }}" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-edit"></span> Düzelt</a></td>
                             
                              <td>
                              <form id="delete-form-{{ $type->id }}" method="get" action="{{ route('admin.turu.delete', $type->id) }}" style="display: none">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                              </form>
                              <a href="" onclick="
                              if(confirm('Silmek istediğinizden emin misiniz?'))
                                  {
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $type->id }}').submit();
                                  }
                                  else{
                                    event.preventDefault();
                                  }" class="btn btn-danger btn-block"><span class="glyphicon glyphicon-trash"></span> Sil</a>
                            </td>
                              </tr>


                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


                  <!-- Small modal -->
                  

         
  </section>
  <!-- /.content -->

<!-- /.content-wrapper -->

@endsection

@section('footer')

 <!-- Datatables -->
    <script src="../../../../admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../../../admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../../../admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../../../../admin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../../../admin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../../../admin/vendors/pdfmake/build/vfs_fonts.js"></script>
@endsection

