<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="il_id"]').on('click', function() {
            var il_id = $(this).val();
           // console.log(il_id);
            
            if(il_id) {
                $.ajax({
                    url: 'il/'+il_id,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                            
                    // console.log(data);

                        
                        $('select[name="ilce_id"]').empty();

                        $.each(data, function(key, value) {
                            // console.log("LE KEY: " + key);
                          // console.log("lA VALEUR: "+ value);
                            
                            $('select[name="ilce_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });

                    }
                });
                
            }else{
                $('select[name="ilce_id"]').empty();
            }
        });
    });
</script>