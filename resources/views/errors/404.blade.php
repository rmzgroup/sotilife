<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic page needs -->
<meta charset="utf-8">
<!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <![endif]-->
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Decorate - Furniture & Interior Website Templates</title>
<meta name="description" content="Explore the live preview of the professional Modern - Furniture & Interior Website Templates get yourself knowledgeable about this theme prior to buying it. Browse through the pages, check out the images, click the buttons, explore the features.">
<meta name="keywords" content="ecommerce, business, clean, company, contractor, corporate, creative, flat design, furniture, interior, minimal, fashion, furniture store, interior store, Kitchen Store, shopping template, modern, shopping"/>

<!-- Mobile specific metas  -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Favicon  -->
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

<!-- Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<!-- CSS Style -->

<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="../../../user/css/bootstrap.min.css">

<!-- font-awesome & simple line icons CSS -->
<link rel="stylesheet" type="text/css" href="../../../user/css/font-awesome.css" media="all">
<link rel="stylesheet" type="text/css" href="../../../user/css/simple-line-icons.css" media="all">

<!-- owl.carousel CSS -->
<link rel="stylesheet" type="text/css" href="../../../user/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="../../../user/css/owl.theme.css">
<link rel="stylesheet" type="text/css" href="../../../user/css/owl.transitions.css">

<!-- animate CSS  -->
<link rel="stylesheet" type="text/css" href="../../../user/css/animate.css" media="all">

<!-- jquery-ui.min CSS  -->
<link rel="stylesheet" type="text/css" href="../../../user/css/jquery-ui.css">

<!-- style CSS -->
<link rel="stylesheet" type="text/css" href="../../../user/css/style.css" media="all">
</head>


<body class="404error_page">
  


  <!--Container -->
  <div class="error-page" style="margin-top: 150px;">
    <div class="container">
      <div class="error_pagenotfound"> <strong>4<span id="animate-arrow">0</span>4 </strong> <br />
        <b>Oops... Nour Not Found!</b> 
        <br />
        <a href="index" class="button-back"><i class="fa fa-arrow-circle-left fa-lg"></i>&nbsp; <i class="fa fa-home fa-lg"></i></a> </div>
      <!-- end error page notfound --> 
      
    </div>
  </div>
  <!-- Container End --> 


<!-- jquery js --> 
<script type="text/javascript" src="../../../user/js/jquery.min.js"></script> 

<!-- bootstrap js --> 
<script type="text/javascript" src="../../../user/js/bootstrap.min.js"></script> 

<!-- owl.carousel.min js --> 
<script type="text/javascript" src="../../../user/js/owl.carousel.min.js"></script> 

<!-- bxslider js --> 
<script type="text/javascript" src="../../../user/js/jquery.bxslider.js"></script> 

<!-- jquery.mobile-menu js --> 
<script type="text/javascript" src="../../../user/js/mobile-menu.js"></script> 

<!--jquery-ui.min js --> 
<script type="text/javascript" src="../../../user/js/jquery-ui.js"></script> 

<!-- main js --> 
<script type="text/javascript" src="../../../user/js/main.js"></script>

</body>