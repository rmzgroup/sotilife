@extends('members/app')


@section('head')
	<!-- Dropify -->
	<link rel="stylesheet" href="{{ asset('assets/plugin/dropify/css/dropify.min.css') }}">

	<!-- DateRangepicker -->
	<link rel="stylesheet" href="{{ asset('assets/plugin/datepicker/css/bootstrap-datepicker.min.css') }}">
		<!-- Select2 -->
	<!-- Sweet Alert -->
	<link rel="stylesheet" href="{{ asset('assets/plugin/sweet-alert/sweetalert.css') }}">

@endsection

@section('pagename','Edit My Profile')
@section('mainBody')

  <!-- page content -->
<div class="row small-spacing">

			 <form method="POST" action="{{ route('member.profileSave') }}" enctype="multipart/form-data">
                    
                    @csrf

			<div class="col-md-3 col-xs-12">
				<div class="box-content bordered primary margin-bottom-20">


					<div class="profile-avatar">
							<div class="box-content">
								<h4 class="box-title">Profile Picture</h4>
								<!-- /.box-title -->
								<!-- /.dropdown js__dropdown -->

								<input type="file" id="input-file-now-custom-1" name="avatar" class="dropify" data-default-file="{{ Auth::user()->avatar == '' ? Auth::user()->sex == "Male"  ? "/avatar-male.png" : "/avatar-female.png" : Storage::url(Auth::user()->avatar) }}" data-allowed-formats="portrait square" data-max-file-size="2M" data-max-height="2000" />
								<p class="help margin-top-10">Only portrait or square images, 2M max and 2000px max-height.</p>
							</div>
							<!-- /.box-content -->

						    <h4>Ref Code:</h4>
							<input id="ref-code" type="text" class=" btn btn-block btn-inbox margin-bottom-20 form-control" value="https://sotylife.com/register?code={{ Auth::user()->code }}" readonly title="Click to copy the code">

						<h3><strong>{{ Auth::user()->name }}</strong></h3>
						<h4>{{ Auth::user()->occupation }}</h4>
						<h5>{{ Auth::user()->email }}</h5>
					</div>


					<!-- .profile-avatar -->
					<table class="table table-hover no-margin">
						<tbody>
							<tr>
								<td>Status</td>
								<td><span class="investment-{{ $soties >= 1 ? 'Active' : 'Inactive' }}"> {{ $soties >= 1 ? 'Active' : 'Passive' }} </span>
								</td>
							</tr>
							<tr>
								<td>Member Since</td>
								<td>{{ \Carbon\Carbon::parse(Auth::user()->created_at)->format('d - m - Y')}}</td>
							</tr>

						</tbody>
					</table>
				</div>
				<!-- /.box-content bordered -->

			
				<!-- /.box-content -->
			</div>
			<!-- /.col-md-3 col-xs-12 -->
			<div class="col-md-4 col-xs-12">
			
				<div class="box-content card white">
					<h4 class="box-title">Edit My Profile</h4>
					<!-- /.box-title -->
			
							<div class="card-content">
						<div class="input-group margin-bottom-20">


							  @if ($errors->has('name'))
			                        <span class="invalid-feedback" role="alert">
			                            <strong>{{ $errors->first('name') }}</strong>
			                        </span>
			                    @endif


							<input type="text" class="form-control" name="name" placeholder="Name and Surname" value="{{ Auth::user()->name }}">
							<div class="input-group-btn"><button type="button" class="btn btn-violet no-border waves-effect bg-navy-blue"><i class="fa fa-meh-o text-white"></i></button></div>
							<!-- /.input-group-btn -->
						</div>

						<div class="input-group margin-bottom-20">

							  @if ($errors->has('phone'))
			                        <span class="invalid-feedback" role="alert">
			                            <strong>{{ $errors->first('phone') }}</strong>
			                        </span>
			                    @endif


							<input type="number" class="form-control" name="phone" placeholder="Phone Number"  value="{{ Auth::user()->phone }}">
							<div class="input-group-btn"><button type="button" class="btn btn-success no-border waves-effect bg-navy-blue"><i class="fa fa-envelope text-white"></i></button></div>
							<!-- /.input-group-btn -->
						</div>


						<div class="input-group margin-bottom-20">
							  @if ($errors->has('country'))
			                        <span class="invalid-feedback" role="alert">
			                            <strong>{{ $errors->first('country') }}</strong>
			                        </span>
			                    @endif

							<input type="text" class="form-control" name="country" placeholder="Country"  value="{{ Auth::user()->country }}">
							<div class="input-group-btn"><button type="button" class="btn btn-violet no-border waves-effect bg-navy-blue"><i class="fa fa-globe text-white"></i></button></div>
							<!-- /.input-group-btn -->
						</div>


						<div class="input-group margin-bottom-20">
							  @if ($errors->has('city'))
			                        <span class="invalid-feedback" role="alert">
			                            <strong>{{ $errors->first('city') }}</strong>
			                        </span>
			                    @endif

							<input type="text" class="form-control" name="city" placeholder="City"  value="{{ Auth::user()->city }}">
							<div class="input-group-btn"><button type="button" class="btn btn-violet no-border waves-effect bg-navy-blue"><i class="fa fa-map-o text-white"></i></button></div>
							<!-- /.input-group-btn -->
						</div>

						<!-- /.input-group -->
						<div class="input-group margin-bottom-20">
							  @if ($errors->has('email'))
			                        <span class="invalid-feedback" role="alert">
			                            <strong>{{ $errors->first('email') }}</strong>
			                        </span>
			                    @endif

							<input type="email" class="form-control" name="email" placeholder="Email address" value="{{ Auth::user()->email }}" readonly>
							<div class="input-group-btn"><button type="button" class="btn btn-success no-border waves-effect bg-navy-blue"><i class="fa fa-envelope text-white"></i></button></div>
							<!-- /.input-group-btn -->
						</div>

						<button type="submit" class="btn btn-icon btn-icon-right btn-success btn-sm waves-effect pull-right margin-bottom-20"><i class="ico fa fa-save"></i>Edit My Profile</button>



					</div>

					</form>
					<!-- /.card-content -->




				</div>
				<!-- /.box-content card white -->
			
				<!-- /.row -->
			</div>
			<!-- /.col-md-9 col-xs-12 -->

			</form>

			<div class="col-md-3 col-xs-12">
				<div class="box-content card white">
					<h4 class="box-title">Change Your Password</h4>
					<!-- /.box-title -->
					<div class="card-content">
				 		<form method="POST" action="{{ route('member.changePassword') }}">
							@csrf
							<div class="form-group">
								<label for="exampleInputEmail1">Enter your Password</label>

								<input type="password" class="form-control"  id="current-password" type="password" name="current-password" placeholder="Enter your Password" required="true">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Enter the New Password</label>

			                     @if ($errors->has('password'))
			                        <span class="invalid-feedback" role="alert">
			                           <p><strong>{{ $errors->first('password') }}</strong></p>
			                        </span>
			                    @endif

								<input type="password" class="form-control" type="password" name="password" placeholder="Enter the New Password" required="true">
							</div>

							<div class="form-group">
								<label for="exampleInputPassword1">Confirm your Password</label>

								<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm your Password" required="true">
							</div>
							<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light pull-right margin-bottom-20"> <i class="ico fa fa-save"></i> Change Password</button>
						</form>
					</div>
					<!-- /.card-content -->
				</div>
				<!-- /.box-content -->
			</div>



			
		</div>
  	


  <!-- /page content -->
@endsection

@section('footer')



	<!-- Dropify -->
	<script src="{{ asset('assets/plugin/dropify/js/dropify.min.js') }}"></script>
	<script src="{{ asset('assets/scripts/fileUpload.demo.min.js') }}"></script>

	<!-- Datepicker -->
	<script src="{{ asset('assets/plugin/datepicker/js/bootstrap-datepicker.min.js') }}"></script>

	<!-- Demo Scripts -->
	<script src="{{ asset('assets/scripts/form.demo.min.js') }}"></script>

	<script src="{{ asset('assets/plugin/sweet-alert/sweetalert.min.js') }}"></script>

	<script type="text/javascript">
		document.getElementById("ref-code").onclick = function() {
		    this.select();
		    document.execCommand('copy');
		    swal("Copied!", "Your Reference code has been Copied", "success");
		}
	</script>
@endsection