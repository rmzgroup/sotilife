
<!DOCTYPE html>
<html lang="tr">

<head>

@include('members/layouts/head')

</head>

<body>
    <div class="main-menu">

            @include('members/layouts/header-profile') 

            <div class="content">
              
                 @include('members/layouts/sidebar') 

            </div>

    </div> 


    @include('members/layouts/fixed-navbar') 
    @include('members/layouts/popup') 


     <div id="wrapper">
            <div class="main-content">
              
               @section('mainBody')@show

               @include('members/layouts/footer')   
               
            </div>
     </div>
      
 	
    @include('members/layouts/scripts') 

</body>

</html>
