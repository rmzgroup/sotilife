@extends('members/app')
@section('head')
	<!-- Table Responsive -->
	<link rel="stylesheet" href="{{ asset('assets/plugin/RWD-table-pattern/css/rwd-table.min.css') }}">
	<style type="text/css" media="screen">
		.btn {
			padding: 5px;
		}

		.withdraw-Inactive{
			font-weight: 700;
			color: green;
		}
	</style>
@endsection
@section('pagename','About the System')
@section('mainBody')

  <!-- page content -->
    <!-- page content -->
  	<div class="col-lg-12 col-md-12 col-xs-12 ui-sortable-handle">
			<div class="box-content card bordered-all success">
			<h4 class="box-title bg-success"><i class="ico fa fa-usd"></i>About the Sotylife Gain System</h4>
			<!-- /.box-title -->
			<!-- /.dropdown js__dropdown -->
				<div class="card-content">
					<div class="box-content">
						<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:14.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Sotylife Gain System</span></span></strong></span></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">SOTYLİFE LLC is an investment platform that has a unique strategy that will enable investors to acquire high efficiency markets.</span></span></strong></span></span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">The company uses its assets to diversify and generate maximum profit. Investments and Real Estate Consultancy, Rentacar, Tourism and Crypto &amp; Forex buy and sell operations combine the tangible and intangible assets of investors. Thus, it contributes to the high standard of living of investors.</span></span></span></span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">The Sotylife Gain System contains two different investment systems. Monthly Earnings and Team earnings system.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">A-</span></span></strong> <strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">MONTHLY GAIN SYSTEM</span></span></strong></span></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">1-The investor enters the system with a minimum of $ 50. If the investor does not register to the system within one month from the date of entry into the system, he / she earns 60 $.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">2- You can request instant shooting of $60 reflected on your wallet. If he wants to continue to win, he will re-enter the system with $ 50.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">3- In a month, the earnings according to the number of members included in the system varies.</span></span></span></span></p>

<p style="text-align:justify">&nbsp;</p>

<table align="center" border="1" cellspacing="0">
	<tbody>
		<tr>
			<td style="vertical-align:top; width:127.55pt">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot; "><strong>User Number</strong></span></span></span></span></p>
			</td>
			<td style="vertical-align:top; width:173.65pt">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot; text-align: center;">End of month Earning</span></span></strong></span></span></p>

			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:127.55pt">
			<ol>
				<li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Member </span></span></span></span></li>
			</ol>
			</td>
			<td style="vertical-align:top; width:173.65pt">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">65 $</span></span></strong></span></span></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:127.55pt">
			<ol>
				<li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Member</span></span></span></span></li>
			</ol>
			</td>
			<td style="vertical-align:top; width:173.65pt">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">70 $</span></span></strong></span></span></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:127.55pt">
			<ol>
				<li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Member</span></span></span></span></li>
			</ol>
			</td>
			<td style="vertical-align:top; width:173.65pt">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">75 $</span></span></strong></span></span></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:127.55pt">
			<ol>
				<li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Member</span></span></span></span></li>
			</ol>
			</td>
			<td style="vertical-align:top; width:173.65pt">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">80 $</span></span></strong></span></span></p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">4- In one month the investor earns a monthly income of up to 3 members. Investors are required to invest a minimum of $ 50 each month in order to gain earnings.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">5- If the investor has registered two persons in the system within one month, he / she can demand a withdrawal of $ 70 at the end of the month. When these people invest each month, the member will gain earnings again.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">6 - Monthly earnings system is a system adapted for people who can not make a member. Once the investor completes the 4th member, the earnings of these members will start to gain earnings through the TEAM GAIN SYSTEM.</span></span></span></span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">A-TEAM GAINS</span></span></strong></span></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">In this system, where the investor will gain a large profit, a steady gain is achieved.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">1-The investor enters the system with a minimum of $ 50. The system is based on the 4 membership system. When the investor builds a team of 4, he earns $ 80 in his wallet.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">2-Since each member has $ 20 dollars in earnings, he or she can request an instant withdraw on a 4th person membership.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">3-The more the number of teams the investor has done, the more he wins. His earnings reamains related these people, so he will continue to win $ 20 each time his team invests 4 times</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">4- In order to benefit from this team, he must perfom a soty investsment of $ 50 per 4-person team and earns $ 80.</span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">5-There is no loss in this gain system, if every individual investor makes a team of 4 people, he / she gets his / her income instantly. When his team invests continuously, the investor earns $ 30 on each soty investment.</span></span></span></span></p>

					</div>
				
				</div> 
			</div>
			<!-- /.card-content -->
		</div>

  <!-- /page content -->
@endsection

@section('footer')
	<!-- Responsive Table -->
@endsection