@extends('members/app')
@section('head')
	<!-- Table Responsive -->
	<link rel="stylesheet" href="{{ asset('assets/plugin/RWD-table-pattern/css/rwd-table.min.css') }}">
	<style type="text/css" media="screen">
		.btn {
			padding: 5px;
		}

		.withdraw-Inactive{
			font-weight: 700;
			color: green;
		}
	</style>
@endsection
@section('pagename','Withdraw')
@section('mainBody')

  <!-- page content -->
    <!-- page content -->
  	<div class="col-lg-12 col-md-12 col-xs-12 ui-sortable-handle">
			<div class="box-content card bordered-all success">
			<h4 class="box-title bg-success"><i class="ico fa fa-usd"></i>My Investments</h4>
			<!-- /.box-title -->
			<!-- /.dropdown js__dropdown -->
				<div class="card-content">
					<div class="box-content">
					<div class="table-responsive" data-pattern="priority-columns">
						<table id="tech-companies-1" class="table table-small-font table-bordered table-striped">
							<thead>
								<tr>
									<th data-priority="1">Amount USD</th>
									<th data-priority="1">Amount Crypto</th>
									<th data-priority="1">Active Team Investment</th>
									<th data-priority="1">Earnings</th>
									{{-- <th data-priority="4">Status</th> --}}
									<th data-priority="5">Wallet Address</th>
									<th data-priority="5">Investment Date</th>
									<th data-priority="1">Withdraw Request</th>
								</tr>
							</thead>
							<tbody>
								{{-- <h1>{{ $activeTeamUsers }}</h1> --}}
								
							{{-- 	@php
									$remainder = $activeTeamUsers % 4;
									$number = ($activeTeamUsers - $remainder) / 4;
								@endphp --}}

								
								{{-- <h1>{{ $remainder }} => {{ App\Members\Investment::getGainPerActiveTeamUsers($remainder) }}</h1> --}}

								@foreach ($casheWithdrawals as $withdrawal)
									
								
								<tr>
									<th>50 USD </th>
									<td>
											{{-- @php
												$number_soties = $withdrawal->soty->amount_usd / 50;
												echo $withdrawal->soty->amount_crypto / $number_soties;
											@endphp --}}
									</td>
									
									<td>

										{{-- @if($number>=1)
										 
										 	4 Users withdrawals
											
										@elseif($remainder>0)
											{{ $remainder }}  Users withdrawals
											
										@else
											0 Users withdrawals
										@endif --}}

									</td>

									<td>

										{{-- @if($number>=1)
											{{ $amount_usd =  App\Members\withdrawal::getGainPerActiveTeamUsers(4) }}


											@php

												$number = $number - 1;
												$setPaymentButtom = 1;
											@endphp
										@elseif($remainder>0)
											{{ $amount_usd = App\Members\withdrawal::getGainPerActiveTeamUsers($remainder) }}
											@php
												$remainder = $remainder - 4;
												$setPaymentButtom = 0;
											@endphp
										@else
											@php
												$setPaymentButtom = 0;
											@endphp
											{{ $amount_usd = App\Members\withdrawal::getGainPerActiveTeamUsers(0) }}
										@endif --}}

									 USD

									</td>
									{{-- <td><span class="withdrawal-{{ $withdrawal->status == 'Active' ? 'Active' : 'Inactive' }}"> {{ $withdrawal->status }} </span></td> --}}
									<td>******</td>
									 <td>{{ \Carbon\Carbon::parse($withdrawal->created_at)->format('d - m - Y')}}</td>
									<td>
										@if ($setPaymentButtom == 1)

											 <a href="{{ route('member.requsetCasheWithdrawal', $withdrawal->id, $amount_usd ) }}" class="btn btn-block btn-success"><i class="fa fa-check-square" aria-hidden="true"></i> Request Payment</a>
										@elseif(\Carbon\Carbon::now() >= \Carbon\Carbon::parse($withdrawal->created_at)->addMonths(1))
										 {{ $amount_usd }}
											 <a href="{{ route('member.requsetCasheWithdrawal', [$withdrawal->id, $amount_usd] ) }}" class="btn btn-block btn-success"><i class="fa fa-check-square" aria-hidden="true"></i> Request Payment</a>
										@else

											 <span class="payment-request">You can request for payment after the {{ \Carbon\Carbon::parse($withdrawal->created_at)->addMonths(1)->format('d - m - Y') }}</span>
											 
										@endif
									</td>	
								</tr>   
								
								@endforeach

							</tbody>
						</table>
					</div> 
				</div>
				
				</div> 
			</div>
			<!-- /.card-content -->
		</div>


		<div class="col-lg-12 col-md-12 col-xs-12 ui-sortable-handle">
			<div class="box-content card bordered-all success">
			<h4 class="box-title bg-success"><i class="ico fa fa-usd"></i>Withdraw</h4>
			<!-- /.box-title -->
			<!-- /.dropdown js__dropdown -->
				<div class="card-content">
					<div class="box-content">
					<div class="table-responsive" data-pattern="priority-columns">
						<table id="tech-companies-1" class="table table-small-font table-bordered table-striped">
							<thead>
								<tr>
									<th data-priority="1">Amount</th>
									<th data-priority="1">Earning</th>
									<th data-priority="1">Request Date</th>
									<th data-priority="4">Send</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($casheWithdrawals as $casheWithdrawal)

										<tr>
											<th>50 USD</th>
											<td>{{ $gainPerActiveTeamUsers }} USD</td>
											<td>{{ \Carbon\Carbon::parse($casheWithdrawal->created_at)}}</td>
											<td><span class="withdraw-{{ $casheWithdrawal->status }}">Waiting for Approval </span></td>
										</tr> 

								@endforeach
							
							</tbody>
						</table>
					</div> 
				</div>
				
				</div> 
			</div>
			<!-- /.card-content -->
		</div>

		<div class="col-lg-12 col-md-6 col-xs-12 ui-sortable-handle">
			<div class="box-content card bordered-all success">
				<h4 class="box-title bg-success"><i class="ico fa fa-usd"></i>Investment Earnings</h4>
				<!-- /.box-title -->
				<!-- /.dropdown js__dropdown -->
				<div class="card-content">
					
						<table id="tech-companies-2" class="table table-small-font table-bordered table-striped">
							<thead>
								<tr>
									<th data-priority="1">Amount</th>
									<th data-priority="1">Earning</th>
									<th data-priority="1">Request Date</th>
									<th data-priority="1">Confirmation Date</th>
								</tr>
							</thead>

							<tbody>
								{{-- @php
									$remainder = $numberOfPaidSoties % 4;
									$number = ($numberOfPaidSoties - $remainder) / 4;
								@endphp --}}

								@foreach ($casheWithdrawals as $casheWithdrawal)

									  	<tr>
											<td>50 USD</td>
											<td>
												{{-- @if($number>=1)
													{{ App\Members\Investment::getGainPerActiveTeamUsers(4) }}

													@php
														$number = $number - 1;
														$setPaymentButtom = 1;
													@endphp
													@elseif($remainder>0)
														{{ App\Members\Investment::getGainPerActiveTeamUsers($remainder) }}
														@php
															$remainder = $remainder - 4;
															$setPaymentButtom = 0;
														@endphp
													@else
														@php
															$setPaymentButtom = 0;
														@endphp
														{{ App\Members\Investment::getGainPerActiveTeamUsers(0) }}
													@endif --}}
											</td> 	
											<td>{{ \Carbon\Carbon::parse($casheWithdrawal->created_at) }}</td>
											<td>{{ \Carbon\Carbon::parse($casheWithdrawal->updated_at) }}</td>
										</tr>   
								@endforeach		

							</tbody>
						</table>
					</div> 
				</div>
				<!-- /.card-content -->
			</div>
			<!-- /.box-content -->
		</div>
  
  <!-- /page content -->
@endsection

@section('footer')
	<!-- Responsive Table -->
	<script src="{{ asset('assets/plugin/RWD-table-pattern/js/rwd-table.min.js') }}"></script>
	<script src="{{ asset('assets/scripts/rwd.demo.min.js') }}"></script>
@endsection