@extends('members/app')
@section('head')
	<!-- Table Responsive -->
	<link rel="stylesheet" href="{{ asset('assets/plugin/RWD-table-pattern/css/rwd-table.min.css') }}">
	<style type="text/css" media="screen">
		.btn {
			padding: 5px;
		}

		.withdraw-Inactive{
			font-weight: 700;
			color: green;
		}
	</style>
@endsection
@section('pagename','Withdraw')
@section('mainBody')

  <!-- page content -->
    <!-- page content -->
  	<div class="ui-sortable-handle">
			<div class="box-content card bordered-all success">
			<h4 class="box-title bg-success"><i class="ico fa fa-usd"></i>My Investments</h4>
			<!-- /.box-title -->
			<!-- /.dropdown js__dropdown -->
				<div class="card-content">
					
					<div class="table-responsive" data-pattern="priority-columns">
						<table id="tech-companies-1" class="table table-small-font table-bordered table-striped">
							<thead>
								<tr>
									<th data-priority="1">Amount USD</th>
									<th data-priority="1">Active Team Investment</th>
									<th data-priority="1">Earnings</th>
									{{-- <th data-priority="4">Status</th> --}}
									<th data-priority="1">Wallet Address</th>
									<th data-priority="1">Withdraw Request</th>
								</tr>
							</thead>
							<tbody>
									
								@php
									$remainder = $numberOfActiveTeamInvestments % 4;
									$gainRemainder = $numberOfActiveTeamInvestments % 4;
									$number = ($numberOfActiveTeamInvestments - $remainder) / 4;
									$gainNumber = ($numberOfActiveTeamInvestments - $remainder) / 4;

									
								@endphp

								@for ($i = 0; $i < $number_of_investments; $i++)
								

									<tr>
										<th>50 USD </th>
										
										<td>

											@if($number>=1)
												@php

													$number = $number - 1;
												@endphp

													@if ($number_of_investments == 1)
														{{ $numberOfActiveTeamInvestments }} Investements
													@else
														4 Investements
													@endif	
											
											@elseif($remainder>0)
													{{ $remainder }} Investements
												@php
													$remainder = $remainder - 4;
												@endphp
											@else
												@php
													$setPaymentButtom = 0;
												@endphp

												0 Investements
											@endif

										</td>

										<td>

											@if($gainNumber>=1)
												{{ $amount_usd =  App\Members\Withdraw::getGainPerActiveTeamUsers(4) }}

												@php

													$gainNumber = $gainNumber - 1;
													$setPaymentButtom = 1;
													
												@endphp
											@elseif($gainRemainder>0)
												{{ $amount_usd = App\Members\Withdraw::getGainPerActiveTeamUsers($gainRemainder) }}
												@php

													$gainRemainder = $gainRemainder - 4;
													$setPaymentButtom = 0;
												@endphp
											@else
												@php
													$setPaymentButtom = 0;

													// echo $gainNumber;
												@endphp
												{{ $amount_usd = App\Members\Withdraw::getGainPerActiveTeamUsers(0) }}
											@endif

										 USD
												
										</td>
										<td>{{ $user->address }}</td>
										
										<td>
											
											@if ($setPaymentButtom == 1)

												 <a href="{{ route('member.requsetCasheWithdrawal', $amount_usd ) }}" class="btn btn-block btn-success"><i class="fa fa-check-square" aria-hidden="true"></i> Request Payment</a>
											@elseif(\Carbon\Carbon::now() >= \Carbon\Carbon::parse($lastInvestment->created_at)->addMonths(1))

												 <a href="{{ route('member.requsetCasheWithdrawal', $amount_usd ) }}" class="btn btn-block btn-success"><i class="fa fa-check-square" aria-hidden="true"></i> Request Payment</a>
											@else

												 <span class="payment-request">You can request for payment after the {{ \Carbon\Carbon::parse($lastInvestment->created_at)->addMonths(1)->format('d - m - Y') }}</span>
												 
											@endif

										</td>
										
										
									</tr>   
								@endfor

							</tbody>
						</table>
					</div> 
				
				</div> 
			</div>
			<!-- /.card-content -->
		</div>


		<div class="ui-sortable-handle">
			<div class="box-content card bordered-all success">
			<h4 class="box-title bg-success"><i class="ico fa fa-usd"></i>Withdraw</h4>
			<!-- /.box-title -->
			<!-- /.dropdown js__dropdown -->
				<div class="card-content">
					<div class="table-responsive" data-pattern="priority-columns">
						<table id="tech-companies-1" class="table table-small-font table-bordered table-striped">
							<thead>
								<tr>
									<th data-priority="1">Amount</th>
									<th data-priority="1">Earning</th>
									<th data-priority="1">Request Date</th>
									<th data-priority="1">Send</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($withdrawals as $casheWithdrawal)

										<tr>
											<th>50 USD</th>
											<td>{{ $casheWithdrawal->amount_usd }} USD</td>
											<td>{{ \Carbon\Carbon::parse($casheWithdrawal->created_at)}}</td>
											<td><span class="withdraw-{{ $casheWithdrawal->status }}">Waiting for Approval </span></td>
										</tr> 

								@endforeach 
							
							</tbody>
						</table>
					</div> 
				
				
				</div> 
			</div>
			<!-- /.card-content -->
		</div>

		<div class="ui-sortable-handle">
			<div class="box-content card bordered-all success">
				<h4 class="box-title bg-success"><i class="ico fa fa-usd"></i>Investment Earnings</h4>
				<!-- /.box-title -->
				<!-- /.dropdown js__dropdown -->
				<div class="card-content">
					<div class="table-responsive" data-pattern="priority-columns">
							<table id="tech-companies-2" class="table table-small-font table-bordered table-striped">
							<thead>
								<tr>
									<th data-priority="1">Amount</th>
									<th data-priority="1">Earning</th>
									<th data-priority="1">Request Date</th>
									<th data-priority="1">Confirmation Date</th>
								</tr>
							</thead>

							<tbody>

								@foreach ($Confirmedwithdrawals as $casheWithdrawal)

										<tr>
											<th>50 USD</th>
											<td>{{ $casheWithdrawal->amount_usd }} USD</td>
											<td>{{ \Carbon\Carbon::parse($casheWithdrawal->created_at)}}</td>
											<td>{{ \Carbon\Carbon::parse($casheWithdrawal->updated_at)}}</td>
										</tr> 

								@endforeach 


							</tbody>
						</table>
					</div>
					
					</div> 
				</div>
				<!-- /.card-content -->
			</div>
			<!-- /.box-content -->
		</div>
  
  <!-- /page content -->
@endsection

@section('footer')
	<!-- Responsive Table -->
	<script src="{{ asset('assets/plugin/RWD-table-pattern/js/rwd-table.min.js') }}"></script>
	<script src="{{ asset('assets/scripts/rwd.demo.min.js') }}"></script>
@endsection