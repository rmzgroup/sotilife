
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sotylife | Reset Your Password</title>
    <link rel="stylesheet" href="{{ asset('assets/styles/style.css') }}">

    <!-- Waves Effect -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/waves/waves.min.css') }}">

</head>

<body>

<div id="single-wrapper">
     <form method="POST" action="{{ route('password.update') }}" class="frm-single">
                    @csrf

            <input type="hidden" name="token" value="{{ $token }}">


        <div class="inside">
            <div class="title"><strong>Sotylife</strong>Reset Your Password</div>
            <!-- /.title -->
            <div class="frm-title">Reset Your Password Now!</div>
            <!-- /.frm-title -->
            
            <div class="frm-input">
                 <input id="email" type="email" class="frm-inp {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus><i class="fa fa-envelope frm-ico"></i>

                   @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
            </div>
            <!-- /.frm-input -->
            <div class="frm-input">
                  <input id="password" type="password" class="frm-inp{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password"><i class="fa fa-lock frm-ico"></i>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
            </div>

             <div class="frm-input">
                  <input id="password-confirm" type="password" class="frm-inp{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password_confirmation" required placeholder="Confirm your password"><i class="fa fa-lock frm-ico"></i>
            </div>
            <!-- /.frm-input -->

            <!-- /.clearfix -->
            <button type="submit" class="frm-submit">Reset Password<i class="fa fa-arrow-circle-right"></i></button>
            <!-- /.row -->
          
            <div class="frm-footer">Sotylife © 2019.</div>
            <!-- /.footer -->
        </div>
        <!-- .inside -->
    </form>
    <!-- /.frm-single -->
</div><!--/#single-wrapper -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/script/html5shiv.min.js"></script>
        <script src="assets/script/respond.min.js"></script>
    <![endif]-->
    <!-- 
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('assets/scripts/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/modernizr.min.js') }}"></script>
    <script src="{{ asset('assets/plugin/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/plugin/nprogress/nprogress.js') }}"></script>
    <script src="{{ asset('assets/plugin/waves/waves.min.js') }}"></script>

    <script src="{{ asset('assets/scripts/main.min.js') }}"></script>
</body>
</html>

