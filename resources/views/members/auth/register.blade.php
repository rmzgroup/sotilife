
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sotylife  | Register</title>
    <link rel="stylesheet" href="{{ asset('assets/styles/style.css') }}">

    <!-- Waves Effect -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/waves/waves.min.css') }}">

</head>

<body>

<div id="single-wrapper">

      @include('members/layouts/notifications') 

     <form method="POST" action="{{ route('register') }}" class="frm-single">
                    @csrf
        <div class="inside">
             
              
            <div class="title"><strong>Sotylife </strong>Registration</div>
            <!-- /.title -->
            <div class="frm-title">Register Now!</div>
            <!-- /.frm-title -->
            <div class="frm-input">
                 <input id="name" type="text" class="frm-inp {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Name and Surname *" required autofocus><i class="fa fa-user frm-ico"></i>

                  @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
            </div>

            <div class="frm-input">
                 <input id="email" type="email" class="frm-inp {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email *" required autofocus><i class="fa fa-envelope frm-ico"></i>

                   @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
            </div>

            <div class="frm-input">
                 <input id="code" type="text" class="frm-inp {{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" value="{{ \Request::get('code') }}" readonly="true" placeholder="Reference Code" autofocus><i class="fa fa-user frm-ico"></i>

                  @if ($errors->has('code'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('code') }}</strong>
                        </span>
                    @endif
            </div>
            <!-- /.frm-input -->
            <div class="frm-input">
                  <input id="password" type="password" class="frm-inp{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password *"><i class="fa fa-lock frm-ico"></i>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
            </div>

             <div class="frm-input">
                  <input id="password-confirm" type="password" class="frm-inp{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password_confirmation" required placeholder="Confirm your password *"><i class="fa fa-lock frm-ico"></i>
            </div>
            <!-- /.frm-input -->

            <!-- /.clearfix -->
            <button type="submit" class="frm-submit">Register<i class="fa fa-arrow-circle-right"></i></button>
            <!-- /.row -->
            <a href="{{ route('login') }}" class="a-link"><i class="fa fa-key"></i>Already have account? Login.</a>
            <div class="frm-footer">Sotylife  © 2019.</div>
            <!-- /.footer -->
        </div>
        <!-- .inside -->
    </form>
    <!-- /.frm-single -->
</div><!--/#single-wrapper -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/script/html5shiv.min.js"></script>
        <script src="assets/script/respond.min.js"></script>
    <![endif]-->
    <!-- 
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('assets/scripts/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/modernizr.min.js') }}"></script>
    <script src="{{ asset('assets/plugin/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/plugin/nprogress/nprogress.js') }}"></script>
    <script src="{{ asset('assets/plugin/waves/waves.min.js') }}"></script>

    <script src="{{ asset('assets/scripts/main.min.js') }}"></script>
</body>
</html>

