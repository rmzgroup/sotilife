@extends('members/app')

@section('head')
	<style type="text/css" media="screen">
		.desc{
			font-size: 18px;
			font-weight: 700;
		}
		.desc span{
			color: green;
			font-size: 19px;
		}

		.address-name h4{
			font-weight: 700;
			font-size: 25px;
		}



		#clockdiv{
		    font-family: sans-serif;
		    color: #fff;
		    display: inline-block;
		    font-weight: 100;
		    text-align: center;
		    font-size: 30px;
		}

		#clockdiv > div{
		    padding: 10px;
		    border-radius: 3px;
		    background: #00BF96;
		    display: inline-block;
		}

		#clockdiv div > span{
		    padding: 15px;
		    border-radius: 3px;
		    background: #00816A;
		    display: inline-block;
		}

		.smalltext{
		    padding-top: 5px;
		    font-size: 16px;
		}



		/*2*/

		#clockdiv-2 {
		    font-family: sans-serif;
		    color: #fff;
		    display: inline-block;
		    font-weight: 100;
		    text-align: center;
		    font-size: 30px;
		}

		#clockdiv-2 > div{
		    padding: 10px;
		    border-radius: 3px;
		    background: #00BF96;
		    display: inline-block;
		}

		#clockdiv-2 div > span{
		    padding: 15px;
		    border-radius: 3px;
		    background: #00816A;
		    display: inline-block;
		}

		.smalltext-2{
		    padding-top: 5px;
		    font-size: 16px;
		}

		.avatar{
			text-align: center;
		}

	</style>
@endsection


@section('pagename','Buy Soty')
@section('mainBody')

  <!-- page content -->

  <div class="row small-spacing">
  		
		<div class="box-content ">
			<!-- /.box-title -->
			<div class="row">
				
				<div class="col-xs-12"> 

					<div class="box-content card">
						<h4 class="box-title"><i class="fa fa-globe ico"></i> SEND YOUR COINS TO THE ADDRESS BELOW</h4>
						<!-- /.dropdown js__dropdown -->
						
						<div class="card-content">
							<ul class="notice-list">
								<li>

									<a href="#">
										<span class="avatar"><img src="{{ asset('user/images/members/bitcoin.png') }}" alt=""></span>
										<span class="address-name"><h4>{{ $addCoinDeposit->user->address }}</h4></span>
										<span class="desc">Amount:  <span>	{{ $addCoinDeposit->amount_usd }} USD : {{ $addCoinDeposit->amount_crypto }} {{ $addCoinDeposit->coin_type }}</span> </span>
									</a>
								</li>
								
							</ul>
							<!-- /.notice-list -->
							{{-- <h1>{{ date("Y-m-d H:m:s", strtotime("+2 hours +20 minutes", $addCoinDeposit->created_at)) }}</h1> --}}
							
							 
							  <span class="address-name"><h4>Time Remaining</h4></span>
								<div id="clockdiv">
								  <div style="display: none">
								    <span class="days"></span>
								    <div class="smalltext">Days</div>
								  </div>
								  <div>
								    <span class="hours"></span>
								    <div class="smalltext">Hours</div>
								  </div>
								  <div>
								    <span class="minutes"></span>
								    <div class="smalltext">Minutes</div>
								  </div>
								  <div>
								    <span class="seconds"></span>
								    <div class="smalltext">Seconds</div>
								  </div>	
								</div>
								<!-- 2-->

									<h4>
										<a href="{{ route('member.casheDeposit') }}" class="btn  btn-rounded btn-bordered  waves-light text-white margin-top-50" style="background: #Ff7f00; border: none;" title="Details ...">Go back</a>

										 <form id="delete-form-cancel" method="get" action="{{ route('member.casheDeposit.cancel') }}" style="display: none">
			                                {{ csrf_field() }}
			                                {{ method_field('DELETE') }}
			                              </form>
			                              <a href="" onclick="
			                              if(confirm('Are you sure you want to cancel this payment deposite request?'))
			                                  {
			                                    event.preventDefault();
			                                    document.getElementById('delete-form-cancel').submit();
			                                  }
			                                  else{
			                                    event.preventDefault();
			                                  }" class="btn  btn-rounded btn-bordered  waves-light text-white margin-top-50" style="background:red; border: none;"><span class="glyphicon glyphicon-trash"></span> Cancel</a>

									</h4>
						</div>
						<!-- /.card-content -->
					</div>


				

				</div>
			
			</div>
		</div>
		<!-- /.box-content -->
				  
  </div>
  
  <!-- /page content -->
@endsection

@section('footer')
	<script type="text/javascript">
	   	
	   	function getTimeRemaining(endtime) {
				  var t = Date.parse(endtime) - Date.parse(new Date());
				  var seconds = Math.floor((t / 1000) % 60);
				  var minutes = Math.floor((t / 1000 / 60) % 60);
				  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
				  var days = Math.floor(t / (1000 * 60 * 60 * 24));
				  return {
				    'total': t,
				    'days': days,
				    'hours': hours,
				    'minutes': minutes,
				    'seconds': seconds
				  };
				}

				function initializeClock(id, endtime) {
				  var clock = document.getElementById(id);
				  var daysSpan = clock.querySelector('.days');
				  var hoursSpan = clock.querySelector('.hours');
				  var minutesSpan = clock.querySelector('.minutes');
				  var secondsSpan = clock.querySelector('.seconds');

				  function updateClock() {
				    var t = getTimeRemaining(endtime);

				    daysSpan.innerHTML = t.days;
				    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
				    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
				    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

				    if (t.total <= 0) {
				      clearInterval(timeinterval);
				    }
				  }

				  updateClock();
				  var timeinterval = setInterval(updateClock, 1000);
				}

				var deadline = '{{ date('m d Y H:i:s',strtotime('+4 hour +00 minutes',strtotime($addCoinDeposit->created_at))) }} GMT+0000';
				// var deadline = 'August 31 2019 23:59:59';
				initializeClock('clockdiv', deadline);


	   </script>
@endsection