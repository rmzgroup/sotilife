@extends('members/app')


@section('head')
	<!-- Table Responsive -->
	<link rel="stylesheet" href="{{ asset('assets/plugin/RWD-table-pattern/css/rwd-table.min.css') }}">
	<style type="text/css" media="screen">
		.btn {
			padding: 5px;
		}
	</style>
@endsection

@section('pagename','My Team')
@section('mainBody')

  <!-- page content -->
	<div class="ui-sortable-handle">
			<div class="box-content card bordered-all success">
			<h4 class="box-title bg-success"><i class="ico fa fa-usd"></i>My Team</h4>
			<!-- /.box-title -->
			<!-- /.dropdown js__dropdown -->
				<div class="card-content">
					<div class="table-responsive" data-pattern="priority-columns">
						<table id="tech-companies-1" class="table table-small-font table-bordered table-striped">
							<thead>
								<tr>
									<th data-priority="1">NO</th>
									<th data-priority="1">Name and Surname</th>
									<th data-priority="1">Registration Date</th>
									<th data-priority="1">Status</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($team as $person)
									
								<tr>
									<th>{{ $loop->index + 1 }}</th>
									<th class="table-name">
										<img src="{{ $person->avatar == '' ? $person->sex == "Male"  ? "/avatar-male.png" : "/avatar-female.png" : Storage::url($person->avatar) }}" alt="" class="avatar team-avatar hidden-xs">

										{{ $person->name }}
									</th>
									<td>{{ \Carbon\Carbon::parse($person->created_at)}}</td>
									<td>
										@php
											$isActive = 0
										@endphp
										@foreach ($soties as $soty)
											@if ($soty->user_id == $person->id)
												@php
													$isActive = 1;
												@endphp
											@endif
											
										@endforeach

										<span class="investment-{{ $isActive == 1 ? 'Active' : 'Inactive' }}"> {{ $isActive == 1 ? 'Active' : 'Inactive' }} </span>
											
									 </td>
								</tr>   
								
								@endforeach
								

							</tbody>
						</table>
					</div> 
				
				
				</div> 
			</div>
			<!-- /.card-content -->
		</div>


@endsection

@section('footer')
<!-- Responsive Table -->
	<script src="{{ asset('assets/plugin/RWD-table-pattern/js/rwd-table.min.js') }}"></script>
	<script src="{{ asset('assets/scripts/rwd.demo.min.js') }}"></script>
@endsection