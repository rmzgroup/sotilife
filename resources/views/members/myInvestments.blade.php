@extends('members/app')
@section('head')
	<!-- Table Responsive -->
	<link rel="stylesheet" href="{{ asset('assets/plugin/RWD-table-pattern/css/rwd-table.min.css') }}">
	<style type="text/css" media="screen">
		.btn {
			padding: 5px;
		}

		.payment-request{
			font-weight: 700;
			color: red;
		}
	</style>
@endsection
@section('pagename','My Investments')
@section('mainBody')

  <!-- page content -->
  	<div class="ui-sortable-handle">
			<div class="box-content card bordered-all success">
			<h4 class="box-title bg-success"><i class="ico fa fa-usd"></i>My Investments</h4>
			<!-- /.box-title -->
			<!-- /.dropdown js__dropdown -->
			
					<div class="box-content">
					<div class="table-responsive" data-pattern="priority-columns">
						<table id="tech-companies-1" class="table table-small-font table-bordered table-striped">
							<thead>
								<tr>
									<th data-priority="1">Amount USD</th>
									<th data-priority="1">Amount Crypto</th>
									<th data-priority="1">Active Team Investment</th>
									{{-- <th data-priority="4">Status</th> --}}
									<th data-priority="1">Wallet Address</th>
									<th data-priority="1">Investment Date</th>
								</tr>
							</thead>
							<tbody>

								@foreach ($myInvestments as $investment)
	

								<tr>
									<th>{{ $investment->amount_usd }} </th>
									<td>
											@php
												$number_soties = $investment->amount_usd / 50;
												echo $investment->amount_crypto / $number_soties;
											@endphp
									</td>
									
									<td>{{ $investment->user->soties_number }} Investments</td>
									<td>{{ $user->address }}</td>
									<td>{{ \Carbon\Carbon::parse($investment->created_at)->format('d - m - Y')}}</td>
									
								</tr>      
								@endforeach

							</tbody>
						</table>
					</div> 
				</div>
				
				
			</div>
			<!-- /.card-content -->
		</div>

  <!-- /page content -->
@endsection

@section('footer')
	<!-- Responsive Table -->
	<script src="{{ asset('assets/plugin/RWD-table-pattern/js/rwd-table.min.js') }}"></script>
	<script src="{{ asset('assets/scripts/rwd.demo.min.js') }}"></script>
@endsection