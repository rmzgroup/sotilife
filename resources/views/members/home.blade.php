@extends('members/app')

@section('head')
	<!-- Jquery UI -->
	<link rel="stylesheet" href="{{ asset('assets/plugin/jquery-ui/jquery-ui.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/plugin/jquery-ui/jquery-ui.structure.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/plugin/jquery-ui/jquery-ui.theme.min.css') }}">

	<style type="text/css" media="screen">
		.petek{
			margin: 5px 18px;
		}

		.table{
			margin-bottom: 0px;
		}

		.payment-notice{
			font-weight: 700;
			text-align: center;
		}

		.code{
			color: orange;
			text-align: right;
			font-size: 12px;
			width: 240px;
			padding: 8px;
		}

		@media(max-width:767px){ 
			.petek{
				margin: 5px 8px;
			}

			.number_soties{
				font-size: 18PX;
			}
		 }
		@media(min-width:768px){}
		@media(min-width:992px){}
		@media(max-width:1800px){
			.petek{
				margin: 5px;
			}
		}


	</style>

@endsection
@section('pagename','Dashboard')
@section('mainBody')

  <!-- page content -->
					
  	<div class="row small-spacing">

  		<div class="col-lg-4 col-md-6 col-xs-12">
			<div class="box-content bg-navy-blue text-white">
				<div class="statistics-box with-icon">
					<i class="ico small fa fa-usd"></i>
					<p class="text text-white">TEAM EARNINGS</p>
					<h2 class="counter">{{ $userEarnings }} USD</h2>
				</div>
			</div>
			<!-- /.box-content -->
		</div>

		<!-- /.col-lg-4 col-md-6 col-xs-12 -->
		<div class="col-lg-4 col-md-6 col-xs-12">
			<div class="box-content bg-navy-blue text-white">
				<div class="statistics-box with-icon">
					<i class="ico small fa fa-users"></i>
					<p class="text text-white">NUMBER OF TEAMS</p>
					<h2 class="counter">{{ $teamNumber }}</h2>
				</div>
			</div>
			<!-- /.box-content -->
		</div>

		<!-- /.col-lg-4 col-md-6 col-xs-12 -->
		<div class="col-lg-4 col-md-6 col-xs-12">
			<div class="box-content bg-navy-blue text-white">
				<div class="statistics-box with-icon">
					<i class="ico small fa fa-usd"></i>
					<p class="text text-white">TOTAL EARNINGS</p>
					<h2 class="counter">{{ $totalUserEarnings }} USD</h2>
				</div>
			</div>
			<!-- /.box-content -->
		</div>

		<!-- /.col-lg-4 col-md-6 col-xs-12 -->
		<div class="col-lg-4 col-md-6 col-xs-12">
			<div class="box-content bg-navy-blue text-white">
				<div class="statistics-box with-icon">
					<i class="ico small fa fa-sort-numeric-asc"></i>
					<p class="text text-white">TOTAL INVESTEMENT</p>
					<h2 class="counter">{{ $totalUserInvestment }} USD</h2>
				</div>
			</div>
			<!-- /.box-content -->
		</div>

		<div class="col-lg-4 col-md-6 col-xs-12">
			<div class="box-content bg-navy-blue text-white">
				<div class="statistics-box with-icon text-white">
					<i class="ico fa fa-user text-white"></i>
					<p class="text text-white">My Reference Link</p>
					<input id="ref-code" type="text" class="code" value="https://sotylife.com/register?code={{ Auth::user()->code }}" readonly title="Click to copy the code">
					
				</div>
			</div>
			<!-- /.box-content -->
		</div>
		<!-- /.col-lg-4 col-md-6 col-xs-12 -->
		<div class="col-lg-4 col-md-6 col-xs-12">
			<div class="box-content bg-navy-blue">
				<div class="statistics-box with-icon">
					<i class="ico fa fa-users text-white"></i>
					<p class="text text-white">Total Users</p>
					<h2 class="counter text-white">{{ $allUsersNumber }}</h2>
					
				</div>
			</div>
			<!-- /.box-content -->
		</div>
		<!-- /.col-lg-4 col-md-6 col-xs-12 -->
	</div>
	<!-- .row -->

	{{-- @php
		if ($numberOfActiveTeamUsers>3) {
			$full = $numberOfActiveTeamUsers % 4;
		   $remainder = $numberOfActiveTeamUsers - $full * 4;

		}else{
		   $full = 0;
		   $remainder = $numberOfActiveTeamUsers - $full * 4;
		}
	
		$remainderDisplay = 0;
	@endphp
 --}}

 		<div class="row small-spacing margin-top-50">
				
				<div class="box-content petekler">
				
					<h1 class="number_soties">Number of Soties: {{ $number_of_investments }} > Team Investment: {{ $numberOfActiveTeamInvestments }}</h1>
					
					<div class="row">
						@for ($investment = 0; $investment < $number_of_investments; $investment++)
							<div class="col-lg-3 col-md-6 col-xs-12" style="">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th><strong>Soty No: {{ $investment + 1 }}</strong></th> 
										</tr> 
										<tr>
											<th>
												@if ($numberOfActiveTeamInvestments > 3)
													<a href="{{ route('member.myInvestments') }}" class="btn btn-block btn-success" title="Request Payment">Request Payment</a>
												@else
													<span class="payment-notice"> <i class="ico small fa fa-info"></i> End of month payment</span>
												@endif
											</th> 
										</tr> 
									</thead> 
								</table>
							</div>
						@endfor
					</div>

					<div class="row investment-soties">
							@php
								$remainder = $numberOfActiveTeamInvestments % 4;
								$number = ($numberOfActiveTeamInvestments - $remainder) / 4;
							@endphp

							@for ($i = 0; $i < $number; $i++)
								<div class="col-lg-3 col-md-6 col-xs-12">
									<table class="table table-bordered">
										<tbody> 
											<tr> 
												<td class="soty-box">
													
													  	<img src="{{ asset('user/images/petek.png') }}" class="petek">
													  	<img src="{{ asset('user/images/petek.png') }}" class="petek">
													  	<img src="{{ asset('user/images/petek.png') }}" class="petek">
													  	<img src="{{ asset('user/images/petek.png') }}" class="petek">
												</td> 
											</tr> 
										</tbody> 
									</table>
								</div>
						    @endfor

						    @if ($remainder>0)
							    <div class="col-lg-3 col-md-6 col-xs-12">
									<table class="table table-bordered">
										<tbody> 
											<tr> 
												<td class="soty-box">
													 @for ($i = 0; $i < $remainder; $i++)
													  	<img src="{{ asset('user/images/petek.png') }}" class="petek">
													 @endfor

												</td> 
											</tr> 
										</tbody> 
									</table>
								</div>
						    @endif
							
						   
			    	</div>
			    </div>

		</div>


	


  <!-- /page content -->
@endsection

@section('footer')
	<!-- Jquery UI -->
	<script src="{{ asset('assets/plugin/jquery-ui/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('assets/plugin/jquery-ui/jquery.ui.touch-punch.min.js') }}"></script>

	<script type="text/javascript">
	document.getElementById("ref-code").onclick = function() {
		    this.select();
		    document.execCommand('copy');
		    swal("Copied!", "Your Reference code has been Copied", "success");
		}
	</script>


@endsection