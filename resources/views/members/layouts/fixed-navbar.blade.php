<div class="fixed-navbar">
  <div class="pull-left">
    <button type="button" class="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
    <h1 class="page-title">@yield('pagename')</h1>
    <!-- /.page-title -->
  </div>
  <!-- /.pull-left -->
  <div class="pull-right">
    <div class="ico-item">
      <form action="#" id="searchform-header" class="searchform js__toggle"><input type="search" placeholder="Search..." class="input-search"><button class="mdi mdi-magnify button-search" type="submit"></button></form>
      <!-- /.searchform -->
    </div>
    <!-- /.ico-item -->
    <a href="#" class="ico-item mdi mdi-logout js__logout"></a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>


  </div>
  <!-- /.pull-right -->
</div>
<!-- /.fixed-navbar -->



