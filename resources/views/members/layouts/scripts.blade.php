    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="{{ asset('assets/script/html5shiv.min.js') }}"></script>
        <script src="{{ asset('assets/script/respond.min.js') }}"></script>
    <![endif]-->
    {{-- 
     <script src="{{ asset('assets/script/html5shiv.min.js') }}"></script>
        <script src="{{ asset('assets/script/respond.min.js') }}"></script> --}}
    <!-- 
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('assets/scripts/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/modernizr.min.js') }}"></script>
    <script src="{{ asset('assets/plugin/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('assets/plugin/nprogress/nprogress.js') }}"></script>
    <script src="{{ asset('assets/plugin/sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugin/waves/waves.min.js') }}"></script>

   

    <script src="{{ asset('assets/scripts/main.js') }}"></script>


    <!-- Remodal -->
    <script src="{{ asset('assets/plugin/modal/remodal/remodal.min.js') }}"></script>


    <!-- Toastr -->
    <script src="{{ asset('assets/plugin/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/toastr.demo.min.js') }}"></script>


    <script>


      @if(Session::has('success'))
            toastr.success("{{ Session::get('success') }}");
      @endif


      @if(Session::has('info'))
            toastr.info("{{ Session::get('info') }}");
      @endif


      @if(Session::has('warning'))
            toastr.warning("{{ Session::get('warning') }}");
      @endif


      @if(Session::has('error'))
            toastr.error("{{ Session::get('error') }}");
      @endif


    </script>


     @section('footer')

     @show