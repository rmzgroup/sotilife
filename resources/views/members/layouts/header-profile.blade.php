<header class="header">
		<a href="{{ route('member.index') }}" class="logo"><i class="ico mdi mdi-polymer"></i>SOTYLIFE</a>
		<button type="button" class="button-close fa fa-times js__menu_close"></button>
		<div class="user">
			<a href="#" class="avatar"><img src="{{ Auth::user()->avatar == '' ? Auth::user()->sex == "Male"  ? "/avatar-male.png" : "/avatar-female.png" : Storage::url(Auth::user()->avatar) }}" alt="{{ Auth::user()->name }}"><span class="status online"></span></a>
			<h5 class="name"><a href="{{ route('member.profile') }}">{{ strtok(Auth::user()->name, ' ') }}</a></h5>
			<h5 class="position">Online</h5>
			<!-- /.name -->
			<div class="control-wrap js__drop_down">
				<i class="fa fa-caret-down js__drop_down_button"></i>
				<div class="control-list">
					<div class="control-item"><a href="{{ route('member.profile') }}"><i class="fa fa-user"></i> Profile</a></div>
					<div class="control-item"><a href="#" data-remodal-target="settings" ><i class="fa fa-gear"></i> Settings</a></div>
					<div class="control-item"><a href="#" class="ico-item mdi mdi-logout js__logout"> Log out</a></div>
				</div>
				<!-- /.control-list -->

					<div class="remodal" data-remodal-id="settings" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
						<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
						<div class="remodal-content">
							<h2 id="modal1Title">Settings</h2>
								
								<form method="POST" action="{{ route('member.changePassword') }}">
										@csrf
							        <div class="inside">
							            <!-- /.frm-title -->
							            <div class="frm-input">

							                <label for="exampleInputEmail1">Enter your Password</label>
											<input type="password" class="form-control" id="current-password" placeholder="Enter your Password" name="current-password">

							                    @if ($errors->has('current-password'))
							                        <span class="invalid-feedback" role="alert">
							                            <p><strong>{{ $errors->first('current-password') }}</strong></p>
							                        </span>
							                    @endif
							            </div>
							         	
							         	 <div class="frm-input">


							               <label for="exampleInputPassword1">Enter the New Password</label>
											<input type="password" class="form-control" id="password" name="password" placeholder="Enter the New Password">

							                @if ($errors->has('password'))
						                        <span class="invalid-feedback" role="alert">
						                           <p><strong>{{ $errors->first('password') }}</strong></p>
						                        </span>
						                    @endif
							            </div>

							             <div class="frm-input">

							               <label for="exampleInputPassword1">Confirm your Password</label>
											<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm your Password">
							            </div>



							             <div class="frm-input" style="text-align: center;">
							        		<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
								     		<button type="submit" class="remodal-confirm">Save</button>
	
							      		  </div>
   


							        </div>
							        <!-- .inside -->
							       
							        
							    </form>
						</div>
						
					</div>





			</div>
			<!-- /.control-wrap -->
		</div>
		<!-- /.user -->
	</header>
	<!-- /.header -->