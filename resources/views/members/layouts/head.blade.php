    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sotylife | @yield('pagename')</title>

    <!-- Main Styles -->
    <link rel="stylesheet" href="{{ asset('assets/styles/style.css') }}">
    
    <!-- Material Design Icon -->
    <link rel="stylesheet" href="{{ asset('assets/fonts/material-design/css/materialdesignicons.css') }}">

    <!-- mCustomScrollbar -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css') }}">

    <!-- Waves Effect -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/waves/waves.min.css') }}">

    <!-- Sweet Alert -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/sweet-alert/sweetalert.css') }}">
    
    <!-- Morris Chart -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/chart/morris/morris.css') }}">

    <!-- FullCalendar -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugin/fullcalendar/fullcalendar.print.css') }}" media='print'>

    <!-- Dark Themes -->
    <link rel="stylesheet" href="{{ asset('assets/styles/style-dark.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/styles/custom.css') }}">

    <!-- Remodal -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/modal/remodal/remodal.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugin/modal/remodal/remodal-default-theme.css') }}">


    <!-- Toastr -->
    <link rel="stylesheet" href="{{ asset('assets/plugin/toastr/toastr.css') }}">

    
    @section('head')

    @show