         <div class="col-md-8 col-md-offset-2">
         	        <div class="x_content bs-example-popovers">
                  
                   @if(session('error'))
                    <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-ban"></i> Ooopps, bir hata oluştu!</h4>
                      {{ session('error') }}
                    </div>
                    
              @elseif(session('success'))

                   <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Başarı! {{ session('success') }}</strong>
                  </div>


              @elseif(session('warning'))
                  <div class="alert alert-warning alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Uyari! {{ session('warning') }}</strong>
                  </div>


              @elseif(session('info'))
                     <div class="alert alert-info alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Alert!   {{ session('info') }}</strong>
                  </div>
                
              @endif 

                </div>
         </div>