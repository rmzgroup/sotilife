<div class="navigation">
      <!-- /.title -->
      <ul class="menu js__accordion">
        <li class=" {{ str_contains(request()->url(), '/home') ? 'current' : '' }}">
          <a class="waves-effect" href="{{ route('member.index') }}"><i class="menu-icon mdi mdi-view-dashboard"></i><span>Dashboard</span></a>
        </li>

         <li class=" {{ str_contains(request()->url(), '/profile') ? 'current' : '' }}">
          <a class="waves-effect" href="{{ route('member.profile') }}"><i class="menu-icon mdi mdi-worker"></i><span>My Profile</span></a>
        </li>


         <li class=" {{ str_contains(request()->url(), '/cash-deposit') ? 'current' : '' }}">
          <a class="waves-effect" href="{{ route('member.casheDeposit') }}"><i class="menu-icon mdi mdi-arrow-down-bold-hexagon-outline"></i><span>Buy Soty</span></a>
        </li>

        <li class=" {{ str_contains(request()->url(), '/my-investments') ? 'current' : '' }}">
          <a class="waves-effect" href="{{ route('member.myInvestments') }}"><i class="menu-icon mdi mdi-square-inc-cash"></i><span>My Investments</span></a>
        </li>

         <li class=" {{ str_contains(request()->url(), '/cash-withdrawal-transactions') ? 'current' : '' }}">
          <a class="waves-effect" href="{{ route('member.casheWithdrawal') }}"><i class="menu-icon mdi mdi-bank"></i><span>Withdraw</span></a>
        </li>


         <li class=" {{ str_contains(request()->url(), '/wallet-addresses') ? 'current' : '' }}">
          <a class="waves-effect" href="{{ route('member.walletAddresses') }}"><i class="menu-icon mdi mdi mdi-book-open-variant"></i><span>My Wallet Adresses</span></a>
        </li>



        <li class=" {{ str_contains(request()->url(), '/reference-list') ? 'current' : '' }}">
          <a class="waves-effect" href="{{ route('member.reference') }}"><i class="menu-icon mdi mdi-view-list"></i><span>My Team</span></a>
        </li>

        <li class=" {{ str_contains(request()->url(), '/about-the-system') ? 'current' : '' }}">
          <a class="waves-effect" href="{{ route('member.aboutSystem') }}"><i class="menu-icon mdi mdi-view-list"></i><span>About Sotylife Gain System</span></a>
        </li>


        <li>
           <a href="#" class=" waves-effect mdi-logsout js__logout"><i class="menu-icon mdi mdi-logout-variant"></i><span>Log Out</span></a>
        </li>

        

      </ul>
      <!-- /.menu js__accordion -->
      <!-- /.menu js__accordion -->
    </div>
    <!-- /.navigation -->