@extends('members/app')
@section('head')
	<style type="text/css" media="screen">
		.avatar img{
			height: 55px;
		}

		
	</style>
@endsection
@section('pagename','My The Wallet Address')
@section('mainBody')

  <!-- page content -->

  <!-- page content -->
  	<div class="ui-sortable-handle">

  		     @if(session('address'))
                <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4><i class="icon fa fa-ban"></i> Attention, Your address is empty!</h4>
                  {{ session('address') }}
                </div>

              @endif


  		     @if(session('addressError'))
                <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4><i class="icon fa fa-ban"></i> Attention, Your address is different!</h4>
                  {{ session('addressError') }}
                </div>

              @endif

              
			<div class="box-content card bordered-all success">
			<h4 class="box-title bg-success"><i class="ico fa fa-usd"></i>My The Wallet Address</h4>
			<!-- /.box-title -->
			<!-- /.dropdown js__dropdown -->
				<div class="card-content">
				
							<div class="table-responsive" data-pattern="priority-columns">
								<table id="tech-companies-1" class="table table-small-font table-bordered table-striped">
									<thead>
										<tr>
											<th data-priority="1">Wallet</th>
											<th data-priority="1">Address</th>
										</tr>
									</thead>
									<tbody>

										@if ($walletAddresses)
										    	<tr>
											<th><span class="avatar"><img src="/user/images/members/{{ $walletAddresses->type }}.png" alt=""></span><br>{{ $walletAddresses->type }}</th>
											<td><span class="address">@if($walletAddresses){{ $walletAddresses->address }}@endif</span></td>
										</tr>  
										    @endif    


										
									</tbody>
								</table>
							</div> 


						<button type="button" data-remodal-target="remodal" class="btn btn-icon btn-icon-right btn-success btn-sm waves-effect waves-light margin-bottom-20"><i class="ico fa fa-edit"></i> Edit the Addresses</button>

						<div class="remodal" data-remodal-id="remodal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
							<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
							<div class="remodal-content">
								<h2 id="modal1Title">Edit my Wallet Addresses</h2>
									
									 <form method="POST" action="{{ route('member.walletAddresses.save') }}" class="frm-single">
								                    @csrf
								        <div class="inside">
								            <!-- /.frm-title -->

								           


								            <div class="frm-input">


								            	<label>Bitcoin Type</label>


								            	 <div class="radio primary coin-select"><input type="radio" name="type" value="bitcoin" id="radio-1" @if($walletAddresses){{ $walletAddresses->type == 'bitcoin' ? 'checked':'' }} }@endif><label for="radio-1">Bitcoin</label></div>
												{{-- <div class="radio primary coin-select"><input type="radio" name="type" value="ether" id="radio-2" @if($walletAddresses){{ $walletAddresses->type == 'ether' ? 'checked':'' }} }@endif><label for="radio-2">Ether</label></div>
												<div class="radio primary coin-select"><input type="radio" name="type" value="litecoin" id="radio-3" @if($walletAddresses) {{ $walletAddresses->type == 'litecoin' ? 'checked':'' }} }@endif><label for="radio-3">Litecoin</label></div>
												<div class="radio primary coin-select"><input type="radio" name="type" value="dogecoin" id="radio-4" @if($walletAddresses){{ $walletAddresses->type == 'dogecoin' ? 'checked':'' }} }@endif><label for="radio-4">Dogecoin</label></div> --}}



								            	<label>Bitcoin Address</label>

								                 <input id="address" type="text" class="frm-inp" name="address" value="@if($walletAddresses){{ $walletAddresses->address }}@endif" placeholder="Coin Address" autofocus ></i>
								                  
								            </div>
								         

								             <div class="frm-input" style="text-align: center;">
								        		<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
									     		<button type="submit" class="remodal-confirm">Save</button>

								      		 </div>
		   
								        </div>
								        <!-- .inside -->
								        
								    </form>
							</div>
							
						</div>

				</div> 
			</div>
			<!-- /.card-content -->
		</div>

  <!-- /page content -->
@endsection

@section('footer')

@endsection