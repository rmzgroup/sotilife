@extends('members/app')
@section('head')

	<!-- Sweet Alert -->
	<link rel="stylesheet" href="{{ asset('assets/plugin/sweet-alert/sweetalert.css') }}">
		
		<style type="text/css" media="screen">
			.ref-code{
				font-size: 11px;
			}
		</style>
@endsection
@section('pagename','My Profile')
@section('mainBody')

  <!-- page content -->
  	<div class="box-content card">
		<div class="row small-spacing">
			<div class="col-xs-12">

				<h4 class="box-title"><i class="fa fa-user ico"></i>About</h4>
					<!-- /.box-title -->
					<div class="dropdown js__drop_down">
						<a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
						<ul class="sub-menu">
							<li><a href="{{ route('member.editProfile') }}">Edit My Profile</a></li>
						</ul>
						<!-- /.sub-menu -->
					</div>


				<div class="row">
					
					<div class="col-md-3 col-xs-12">

						<div class="box-content bordered primary margin-bottom-20">
							<div class="profile-avatar">
								{{-- <img src="{{ Auth::user()->avatar == '' ? Auth::user()->sex == "Male"  ? "/avatar-male.png" : "/avatar-female.png" : '/storage/profile/'. Auth::user()->avatar }}" alt="{{ Auth::user()->name }}">
							 --}}
							 	<img src="{{ Auth::user()->avatar == '' ? Auth::user()->sex == "Male"  ? "/avatar-male.png" : "/avatar-female.png" : Storage::url(Auth::user()->avatar) }}" alt="">
								<a href="#" class="btn btn-block btn-inbox ">

									<h3><strong>{{ Auth::user()->name }}</strong></h3>
									Ref Link: 
									<input id="ref-code" type="text" class="ref-code margin-bottom-20 form-control" value="https://sotylife.com/register?code={{ Auth::user()->code }}" readonly title="Click to copy the code">
								</a>


							</div>
						
						</div>
					</div>
					<div class="col-md-6 col-xs-12">
				
						
						<!-- /.dropdown js__dropdown -->
						<div class="card-content">
							<div class="row">

								<div class="col-md-12">
									<div class="row">
										<div class="col-xs-3"><label>Member Since:</label></div>
										<!-- /.col-xs-3 -->
										<div class="col-xs-9">{{ \Carbon\Carbon::parse(Auth::user()->created_at)->format('d - m - Y')}}</div>
										<!-- /.col-xs-9 -->
									</div>
									<!-- /.row -->
								</div>
								<div class="col-md-12">
									<div class="row">
										<div class="col-xs-3"><label>Status:</label></div>
										<!-- /.col-xs-3 -->
										<div class="col-xs-9"><span class="notice notice-danger">{{ Auth::user()->status }}</span></div>
										<!-- /.col-xs-9 -->
									</div>
									<!-- /.row -->
								</div>
								<div class="col-md-12">
									<div class="row">
										<div class="col-xs-3"><label>First Name:</label></div>
										<!-- /.col-xs-3 -->
										<div class="col-xs-9">{{ Auth::user()->name }}</div>
										<!-- /.col-xs-9 -->
									</div>
									<!-- /.row -->
								</div>
								<!-- /.col-md-12 -->
									<!-- /.col-md-12 -->
								<div class="col-md-12">
									<div class="row">
										<div class="col-xs-3"><label>Email:</label></div>
										<!-- /.col-xs-3 -->
										<div class="col-xs-9">{{ Auth::user()->email }}</div>
										<!-- /.col-xs-9 -->
									</div>
									<!-- /.row -->
								</div>
								<!-- /.col-md-12 -->
								<div class="col-md-12">
									<div class="row">
										<div class="col-xs-3"><label>Country:</label></div>
										<!-- /.col-xs-3 -->
										<div class="col-xs-9">{{ Auth::user()->country }}</div>
										<!-- /.col-xs-9 -->
									</div>
									<!-- /.row -->
								</div>

								<!-- /.col-md-12 -->
								<div class="col-md-12">
									<div class="row">
										<div class="col-xs-3"><label>City:</label></div>
										<!-- /.col-xs-3 -->
										<div class="col-xs-9">{{ Auth::user()->city }}</div>
										<!-- /.col-xs-9 -->
									</div>
									<!-- /.row -->
								</div>
								
								<!-- /.col-md-12 -->
								<div class="col-md-12">
									<div class="row">
										<div class="col-xs-3"><label>Phone Number:</label></div>
										<!-- /.col-xs-5 -->
										<div class="col-xs-9">{{ Auth::user()->phone }}</div>
										<!-- /.col-xs-9 -->
									</div>
									<!-- /.row -->
								</div>
								<!-- /.col-md-12 -->

								<div class="col-lg-12 margin-top-20">
										<!-- /.dropdown js__dropdown -->
										<a href="{{ route('member.editProfile') }}" title="">
											<button type="button" class="btn btn-icon btn-icon-right btn-success btn-sm waves-effect waves-light"><i class="ico fa fa-edit"></i>Profilimi Güncele</button>
										</a>
										<!-- /.list-inline -->
								</div>
						
								<!-- /.col-md-6 -->
							</div>
							<!-- /.row -->
						</div>
						<!-- /.card-content -->
			
					<!-- /.row -->
					</div>



				</div>
			
				<!-- /.box-content bordered -->

			
				<!-- /.box-content -->
			</div>
			<!-- /.col-md-3 col-xs-12 -->
		
			<!-- /.col-md-9 col-xs-12 -->
		</div>
  	</div>
  <!-- /page content -->
@endsection

@section('footer')

	<script src="{{ asset('assets/plugin/sweet-alert/sweetalert.min.js') }}"></script>

<script type="text/javascript">
	document.getElementById("ref-code").onclick = function() {
	    this.select();
	    document.execCommand('copy');
	    swal("Copied!", "Your Reference code has been Copied", "success");
	}
</script>
@endsection