@extends('members/app')

@section('head')
	<style type="text/css" media="screen">
		.coin-select{
			font-size: 22px;
			font-weight: 700;

		}

		.radio label:before{
			top: 5px;
			height: 22px;
			width: 22px;

		}
		.radio label:after{
			top: 5px;
			left: 0px;
			height: 22px;
			width: 22px;
		}

		.radio label{
			padding-left: 35px;
		}
		.coin-box-title{
			font-weight: 700;
			font-size: 20px;
		}
		
		.payment-addresses img{
			background: #fff;
			border-radius: 50%;
			padding: 10px;
			margin-bottom: 10px;
			margin-right: 10px;
		}
	</style>


	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection


@section('pagename','Buy Soty')
@section('mainBody')

  <!-- page content -->



  <div class="row small-spacing" id="casheDeposit">
  		
  		<div class="box-content card ">
					<h4 class="box-title text-white">Pending Investment Requests</h4>
					<!-- /.box-title -->
					<div class="card-content">

						<div class="col-md-3 ">
								
						 	@if ($casheDepositRequest)
						 		<ul class="notice-list">
									<li>

										<a href="{{ route('member.casheDeposit.casheDepositConfirmation') }}">
											<span class="avatar"><img src="{{ asset('user/images/members/bitcoin.png') }}" alt=""></span>
											<span class="address-name"><h4>{{ $casheDepositRequest->user->address }}</h4></span>
											<span class="desc">Amount:  <span>	{{ $casheDepositRequest->amount_usd }} USD : {{ $casheDepositRequest->amount_crypto }} {{ $casheDepositRequest->coin_type }}</span> </span>
										</a>

									</li>
									
								</ul>

									<h4>

							<a href="{{ route('member.casheDeposit.casheDepositConfirmation') }}" class="btn  btn-rounded btn-bordered  waves-light text-white" style="background: #Ff7f00; color: #fff; border: none;" title="Details ...">Details ...</a></h4>


							 	@else
							 		<h4>You have no recent Investement Pending ...</h4>
							 	

						 	
							@endif

						</div>





				</div>
		</div>

					<div class="box-content  bg-navyblue text-white">
					<h4 class="box-title">Here are the Addresses to which you will perfom the Payments</h4>
					<!-- /.box-title -->
					<div class="row">
						<div class="col-md-3 col-xs-12"> 
							<div class="payment-addresses">
								<label for="radio-1"><img src="{{ asset('user/images/members/bitcoin.png') }}" width="50px;">Bitcoin Address: 3B4cBCZX7cXQyKrxsPPEZqv7jqCMTBDnsX</label>
							</div>
						</div>

						{{-- <div class="col-md-3 col-xs-12"> 
							<div class="payment-addresses">
								<label for="radio-1"><img src="{{ asset('user/images/members/ether.png') }}" width="50px;">Ether Address: </label>
							</div>
						</div>

						<div class="col-md-3 col-xs-12"> 
							<div class="payment-addresses">
								<label for="radio-1"><img src="{{ asset('user/images/members/litecoin.png') }}" width="50px;">Litecoin Address: </label>
							</div>
						</div>

						<div class="col-md-3 col-xs-12"> 
							<div class="payment-addresses">
								<label for="radio-1"><img src="{{ asset('user/images/members/dogecoin.png') }}" width="50px;">Dogecoin Address: </label>
							</div>
						</div> --}}

					</div>
				</div>
				<!-- /.box-content -->

  		<form id="investment-form" action="{{ route('member.casheDeposit.Send') }}" method="post" accept-charset="utf-8">
  				
  				  @csrf
  		
				<div class="box-content  bg-navyblue text-white">
					<h4 class="box-title">Now Select Your Investment</h4>
					<!-- /.box-title -->
					<p>The investment must be between 50-250$</p>
					<strong><p>Select Your Crypto :</p></strong>
					<div class="row">
						<div class="col-md-2 col-xs-4"> 
							<a href="#" class="thumbnail">
							<img alt="100%x180" data-src="holder.js/100%x180" src="{{ asset('user/images/members/bitcoin.png') }}" data-holder-rendered="true">
							</a>

							<div class="radio primary coin-select"><input type="radio" name="coin_type" value="bitcoin" id="radio-1" checked><label for="radio-1">Bitcoin</label></div>

						</div>
				{{-- 		<div class="col-md-2 col-xs-4"> 
							<a href="#" class="thumbnail">
							<img alt="100%x180" data-src="holder.js/100%x180" src="{{ asset('user/images/members/ether.png') }}" data-holder-rendered="true">
							</a>

							<div class="radio primary coin-select"><input type="radio" name="coin_type" value="ether" id="radio-2"><label for="radio-2">Ether</label></div>

						</div>
						<div class="col-md-2 col-xs-4"> 
							<a href="#" class="thumbnail">
							<img alt="100%x180" data-src="holder.js/100%x180" src="{{ asset('user/images/members/litecoin.png') }}" data-holder-rendered="true">
							</a>

							<div class="radio primary coin-select"><input type="radio" name="coin_type" value="litecoin" id="radio-3"><label for="radio-3">Litecoin</label></div>

						</div>
						<div class="col-md-2 col-xs-4"> 
							<a href="#" class="thumbnail">
							<img alt="100%x180" data-src="holder.js/100%x180" src="{{ asset('user/images/members/dogecoin.png') }}" data-holder-rendered="true">
							</a>

							<div class="radio primary coin-select"><input type="radio" name="coin_type" value="dogecoin" id="radio-4"><label for="radio-4">Dogecoin</label></div>

						</div> --}}
					</div>
				</div>
				<!-- /.box-content -->

				<div class="box-content card ">
					<h4 class="box-title text-white">Calculate your Investment</h4>
					<!-- /.box-title -->
					<div class="card-content">

						<div class="col-md-6 col-md-offset-3 ">
								
						 	
						 	 {{-- @{{ coinconvertions }} --}}

							<div class="form-group">
					    	 	<h4 class="coin-box-title">Type Your Investment:</h4>
								<label>Your Investment in USD </label>
									 <select name="amount_usd" id="amount_usd" class="form-control" >
								    	<option value="50" selected="true">50 USD</option>
								    	<option value="100">100 USD</option>
								    	<option value="150">150 USD</option>
								    	<option value="200">200 USD</option>
								    	<option value="250">250 USD</option>
								   </select>

							</div>

						</div>


						
							

						<div class="col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3 col-xs-12">
							<div class="form-group">
					    	 	<h4 class="coin-box-title">Your Balance Will Be :</h4>

								<label>Your Investment - <strong>Bitcoin</strong></label>
								<input type="text" class="form-control" id="bitcoin" name="amount_bitcoin" placeholder="How much do you invest?" value="">
							</div>

						</div>

					{{-- 	<div class="col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3 col-xs-12">
							<div class="form-group">
								<label>Your Investment - <strong>ETHER</strong> (ETH)*:</label>
								<input type="text" class="form-control" id="ether" name="amount_ether" placeholder="How much do you invest?" value="">
							</div>

						</div>

						<div class="col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3 col-xs-12">
							<div class="form-group">
								<label>Your Investment - <strong>LITECOIN</strong> (LTC)*:</label>
								<input type="text" class="form-control" id="litecoin" name="amount_litecoin" placeholder="How much do you invest?" value="">
							</div>

						</div>

						<div class="col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3 col-xs-12">
							<div class="form-group">
								<label>Your Investment - <strong>DOGECOIN</strong> (XDG)*:</label>
								<input type="text" class="form-control" id="dogecoin" name="amount_dogecoin" placeholder="How much do you invest?" value="">
							</div>

						</div> --}}

						<div class="col-md-3 col-md-offset-3 margin-bottom-20">
						
							@if ($casheDepositRequest)
							 <a href="" onclick="
                              if(confirm('Are you sure you want to perform a new Investment Request? This will cancel the pending requests. Click OK to continue'))
                                  {
                                    event.preventDefault();
                                    document.getElementById('investment-form').submit();
                                  }
                                  else{
                                    event.preventDefault();
                                  }" class="btn  btn-rounded btn-bordered  waves-light" style="background: #Ff7f00; color: #fff; border: none;"> Continue</a>

                             @else
                             		<button type="submit" class="btn  btn-rounded btn-bordered  waves-light" style="background: #Ff7f00; color: #fff; border: none;">Continue</button>
                             @endif     	
						</div>	

					</div>
					<!-- /.card-content -->
				</div>

  		</form>
  		
  </div>
  
  <!-- /page content -->
@endsection

@section('footer')

	   <script type="text/javascript">
	   	
	   	 $("select[name='amount_usd']").change(function(){
		      var amount_usd = $(this).val();

		       console.log(amount_usd);

		      var token = $("input[name='_token']").val();

			     $.ajax({
			          headers: {
			          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			          },
			          url: '/member/my-investments/convert/' + amount_usd,
			          dataType : 'json',
			          type: 'POST',
			          data: {},
			          contentType: false,
			          processData: false,
			          success:function(response) {
			               console.log(response);

			               $("#bitcoin").val(response.bitcoin);
			               $("#ether").val(response.ether);
			               $("#litecoin").val(response.litecoin);
			               $("#dogecoin").val(response.dogecoin);

			          },

			          error: function(xhr, status, error, req) {
						        console.log(error);
						}
			     });

		  });

	   </script>

	   
@endsection