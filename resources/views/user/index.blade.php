@extends('user.app') 
@section('title', 'Anasayfa')


@section('header')

@endsection

@section('main-content')
    
      @include('user.layouts.notifications')

    <!-- SLIDER SECTION START -->
    <div class="slider-3 youtube-bg bg-opacity-black-10 hidden-md hidden-xs hidden-sm">
        <div class="slider-content-3 text-center">
            <div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                <h1 class="slider-1-title-2">@lang('index.index_kiralık_daire_aciklama')</h1>
            </div>
            <div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="2s">
                <a class="slider-button mt-40" href="#">@lang('index.index_sotylife_nedir')</a>
            </div>
        </div>
    </div>
    <!-- SLIDER SECTION END -->

      <!-- SLIDER SECTION START -->
    <div class="slider-1 pos-relative slider-overlay hidden-lg">
        <div class="bend niceties preview-1">
            <div id="ensign-nivoslider-3" class="slides">   
                <img src="{{ asset('user/images/slider/1.jpg') }}" alt="" title="#slider-direction-1"  />
                <img src="{{ asset('user/images/slider/2.jpg') }}" alt="" title="#slider-direction-2"  />
                <img src="{{ asset('user/images/slider/3.jpg') }}" alt="" title="#slider-direction-3"  />
            </div>
            <!-- direction 1 -->
            <div id="slider-direction-1" class="slider-direction">
                <div class="slider-content text-center">
                    <div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                        <h4 class="slider-1-title-1"><span>@lang('index.index_sotylife')</span>@lang('index.index_dunyasi')</h4>
                    </div>
                    <div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                        <h1 class="slider-1-title-2">@lang('index.index_aciklama1')</h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">
                        <a class="slider-button mt-40" href="#">@lang('index.index_incele')</a>
                    </div>
                </div>
            </div>
            <!-- direction 2 -->
            <div id="slider-direction-2" class="slider-direction">
                <div class="slider-content text-left">
                    <div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                        <h4 class="slider-1-title-1"><span>@lang('index.index_sotylife')</span> @lang('index.index_dunyasi')</h4>
                    </div>
                    <div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                        <h1 class="slider-1-title-2">@lang('index.index_aciklama2')</h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">
                        <a class="slider-button mt-40" href="#">@lang('index.index_incele')</a>
                    </div>
                </div>
            </div>
            <!-- direction 2 -->
            <div id="slider-direction-3" class="slider-direction">
                <div class="slider-content text-right">
                    <div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                        <h4 class="slider-1-title-1"><span>@lang('index.index_sotylife')</span> @lang('index.index_dunyasi')</h4>
                    </div>
                    <div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                        <h1 class="slider-1-title-2">@lang('index.index_aciklama3')</h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">
                        <a class="slider-button mt-40" href="#">@lang('index.index_incele')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- SLIDER SECTION END -->

      <!-- Start page content -->
        <section id="page-content" class="page-wrapper">
            
            <!-- FIND HOME AREA START -->
            <div class="find-home-area bg-blue call-to-bg plr-140 pt-60 pb-20">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3 col-xs-12">
                            <div class="section-title text-white">
                                <h3>@lang('index.index_sotylife_ile')</h3>
                                <h2 class="h1">@lang('index.index_konfarlu_ev_tut')</h2>
                            </div>
                        </div>
                        <div class="col-md-9 col-xs-12">
                            <div class="find-homes">
                                <form action="{{ route('house.search') }}" method="get" accept-charset="utf-8">
                                  @csrf

                                      <div class="row">
                              
                                        <div class="col-sm-4 col-xs-12">
                                            <div class="find-home-item custom-select">                  
                                                <select class="selectpicker" title="Which City are you planing to visit?" data-hide-disabled="true" name="city">
                                                    <optgroup label="Select a city?">
                                                      
                                                        <option value="Avtonomna Respublika Krym">Avtonomna Respublika Krym</option>    
                                                        <option value="Cherkaska">Cherkaska</option> 
                                                        <option value="Chernihivska">Chernihivska</option>  
                                                        <option value="Chernivetska">Chernivetska</option>  
                                                        <option value="Dnipropetrovska">Dnipropetrovska</option>   
                                                        <option value="Donetska">Donetska</option>  
                                                        <option value="Ivano-Frankivska">Ivano-Frankivska</option>  
                                                        <option value="Kharkivska">Kharkivska</option>    
                                                        <option value="Khersonska">Khersonska</option>    
                                                        <option value="Khmelnytska">Khmelnytska</option>   
                                                        <option value="Kirovohradska">Kirovohradska</option> 
                                                        <option value="Kyiv">Kyiv</option> 
                                                        <option value="Kyivska">Kyivska</option>   
                                                        <option value="Luhanska">Luhanska</option>  
                                                        <option value="Lvivska">Lvivska</option>   
                                                        <option value="Mykolaivska">Mykolaivska</option>   
                                                        <option value="Odeska">Odeska</option>    
                                                        <option value="Poltavska">Poltavska</option> 
                                                        <option value="Rivnenska">Rivnenska</option> 
                                                        <option value="Sevastopol">Sevastopol</option>   
                                                        <option value="Sumska">Sumska</option>    
                                                        <option value="Ternopilska">Ternopilska</option>   
                                                        <option value="Vinnytska">Vinnytska</option> 
                                                        <option value="Volynska">Volynska</option>  
                                                        <option value="Zakarpatska">Zakarpatska</option>   
                                                        <option value="Zaporizka">Zaporizka</option> 
                                                        <option value="Zhytomyrska">Zhytomyrska</option>   

                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>


                                         <div class="col-sm-4 col-xs-12">
                                             <div class="find-home-item">
                                            <!-- shop-filter -->
                                                <div class="shop-filter">
                                                    <div class="price_filter">
                                                        <div class="price_slider_amount">
                                                            <input type="submit"  value="Price Interval :"/> 
                                                            <input type="text" id="amount" name="price"  placeholder="Add Your Price" /> 
                                                        </div>
                                                        <div id="slider-range"></div>
                                                    </div>
                                                </div>
                                            </div>

                                         </div>
                                        

                                        <div class="col-sm-3 col-xs-12">
                                            <div class="find-home-item mb-0-xs">
                                                 <button type="submit" class="button-1 btn-block btn-hover-1">@lang('index.index_ara')</button>
                                               
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIND HOME AREA END -->
            
            <!-- ABOUT SHELTEK AREA START -->
            <div class="about-sheltek-area ptb-115">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="section-title mb-30">
                                <h3>@lang('index.index_sotylife') </h3>
                                <h2>@lang('index.index_hakkimizda')</h2>
                            </div>
                            <div class="about-sheltek-info">
                                <p><span data-placement="top" data-toggle="tooltip" data-original-title="The name you can trust" class="tooltip-content">@lang('index.about_us_isimllc')</span>@lang('index.index_aciklama4')</p>

                                <p>@lang('index.index_aciklama5')</p>
                                
                                <p>@lang('index.index_aciklama6')</p>

                                <a href="#" title="Fazla Oku">@lang('index.index_daha_fazla_oku')</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="about-image">
                                <a href="javascript:void(0)"><img src="user/images/about/4.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ABOUT SHELTEK AREA END -->
            
            <!-- SERVICES AREA START -->
            <div class="services-area pb-60">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title-2 text-center">
                                <h2>@lang('index.index_servislerimiz')</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="service-carousel">
                            <!-- service-item -->
                            <div class="col-md-12">
                                <div class="service-item">
                                    <div class="service-item-image">
                                        <a href="{{ route('servislerimiz') }}"><img src="user/images/service/1.jpg" alt=""></a>
                                    </div>
                                    <div class="service-item-info">
                                        <h5><a href="{{ route('servislerimiz') }}">@lang('index.index_emlak_danismanligi')</a></h5>
                                        <p>
                                            @lang('index.index_aciklama7')
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- service-item -->
                            <div class="col-md-12">
                                <div class="service-item">
                                    <div class="service-item-image">
                                        <a href="{{ route('servislerimiz') }}"><img src="user/images/service/2.jpg" alt=""></a>
                                    </div>
                                    <div class="service-item-info">
                                        <h5><a href="{{ route('servislerimiz') }}">@lang('index.index_reh_tic_danismanligi')</a></h5>
                                        <p>
                                            @lang('index.index_aciklama8') 
                                        </p>
                                    </div>
                                </div>
                            </div>

                                 <!-- service-item -->
                            <div class="col-md-12">
                                <div class="service-item">
                                    <div class="service-item-image">
                                        <a href="{{ route('servislerimiz') }}"><img src="user/images/service/7.jpg" alt=""></a>
                                    </div>
                                    <div class="service-item-info">
                                        <h5><a href="{{ route('servislerimiz') }}">Education Consultancy</a></h5>
                                        <p>
                                          Ukrainian universities, which implement the most appreciated education systems in the world, these educational institutions fall under the Bologna agreement. 
                                        </p>
                                    </div>
                                </div>
                            </div>
                       

                            <!-- service-item -->
                            <div class="col-md-12">
                                <div class="service-item">
                                    <div class="service-item-image">
                                        <a href="{{ route('servislerimiz') }}"><img src="user/images/service/3.jpg" alt=""></a>
                                    </div>
                                    <div class="service-item-info">
                                        <h5><a href="{{ route('servislerimiz') }}">@lang('index.navbar_kripto_forex')</a></h5>
                                        <p>
                                            @lang('index.index_kripto_aciklama')
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- service-item -->
                          
                            
                            <div class="col-md-12">
                                <div class="service-item">
                                    <div class="service-item-image">
                                        <a href="{{ route('servislerimiz') }}"><img src="user/images/service/5.jpg" alt=""></a>
                                    </div>
                                    <div class="service-item-info">
                                        <h5><a href="{{ route('servislerimiz') }}">@lang('index.index_havaalani_transfer')</a></h5>
                                        <p>
                                            @lang('index.index_havaalani_transfer_aciklama')
                                        </p>
                                    </div>
                                </div>
                            </div>

                                   <div class="col-md-12">
                                <div class="service-item">
                                    <div class="service-item-image">
                                        <a href="{{ route('servislerimiz') }}"><img src="user/images/service/6.jpg" alt=""></a>
                                    </div>
                                    <div class="service-item-info">
                                        <h5><a href="{{ route('servislerimiz') }}">@lang('index.index_rantacar') </a></h5>
                                        <p>
                                            @lang('index.index_rantacar_aciklama')
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- SERVICES AREA END -->
            
            <!-- BOOKING AREA START -->
            <div class="booking-area bg-1 call-to-bg plr-140 pt-75">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="section-title text-white">
                                <h3>@lang('index.index_book_your')</h3>
                                <h2 class="h1">@lang('index.index_home_here')</h2>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-12">
                            <div class="booking-conternt clearfix">
                                <div class="book-house text-white">
                                    <h3>@lang('index.index_aciklama9') </h3>
                                    <h2 class="h5">@lang('index.index_ara_tel') </h2>
                                </div>
                                <div class="booking-imgae">
                                    <img src="user/images/others/woman-smiling-png.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- BOOKING AREA END -->
            
            <!-- FEATURED FLAT AREA START -->
            <div class="featured-flat-area pt-115 pb-80">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title-2 text-center">
                                <h2>@lang('index.index_gunluk_kiralık')</h2>
                                <p>@lang('index.index_aciklama10')</p>
                            </div>
                        </div>
                    </div>
                    <div class="featured-flat">
                        <div class="row">
                            <!-- flat-item -->



                                    @foreach ($houses as $house)
                                         <!-- flat-item -->
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <div class="flat-item">
                                                    <div class="flat-item-image">
                                                        <span class="for-sale">{{ $house->price }} USD / Per day</span>
                                                        <a href="{{ route('ilanDetaylari',$house->id) }}"><img src="../../.../../../../upload/{{ $house->main_image }}" alt=""></a>
                                                        <div class="flat-link">
                                                            <a href="{{ route('ilanDetaylari',$house->id) }}">Details</a>
                                                        </div>
                                                        <ul class="flat-desc">
                                                            <li>
                                                                <img src="/user/images/icons/4.png" alt="">
                                                                <span>{{ $house->dimension }} m2</span>
                                                            </li>
                                                            <li>
                                                                <img src="/user/images/icons/5.png" alt="">
                                                                <span>{{ $house->room_number }}</span>
                                                            </li>
                                                            <li>
                                                                <img src="/user/images/icons/6.png" alt="">
                                                                <span>{{ $house->bathrom_number }}</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="flat-item-info">
                                                        <div class="flat-title-price">
                                                            <h5><a href="{{ route('ilanDetaylari',$house->id) }}">{{ $house->title }}</a></h5>
                                                        </div>
                                                        <p><img src="/user/images/icons/location.png" alt="">{{ $house->address }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach

                                    
                                </div>
                            </div>
                            <!-- flat-item -->
               
                      
                </div>
            </div>
            <!-- FEATURED FLAT AREA END -->
            
            <!-- FEATURES AREA START -->
            <div class="features-area fix">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-7 col-md-offset-5">
                            <div class="features-info bg-gray">
                                <div class="section-title mb-30">
                                    <h3>Ukraine</h3>
                                    <h2 class="h1"> Apartments for rent in amazing cities</h2>
                                </div>
                               
                                <div class="features-include">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-6 col-sm-4">
                                            <div class="features-include-list">
                                                <h6><img src="{{ asset('user/images/icons/7.png') }}" alt="">Secure booking</h6>
                                                <p>Guarantees for tourists and owners</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-4">
                                            <div class="features-include-list">
                                                <h6><img src="{{ asset('user/images/icons/7.png') }}" alt="">Reservation savings</h6>
                                                <p>Extra commission booking</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-4">
                                            <div class="features-include-list">
                                                <h6><img src="{{ asset('user/images/icons/7.png') }}" alt="">Real estate from owners</h6>
                                                <p>Without agents and agencies</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-4">
                                            <div class="features-include-list">
                                                <h6><img src="{{ asset('user/images/icons/7.png') }}" alt="">Find Easily</h6>
                                                <p>According to ad details and comments, choose your favorite apartments for daily rent.</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-4">
                                            <div class="features-include-list">
                                                <h6><img src="{{ asset('user/images/icons/7.png') }}" alt="">Pay with Confidence</h6>
                                                <p style="text-align: left;">Take advantage of the 9-month installment options for a secure payment system and credit card.</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-4">
                                            <div class="features-include-list">
                                                <h6><img src="{{ asset('user/images/icons/7.png') }}" alt="">Stay comfortably</h6>
                                                <p style="text-align: left;">Affordable and comfortable accommodation in daily rental homes, apartments and summer villas</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FEATURES AREA END -->
            
            <!-- BLOG AREA START -->
           {{--  <div class="blog-area pb-60 pt-100">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title-2 text-center">
                                <h2>SON DUYURULAR</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="blog-carousel">
                            <!-- blog-item -->
                            <div class="col-md-12">
                                <article class="blog-item bg-gray">
                                    <div class="blog-image">
                                        <a href="single-blog.html"><img src="user/images/blog/1.jpg" alt=""></a>
                                    </div>
                                    <div class="blog-info">
                                        <div class="post-title-time">
                                            <h5><a href="single-blog.html">Maridland de Villa</a></h5>
                                            <p>July 30, 2016 / 10 am</p>
                                        </div>
                                        <p>Lorem must explain to you how all this mistaolt denouncing pleasure and praising pain wasnad I will give you a complete pain was praising</p>
                                        <a class="read-more" href="single-blog.html">İncele</a>
                                    </div>
                                </article>
                            </div>
                            <!-- blog-item -->
                            <div class="col-md-12">
                                <article class="blog-item bg-gray">
                                    <div class="blog-image">
                                        <a href="single-blog.html"><img src="user/images/blog/2.jpg" alt=""></a>
                                    </div>
                                    <div class="blog-info">
                                        <div class="post-title-time">
                                            <h5><a href="single-blog.html">Latest Design House</a></h5>
                                            <p>July 30, 2016 / 10 am</p>
                                        </div>
                                        <p>Lorem must explain to you how all this mistaolt denouncing pleasure and praising pain wasnad I will give you a complete pain was praising</p>
                                        <a class="read-more" href="single-blog.html">İncele</a>
                                    </div>
                                </article>
                            </div>
                            <!-- blog-item -->
                            <div class="col-md-12">
                                <article class="blog-item bg-gray">
                                    <div class="blog-image">
                                        <a href="single-blog.html"><img src="user/images/blog/3.jpg" alt=""></a>
                                    </div>
                                    <div class="blog-info">
                                        <div class="post-title-time">
                                            <h5><a href="single-blog.html">Duplex Villa House</a></h5>
                                            <p>July 30, 2016 / 10 am</p>
                                        </div>
                                        <p>Lorem must explain to you how all this mistaolt denouncing pleasure and praising pain wasnad I will give you a complete pain was praising</p>
                                        <a class="read-more" href="single-blog.html">İncele</a>
                                    </div>
                                </article>
                            </div>
                            <!-- blog-item -->
                            <div class="col-md-12">
                                <article class="blog-item bg-gray">
                                    <div class="blog-image">
                                        <a href="single-blog.html"><img src="user/images/blog/2.jpg" alt=""></a>
                                    </div>
                                    <div class="blog-info">
                                        <div class="post-title-time">
                                            <h5><a href="single-blog.html">Latest Design House</a></h5>
                                            <p>July 30, 2016 / 10 am</p>
                                        </div>
                                        <p>Lorem must explain to you how all this mistaolt denouncing pleasure and praising pain wasnad I will give you a complete pain was praising</p>
                                        <a class="read-more" href="single-blog.html">İncele</a>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            <!-- BLOG AREA END -->
        
        </section>
        <!-- End page content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $('.datepicker').datepicker();
    </script>
@endsection