 <!-- HEADER AREA START -->
        <header class="header-area header-wrapper">
          

            <div class="header-top-bar bg-white">
                <div class="container" >
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="logo">
                                <a href="/">
                                    <img src="{{ asset('user/images/logo/logo.png') }}" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 hidden-sm hidden-xs">
                            <div class="company-info clearfix">
                                {{-- <div class="company-info-item">
                                    <div class="header-icon">
                                        <img src="{{ asset('user/images/icons/phone.png') }}" alt="">
                                    </div>
                                    <div class="header-info">
                                        <h6>@lang('index.navbar_telefon')</h6>
                                        <p><a href="mailto:hnasir770@gmail.com">@lang('index.navbar_email')</a></p>
                                    </div>
                                </div> --}}
                               {{--  <div class="company-info-item">
                                    <div class="header-icon">
                                        <img src="{{ asset('user/images/icons/world.png') }}" alt="">
                                    </div>
                                    <div class="header-info">
                                        <ul class="lang">
                                            <li><a href="/en">EN</a></li>
                                            <li><a href="/tr">TR</a></li>
                                            <li><a href="/ru">RU</a></li>
                                       </ul>
                                 
                                       
                                     </div>
                                </div> --}}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="header-search clearfix">
                                <div class="col-sm-6 col-md-6 col-xs-6">
                                <a class="button-3 btn-block" href="{{ route('login') }}">@lang('index.navbar_login')</a>
                            </div> 
                            <div class="col-sm-6 col-md-6 col-xs-6">
                                <a class="button-3 btn-block" href="{{ route('register') }}">@lang('index.navbar_register')</a>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sticky-header" class="header-middle-area  transparent-header hidden-xs">
                <div class="container">
                    <div class="full-width-mega-drop-menu">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="sticky-logo">
                                    <a href="/">
                                        <img src="{{ asset('user/images/logo/logo.png') }}" alt="">
                                    </a>
                                </div>
                                <nav id="primary-menu">
                                    <ul class="main-menu text-center">
                                        <li><a href="/" class="active">@lang('index.navbar_anasayfa')</a></li>
                                        <li><a href="{{ route('hakkinda') }}">@lang('index.navbar_hakkimizda')</a></li>
                                        <li><a href="{{ route('servislerimiz') }}">@lang('index.navbar_servislerimiz')</a>
                                            <ul class="drop-menu">

                                                 <li><a href="{{ route('servislerimiz') }}">@lang('index.index_emlak_danismanligi')</a></li>
                                                <li><a href="{{ route('servislerimiz') }}">@lang('index.index_reh_tic_danismanligi')</a></li>
                                                <li><a href="{{ route('servislerimiz') }}">Education Consultancy</a></li>
                                                <li><a href="{{ route('servislerimiz') }}">@lang('index.navbar_kripto_forex')</a></li>
                                                <li><a href="{{ route('servislerimiz') }}">@lang('index.index_havaalani_transfer')</a></li>
                                                <li><a href="{{ route('servislerimiz') }}">@lang('index.index_rantacar') </a></li>

                                        
                                            </ul>
                                        </li>

                                        <li><a href="#">@lang('index.navbar_güncel_haber')</a></li>
                                        <li><a href="{{ route('iletisim') }}">@lang('index.navbar_iletisim')</a></li>
                                    
                                       
                                       
                                       
                                    

                                        {{-- <li class="pull-right services-menu"><a href="{{ route('rentacar') }}">@lang('index.navbar_arac_kiralama')</a></li> --}}
                                        <li class="pull-right services-menu"><a href="{{ route('tumIlanlar') }}">@lang('index.navbar_ev_kiralama')</a></li>

                                       

                                    </ul>

                                   

                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                

            </div>
        </header>
        <!-- HEADER AREA END -->



  <!-- MOBILE MENU AREA START -->
        <div class="mobile-menu-area hidden-sm hidden-md hidden-lg">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="mobile-menu">
                            <nav id="dropdown">
                                <ul>
                                     <li><a href="/" class="active">@lang('index.navbar_anasayfa')</a></li>
                                        <li><a href="{{ route('hakkinda') }}">@lang('index.navbar_hakkimizda')</a></li>
                                        <li><a href="{{ route('servislerimiz') }}">@lang('index.navbar_servislerimiz')</a>
                                        <ul>
                                           
                                                 <li><a href="{{ route('servislerimiz') }}">@lang('index.index_emlak_danismanligi')</a></li>
                                                <li><a href="{{ route('servislerimiz') }}">@lang('index.index_reh_tic_danismanligi')</a></li>
                                                <li><a href="{{ route('servislerimiz') }}">Education Consultancy</a></li>
                                                <li><a href="{{ route('servislerimiz') }}">@lang('index.navbar_kripto_forex')</a></li>
                                                <li><a href="{{ route('servislerimiz') }}">@lang('index.index_havaalani_transfer')</a></li>
                                                <li><a href="{{ route('servislerimiz') }}">@lang('index.index_rantacar') </a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">@lang('index.navbar_güncel_haber')</a></li>
                                    <li><a href="{{ route('iletisim') }}">@lang('index.navbar_iletisim')</a></li>

                                    {{-- <li class="pull-right services-menu"><a href="{{ route('iletisim') }}">@lang('index.navbar_arac_kiralama')</a></li> --}}
                                    <li class="pull-right services-menu"><a href="{{ route('tumIlanlar') }}">@lang('index.navbar_ev_kiralama')</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- MOBILE MENU AREA END -->