
     <meta charset="UTF-8">
     <meta name="author" content="NourKas Co.">
     <meta name="city" content="Konya"/> 
     <meta name="country" content="tr"/> 
     <meta name="Audience" content="All"/> 
     <meta name="Owner" content="Sotilife."/>
     <meta name="Copyright" content="Sotilife"/>
     <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
     <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
     <meta name="keywords" content="sotylife" />
     <meta name="description" content= "@yield('description')"/>
 
     <title>@yield('title') | Sotilife</title>  
 
     <meta property="og:site_name" content="Sotilife">
     <meta property="og:title" content="@yield('title') | Sotilife">
     <meta property="og:description" content="SOTYLIFE LLC, provides business guidance and consulting services to businesses. It also provides business consultancy services in the fields of business administration and follow up all transactions, establishment of company, work permit, marriage, sworn translator, notary public and deport.">
     <meta property="og:type" content="website">
     <meta property="og:image" content="{{ asset('user/images/logo/logo.png') }}">
     <meta property="og:url" content="www.sotylife.com">
 
 
         <!-- Favicon -->
     <link rel="shortcut icon" href="{{ asset('user/images/logo/favicon.ico') }}" type="image/x-icon"> 
     <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('user/images/logo/apple-touch-icon-144-precomposed.png') }}">
     <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('user/images/logo/apple-touch-icon-114-precomposed.png') }}">
     <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('user/images/logo/apple-touch-icon-72-precomposed.png') }}">
     <link rel="apple-touch-icon-precomposed" href="{{ asset('user/images/logo/apple-touch-icon-57-precomposed.png') }}">
 

    <!-- All css files are included here. -->
    <!-- Bootstrap fremwork main css -->
    <link rel="stylesheet" href="{{ asset('user/css/bootstrap.min.css') }}">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="{{ asset('user/lib/css/nivo-slider.css') }}"/>
    <!-- This core.css file contents all plugings css file. -->
    <link rel="stylesheet" href="{{ asset('user/css/core.css') }}">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="{{ asset('user/css/shortcode/shortcodes.css') }}">
    <!-- Theme main style -->
    <link rel="stylesheet" href="{{ asset('user/css/style.css') }}">
    <!-- Responsive css -->
    <link rel="stylesheet" href="{{ asset('user/css/responsive.css') }}">
    <!-- Template color css -->
    <link href="{{ asset('user/css/color/skin-orenge.css') }}" data-style="styles" rel="stylesheet">
    <!-- User style -->
    <link rel="stylesheet" href="{{ asset('user/css/custom.css') }}">
     <!-- Datepicker style -->
     <link rel="stylesheet" href="{{ asset('user/css/bootstrap-datepicker.min.css') }}">


    <!-- Modernizr JS -->
    <script src="{{ asset('user/js/vendor/modernizr-2.8.3.min.js') }}"></script>

    @section('header')
    @show
