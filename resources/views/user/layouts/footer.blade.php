 <!-- Start footer area -->
        <footer id="footer" class="footer-area bg-2 bg-opacity-black-90">
            <div class="footer-top pt-110 pb-80">
                <div class="container">
                    <div class="row">



                        <!-- footer-address -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="footer-widget">
                                <h6 class="footer-titel">@lang('index.iletisim')</h6>
                                <ul class="footer-address">
                                  {{--   <li>
                                        <div class="address-icon">
                                            <img src="user/images/icons/location-2.png" alt="">
                                        </div>
                                        <div class="address-info">
                                            <span>@lang('index.adres1')</span>
                                            <span>@lang('index.adres2')</span>
                                        </div>
                                    </li> --}}
                                  {{--   <li>
                                        <div class="address-icon">
                                            <img src="user/images/icons/phone-3.png" alt="">
                                        </div>
                                        <div class="address-info">
                                            <span>@lang('index.telefon1')</span>
                                            <span>@lang('index.telefon2')</span>
                                        </div>
                                    </li> --}}
                                    <li>
                                        <div class="address-icon">
                                            <img src="user/images/icons/world.png" alt="">
                                        </div>
                                        <div class="address-info">
                                            <span>@lang('index.email')</span>
                                            <span>Web :<a href="javascript:void(0)" target="_blank"> www.sotylife.com</a></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>


                                 <!-- footer-latest-news -->
                        <div class="col-lg-5 col-md-5 hidden-sm col-xs-12">
                            <div class="footer-widget middle">

                                   <div class="logo" style="text-align: center; ">
                                        <a href="/">
                                            <img src="{{ asset('user/images/logo/logo.png') }}" alt="">
                                        </a>
                                    </div>

                                <ul class="footer-latest-news">
                                    <li>
                                        <div class="latest-news-info">
                                            <p style="text-align: justify;">@lang('index.bilgi')</p>
                                        </div>
                                    </li>
                              
                                </ul>
                            </div>
                        </div>
                      
                        <!-- footer-contact -->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="footer-widget">
                                <h6 class="footer-titel">@lang('index.iletisime_gec')</h6>
                                <div class="footer-contact">
                                    <p>@lang('index.iletisime_yazı')</p>
                                    <form class="form-horizontal" action="{{ route('contact.mail2') }}" method="POST">
                                        
                                        @csrf

                                        <input type="text" name="name" placeholder="Name and Surname" required="true">
                                        <input type="email" name="email" placeholder="@lang('index.e_posta_yaz')" required="true">
                                        <textarea name="message" placeholder="@lang('index.mesaj_yaz')"></textarea>
                                        <button type="submit" value="button-3 btn-block">@lang('index.gönder')</button>
                                    </form>
                                    <p class="form-messege"></p>
                                </div>
                            </div>
                        </div>

                 

                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="copyright text-center">
                                <p>@lang('index.yazılım_alt_bilgi')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End footer area -->