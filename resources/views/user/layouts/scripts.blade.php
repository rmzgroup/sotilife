
    <!-- jquery latest version -->
    <script src="{{ asset('user/js/vendor/jquery-3.1.1.min.js') }}"></script>
    <!-- Bootstrap framework js -->
    <script src="{{ asset('user/js/bootstrap.min.js') }}"></script>
    <!-- Nivo slider js -->    
    <script src="{{ asset('user/lib/js/jquery.nivo.slider.js') }}"></script>
    <!-- ajax-mail js -->
    <script src="{{ asset('user/js/ajax-mail.js') }}"></script>
    <!-- All js plugins included in this file. -->
    <script src="{{ asset('user/js/plugins.js') }}"></script>
    <!-- Main js file that contents all jQuery plugins activation. -->
    <script src="{{ asset('user/js/main.js') }}"></script>
     <!-- Datepicker plugin -->
    <script src="{{ asset('user/js/bootstrap-datepicker.min.js') }}"></script>
    @section('scripts')
    @show

   