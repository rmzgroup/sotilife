<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    @include('user.layouts.header')
</head>
<body>
	
    {{-- <div class="page_loader"></div> --}}
   
    <!-- Body main wrapper start -->
    <div class="wrapper">
        

        @include('user.layouts.navbar')
         
        @section('main-content')
           
        @show


        {{-- footer start --}}
        @include('user.layouts.footer')
        {{-- footer end --}}

    </div>
    <!-- Body main wrapper end -->  

    @include('user.layouts.scripts')

</body>
</html>