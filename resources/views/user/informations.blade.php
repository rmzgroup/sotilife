@extends('user.app') 
@section('title', 'Duyurularımız')


@section('header')

@endsection

@section('main-content')


        <!-- BREADCRUMBS AREA START -->
        <div class="breadcrumbs-area bread-bg-information bg-opacity-black-70">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h2 class="breadcrumbs-title">@lang('index.information_guncel_haberler')</h2>
                            <ul class="breadcrumbs-list">
                                <li><a href="/">@lang('index.information_anasayfa')</a></li>
                                <li>@lang('index.information_guncel_haberler')</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMBS AREA END -->

        <!-- Start page content -->
        <div id="page-content" class="page-wrapper">

             <!-- BLOG AREA START -->
            <div class="blog-area pt-115 pb-60">
                <div class="container">

                      @include('user.layouts.notifications')
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <!-- blog-item -->
                                <div class="col-sm-6 col-xs-12">
                                    <article class="blog-item bg-gray">
                                        <div class="blog-image">
                                            <a href="{{ route('duyuru','detaylari') }}"><img src="/user/images/blog/1.jpg" alt=""></a>
                                        </div>
                                        <div class="blog-info">
                                            <div class="post-title-time">
                                                <h5><a href="{{ route('duyuru','detaylari') }}">Maridland de Villa</a></h5>
                                                <p>July 30, 2016 / 10 am</p>
                                            </div>
                                            <p>Lorem must explain to you how all this mistaolt denouncing pleasure and praising pain wasnad I will give you a complete pain was praising</p>
                                            <a class="read-more" href="{{ route('duyuru','detaylari') }}">Read more</a>
                                        </div>
                                    </article>
                                </div>
                                <!-- blog-item -->
                                <div class="col-sm-6 col-xs-12">
                                    <article class="blog-item bg-gray">
                                        <div class="blog-image">
                                            <a href="{{ route('duyuru','detaylari') }}"><img src="/user/images/blog/2.jpg" alt=""></a>
                                        </div>
                                        <div class="blog-info">
                                            <div class="post-title-time">
                                                <h5><a href="{{ route('duyuru','detaylari') }}">Latest Design House</a></h5>
                                                <p>July 30, 2016 / 10 am</p>
                                            </div>
                                            <p>Lorem must explain to you how all this mistaolt denouncing pleasure and praising pain wasnad I will give you a complete pain was praising</p>
                                            <a class="read-more" href="{{ route('duyuru','detaylari') }}">Read more</a>
                                        </div>
                                    </article>
                                </div>
                                <!-- blog-item -->
                                <div class="col-sm-6 col-xs-12">
                                    <article class="blog-item bg-gray">
                                        <div class="blog-image">
                                            <a href="{{ route('duyuru','detaylari') }}"><img src="/user/images/blog/3.jpg" alt=""></a>
                                        </div>
                                        <div class="blog-info">
                                            <div class="post-title-time">
                                                <h5><a href="{{ route('duyuru','detaylari') }}">Duplex Villa House</a></h5>
                                                <p>July 30, 2016 / 10 am</p>
                                            </div>
                                            <p>Lorem must explain to you how all this mistaolt denouncing pleasure and praising pain wasnad I will give you a complete pain was praising</p>
                                            <a class="read-more" href="{{ route('duyuru','detaylari') }}">Read more</a>
                                        </div>
                                    </article>
                                </div>
                                <!-- blog-item -->
                                <div class="col-sm-6 col-xs-12">
                                    <article class="blog-item bg-gray">
                                        <div class="blog-image">
                                            <a href="{{ route('duyuru','detaylari') }}"><img src="/user/images/flat/1.jpg" alt=""></a>
                                        </div>
                                        <div class="blog-info">
                                            <div class="post-title-time">
                                                <h5><a href="{{ route('duyuru','detaylari') }}">Latest Design House</a></h5>
                                                <p>July 30, 2016 / 10 am</p>
                                            </div>
                                            <p>Lorem must explain to you how all this mistaolt denouncing pleasure and praising pain wasnad I will give you a complete pain was praising</p>
                                            <a class="read-more" href="{{ route('duyuru','detaylari') }}">Read more</a>
                                        </div>
                                    </article>
                                </div>
                                <!-- blog-item -->
                                <div class="col-sm-6 col-xs-12">
                                    <article class="blog-item bg-gray">
                                        <div class="blog-image">
                                            <a href="{{ route('duyuru','detaylari') }}"><img src="/user/images/flat/2.jpg" alt=""></a>
                                        </div>
                                        <div class="blog-info">
                                            <div class="post-title-time">
                                                <h5><a href="{{ route('duyuru','detaylari') }}">Maridland de Villa</a></h5>
                                                <p>July 30, 2016 / 10 am</p>
                                            </div>
                                            <p>Lorem must explain to you how all this mistaolt denouncing pleasure and praising pain wasnad I will give you a complete pain was praising</p>
                                            <a class="read-more" href="{{ route('duyuru','detaylari') }}">Read more</a>
                                        </div>
                                    </article>
                                </div>
                                <!-- blog-item -->
                                <div class="col-sm-6 col-xs-12">
                                    <article class="blog-item bg-gray">
                                        <div class="blog-image">
                                            <a href="{{ route('duyuru','detaylari') }}"><img src="/user/images/flat/3.jpg" alt=""></a>
                                        </div>
                                        <div class="blog-info">
                                            <div class="post-title-time">
                                                <h5><a href="{{ route('duyuru','detaylari') }}">Latest Design House</a></h5>
                                                <p>July 30, 2016 / 10 am</p>
                                            </div>
                                            <p>Lorem must explain to you how all this mistaolt denouncing pleasure and praising pain wasnad I will give you a complete pain was praising</p>
                                            <a class="read-more" href="{{ route('duyuru','detaylari') }}">Read more</a>
                                        </div>
                                    </article>
                                </div>
                                <!-- blog-item -->
                                <div class="col-sm-6 col-xs-12">
                                    <article class="blog-item bg-gray">
                                        <div class="blog-image">
                                            <a href="{{ route('duyuru','detaylari') }}"><img src="/user/images/flat/4.jpg" alt=""></a>
                                        </div>
                                        <div class="blog-info">
                                            <div class="post-title-time">
                                                <h5><a href="{{ route('duyuru','detaylari') }}">Duplex Villa House</a></h5>
                                                <p>July 30, 2016 / 10 am</p>
                                            </div>
                                            <p>Lorem must explain to you how all this mistaolt denouncing pleasure and praising pain wasnad I will give you a complete pain was praising</p>
                                            <a class="read-more" href="{{ route('duyuru','detaylari') }}">Read more</a>
                                        </div>
                                    </article>
                                </div>
                                <!-- blog-item -->
                                <div class="col-sm-6 col-xs-12">
                                    <article class="blog-item bg-gray">
                                        <div class="blog-image">
                                            <a href="{{ route('duyuru','detaylari') }}"><img src="/user/images/flat/5.jpg" alt=""></a>
                                        </div>
                                        <div class="blog-info">
                                            <div class="post-title-time">
                                                <h5><a href="{{ route('duyuru','detaylari') }}">Latest Design House</a></h5>
                                                <p>July 30, 2016 / 10 am</p>
                                            </div>
                                            <p>Lorem must explain to you how all this mistaolt denouncing pleasure and praising pain wasnad I will give you a complete pain was praising</p>
                                            <a class="read-more" href="{{ route('duyuru','detaylari') }}">Read more</a>
                                        </div>
                                    </article>
                                </div>
                                <!-- pagination-area -->
                                <div class="col-xs-12">
                                    <div class="pagination-area mb-60">
                                        <ul class="pagination-list text-center">
                                            <li><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- widget-search -->
                            <aside class="widget widget-search mb-30">
                                <form action="#">
                                    <input type="text" name="search" placeholder="Duyuru ara...">
                                    <button type="button" class=""><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </aside>
                            <!-- widget-categories -->
                            <aside class="widget widget-categories mb-50">
                                <h5>Duyuru Kategoriler</h5>
                                <ul class="widget-categories-list">
                                    <li><a href="#">Apartment</a></li>
                                    <li><a href="#">Apartment Building</a></li>
                                    <li><a href="#">Bungalow <span>670</span></a></li>
                                    <li><a href="#">Corporate House <span>520</span></a></li>
                                    <li><a href="#">Duplex Villa <span>350</span></a></li>
                                </ul>
                            </aside>
                            <!-- widget-recent-post -->
                            <aside class="widget widget-recent-post mb-50">
                                <h5>Son Eklenenler</h5>
                                <div class="row">
                                    <!-- blog-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <article class="recent-post-item">
                                            <div class="recent-post-image">
                                                <a href="{{ route('duyuru','detaylari') }}"><img src="/user/images/recent-post/1.jpg" alt=""></a>
                                            </div>
                                            <div class="recent-post-info">
                                                <div class="recent-post-title-time">
                                                    <h5><a href="{{ route('duyuru','detaylari') }}">Maridland de Villa</a></h5>
                                                    <p>July 30, 2016 / 10 am</p>
                                                </div>
                                                <p>Lorem must explain to you how all this mistaolt</p>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- blog-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <article class="recent-post-item">
                                            <div class="recent-post-image">
                                                <a href="{{ route('duyuru','detaylari') }}"><img src="/user/images/recent-post/2.jpg" alt=""></a>
                                            </div>
                                            <div class="recent-post-info">
                                                <div class="recent-post-title-time">
                                                    <h5><a href="{{ route('duyuru','detaylari') }}">Maridland de Villa</a></h5>
                                                    <p>July 30, 2016 / 10 am</p>
                                                </div>
                                                <p>Lorem must explain to you how all this mistaolt</p>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- blog-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <article class="recent-post-item">
                                            <div class="recent-post-image">
                                                <a href="{{ route('duyuru','detaylari') }}"><img src="/user/images/recent-post/3.jpg" alt=""></a>
                                            </div>
                                            <div class="recent-post-info">
                                                <div class="recent-post-title-time">
                                                    <h5><a href="{{ route('duyuru','detaylari') }}">Maridland de Villa</a></h5>
                                                    <p>July 30, 2016 / 10 am</p>
                                                </div>
                                                <p>Lorem must explain to you how all this mistaolt</p>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- blog-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <article class="recent-post-item">
                                            <div class="recent-post-image">
                                                <a href="{{ route('duyuru','detaylari') }}"><img src="/user/images/recent-post/3.jpg" alt=""></a>
                                            </div>
                                            <div class="recent-post-info">
                                                <div class="recent-post-title-time">
                                                    <h5><a href="{{ route('duyuru','detaylari') }}">Maridland de Villa</a></h5>
                                                    <p>July 30, 2016 / 10 am</p>
                                                </div>
                                                <p>Lorem must explain to you how all this mistaolt</p>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </aside>
                            <!-- widget-archive -->
                         
                            <!-- widget-add -->
                            <aside class="widget widget-add mb-0">
                                <div class="widget-add-item">
                                    <div class="widget-add-image">
                                        <a href="#"><img src="{{ asset('user/images/others/add.jpg') }}" alt=""></a>
                                        <div class="widget-add-info">
                                            <h5><a href="#"><span>Sotilife ile</span> <br> Ev Satın Al<br> Günlük Kıralık ev tut.</a></h5>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
            <!-- BLOG AREA END -->
            
            <!-- SUBSCRIBE AREA START -->
            <div class="subscribe-area bg-blue call-to-bg plr-140 ptb-50">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="section-title text-white">
                                <h3>BÜLTENİMİZE</h3>
                                <h2 class="h1">ABONE OL</h2>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-12">
                            <div class="subscribe">
                                <form action="#">
                                    <input type="text" name="subscribe" placeholder="E-posta ekle...">
                                    <button type="submit" value="send">Gönder</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- SUBSCRIBE AREA END -->
        </div>
        <!-- End page content -->

@endsection
@section('scripts')


@endsection

