@extends('user.app') 
@section('title', 'Araç Kiralama')


@section('header')

@endsection

@section('main-content')
 <!-- BREADCRUMBS AREA START -->
        <div class="breadcrumbs-area bread-bg-cars bg-opacity-black-70">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h2 class="breadcrumbs-title">@lang('index.cars_arac_kiralama')</h2>
                            <ul class="breadcrumbs-list">
                                <li><a href="/">@lang('index.cars_anasayfa')</a></li>
                                <li>@lang('index.cars_arac_kiralama')</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMBS AREA END -->

         <!-- Start page content -->
        <section id="page-content" class="page-wrapper">
            
            <!-- FEATURED FLAT AREA START -->
            <div class="featured-flat-area pt-115 pb-60">
                <div class="container">

                	<div class="row">
                        <div class="col-md-12">
                            <div class="section-title-2 text-center">
                                <h2>@lang('index.cars_rantacar') </h2>
                               <p>@lang('index.cars_aciklama') </p>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="featured-flat">
                            <div class="row">
                                <!-- flat-item -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="flat-item">
                                        <div class="flat-item-image">
                                            <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank"><img src="/user/images/cars/1.jpg" alt=""></a>
                                            <div class="flat-link">
                                                <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank">Whatsapp Kirala</a>
                                            </div>
                                            <ul class="flat-desc">
                                                <li>
                                                    <span>Audi A3 Sedan Limousine</span>
                                                </li>
                                            	</br>
                                                <li>
                                                    <span>400₺ Gün • 300₺ Hafta •	200₺ Ay</span>
                                                </li>
                                            	</br>
                                                <li>
                                                         <span class="price">400TL / Gün</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- flat-item -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="flat-item">
                                        <div class="flat-item-image">
                                            <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank"><img src="/user/images/cars/2.jpg" alt=""></a>
                                            <div class="flat-link">
                                                <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank">Whatsapp Kirala</a>
                                            </div>
                                            <ul class="flat-desc">
                                                <li>
                                                    <span>Audi A3 Sedan Limousine</span>
                                                </li>
                                            	</br>
                                                <li>
                                                    <span>400₺ Gün • 300₺ Hafta •	200₺ Ay</span>
                                                </li>
                                            	</br>
                                                <li>
                                                         <span class="price">400TL / Gün</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- flat-item -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="flat-item">
                                        <div class="flat-item-image">
                                            <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank"><img src="/user/images/cars/3.jpg" alt=""></a>
                                            <div class="flat-link">
                                                <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank">Whatsapp Kirala</a>
                                            </div>
                                            <ul class="flat-desc">
                                                <li>
                                                    <span>Audi A3 Sedan Limousine</span>
                                                </li>
                                            	</br>
                                                <li>
                                                    <span>400₺ Gün • 300₺ Hafta •	200₺ Ay</span>
                                                </li>
                                            	</br>
                                                <li>
                                                         <span class="price">400TL / Gün</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- flat-item -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="flat-item">
                                        <div class="flat-item-image">
                                            <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank"><img src="/user/images/cars/4.jpg" alt=""></a>
                                            <div class="flat-link">
                                                <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank">Whatsapp Kirala</a>
                                            </div>
                                            <ul class="flat-desc">
                                                <li>
                                                    <span>Audi A3 Sedan Limousine</span>
                                                </li>
                                            	</br>
                                                <li>
                                                    <span>400₺ Gün • 300₺ Hafta •	200₺ Ay</span>
                                                </li>
                                            	</br>
                                                <li>
                                                         <span class="price">400TL / Gün</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- flat-item -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="flat-item">
                                        <div class="flat-item-image">
                                            <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank"><img src="/user/images/cars/5.jpg" alt=""></a>
                                            <div class="flat-link">
                                                <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank">Whatsapp Kirala</a>
                                            </div>
                                            <ul class="flat-desc">
                                                <li>
                                                    <span>Audi A3 Sedan Limousine</span>
                                                </li>
                                            	</br>
                                                <li>
                                                    <span>400₺ Gün • 300₺ Hafta •	200₺ Ay</span>
                                                </li>
                                            	</br>
                                                <li>
                                                         <span class="price">400TL / Gün</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- flat-item -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="flat-item">
                                        <div class="flat-item-image">
                                            <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank"><img src="/user/images/cars/6.jpg" alt=""></a>
                                            <div class="flat-link">
                                                <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank">Whatsapp Kirala</a>
                                            </div>
                                            <ul class="flat-desc">
                                                <li>
                                                    <span>Audi A3 Sedan Limousine</span>
                                                </li>
                                            	</br>
                                                <li>
                                                    <span>400₺ Gün • 300₺ Hafta •	200₺ Ay</span>
                                                </li>
                                            	</br>
                                                <li>
                                                         <span class="price">400TL / Gün</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- flat-item -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="flat-item">
                                        <div class="flat-item-image">
                                            <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank"><img src="/user/images/cars/7.jpg" alt=""></a>
                                            <div class="flat-link">
                                                <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank">Whatsapp Kirala</a>
                                            </div>
                                            <ul class="flat-desc">
                                                <li>
                                                    <span>Audi A3 Sedan Limousine</span>
                                                </li>
                                            	</br>
                                                <li>
                                                    <span>400₺ Gün • 300₺ Hafta •	200₺ Ay</span>
                                                </li>
                                            	</br>
                                                <li>
                                                         <span class="price">400TL / Gün</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- flat-item -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="flat-item">
                                        <div class="flat-item-image">
                                            <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank"><img src="/user/images/cars/8.jpg" alt=""></a>
                                            <div class="flat-link">
                                                <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank">Whatsapp Kirala</a>
                                            </div>
                                            <ul class="flat-desc">
                                                <li>
                                                    <span>Audi A3 Sedan Limousine</span>
                                                </li>
                                            	</br>
                                                <li>
                                                    <span>400₺ Gün • 300₺ Hafta •	200₺ Ay</span>
                                                </li>
                                            	</br>
                                                <li>
                                                         <span class="price">400TL / Gün</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- flat-item -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="flat-item">
                                        <div class="flat-item-image">
                                            <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank"><img src="/user/images/cars/9.jpg" alt=""></a>
                                            <div class="flat-link">
                                                <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank">Whatsapp Kirala</a>
                                            </div>
                                            <ul class="flat-desc">
                                                <li>
                                                    <span>Audi A3 Sedan Limousine</span>
                                                </li>
                                            	</br>
                                                <li>
                                                    <span>400₺ Gün • 300₺ Hafta •	200₺ Ay</span>
                                                </li>
                                            	</br>
                                                <li>
                                                         <span class="price">400TL / Gün</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- flat-item -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="flat-item">
                                        <div class="flat-item-image">
                                            <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank"><img src="/user/images/cars/7.jpg" alt=""></a>
                                            <div class="flat-link">
                                                <a href="https://api.whatsapp.com/send?phone=905392535087" target="_blank">Whatsapp Kirala</a>
                                            </div>
                                            <ul class="flat-desc">
                                                <li>
                                                    <span>Audi A3 Sedan Limousine</span>
                                                </li>
                                            	</br>
                                                <li>
                                                    <span>400₺ Gün • 300₺ Hafta •	200₺ Ay</span>
                                                </li>
                                            	</br>
                                                <li>
                                                         <span class="price">400TL / Gün</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- pagination-area -->
                                <div class="col-xs-12">
                                    <div class="pagination-area mb-60">
                                        <ul class="pagination-list text-center">
                                            <li><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FEATURED FLAT AREA END -->
         
        </section>
        <!-- End page content -->  
@endsection
@section('scripts')

@endsection