@extends('user.app') 
@section('title', 'Rent a House')


@section('header')

@endsection

@section('main-content')

 <!-- BREADCRUMBS AREA START -->
        <div class="breadcrumbs-area bread-bg-houses bg-opacity-black-70">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h2 class="breadcrumbs-title">Rent a House</h2>
                            <ul class="breadcrumbs-list">
                                <li><a href="/">Home Page</a></li>
                                <li>Rent a House</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMBS AREA END -->

         <!-- Start page content -->
        <section id="page-content" class="page-wrapper">

            <!-- PROPERTIES DETAILS AREA START -->
            <div class="properties-details-area pt-50 pb-60">
                <div class="container">

                      @include('user.layouts.notifications')
                    <div class="row">
                        <div class="col-md-8">
                            <!-- pro-details-image -->
                            <div class="pro-details-image mb-60">
                                <div class="pro-details-big-image">
                                       @include('user/layouts/notifications') 
                                     <h5>#{{ $house->house_no }} | {{ $house->city }} | {{ $house->title }}</h5>
                                     <hr>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="pro-0">
                                            <a href="images/single-property/big/1.jpg" data-lightbox="image-1" data-title="Sheltek Properties - 1">
                                               <img src="../../.../../../../upload/{{ $house->main_image }}" alt="">
                                            </a>
                                        </div>

                                        @foreach($houseimages as $image)

                                                <div role="tabpanel" class="tab-pane fade" id="pro-{{ $loop->index + 1 }}">
                                                    <a href="/../.../../../../upload/{{ $image->name }}" data-lightbox="image-1" data-title="Sheltek Properties - 2">
                                                        <img src="/../.../../../../upload/{{ $image->name }}" alt="">
                                                    </a>
                                                </div>

                                        @endforeach

                                    </div>
                                </div>
                                <div class="pro-details-carousel">
                                    <div class="pro-details-item">
                                        <a href="#pro-0" data-toggle="tab"><img src="../../.../../../../upload/{{ $house->main_image }}" alt=""></a>
                                    </div>

                                     @foreach($houseimages as $image)

                                        <div class="pro-details-item">
                                            <a href="#pro-{{ $loop->index + 1 }}" data-toggle="tab">
                                                <img src="/../.../../../../upload/{{ $image->name }}" alt="">                
                                            </a>
                                        </div>
                                    @endforeach




                     

                                </div>                           
                            </div>
                            <!-- pro-details-short-info -->
                            <div class="pro-details-short-info mb-60">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="pro-details-condition">
                                            <h5>{{ $house->price }} USD Per Night</h5>
                                            <div class="pro-details-condition-inner bg-gray">
                                                <ul class="condition-list">
                                                    <li><img src="/user/images/icons/5.png" alt="">{{ $house->dimension }} m2</li>
                                                    <li><img src="/user/images/icons/6.png" alt="">Bedroom {{ $house->room_number }}</li>
                                                    <li><img src="/user/images/icons/7.png" alt="">Bathroom {{ $house->bathrom_number }}</li>
                                                    <li><img src="/user/images/icons/13.png" alt="">Is there a balcon? {{ $house->balcon }}</li>
                                                    <li><img src="/user/images/icons/14.png" alt="">Hitting System: {{ $house->hit_system }}</li>
                                                    
                                                </ul>
                                                <p><img src="/user/images/icons/location.png" alt="">{{ $house->address }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="pro-details-amenities">
                                            <h5>Amenities</h5>
                                            <div class="pro-details-amenities-inner bg-gray">
                                                <ul class="amenities-list">

                                                     @foreach($house_properties as $property)
                                                         <li>{{ App\Model\Admin\Property::find($property->property_id)->title }}</li>
                                                      @endforeach
                                                   
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- pro-details-description -->
                            <div class="pro-details-description mb-50">  {!! $house->description !!}</p>
                            </div>
                            <!-- pro-details-feedback -->


                            <div class="map">
                                <iframe width="100%" height="380" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?q={{ $house->address }}&output=embed"></iframe>
                            </div>


                          
                            <!-- agent-review -->
                            <div class="pro-details-agent-review">
                                <div class="row">
                                    
                                   
                                        
                                        <div class="col-md-9 col-sm-6 col-xs-12">
                                            <div class="agent-details-image" style="margin-bottom: -10px;">
                                                <img src="/user/images/others/details.png" alt="">
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="agent-details-contact">
                                                <h5>Make A reservation</h5>
                                                {{-- <h6> Telefon</h6>
                                                <p><img src="/user/images/icons/phone-2.png" alt=""> +1245  785  659 </p> --}}
                                                <h6>Email</h6>
                                                <p><img src="/user/images/icons/mail-close.png" alt=""> info@sotylife.com</p>
                                               {{--  <h6>Skype</h6>
                                                <p><img src="/user/images/icons/15.png" alt=""> skype id : </p> --}}
                                                <!-- social-media -->
                                                <ul class="social-media">
                                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>

                                          <div class="col-md-8 col-sm-8col-xs-12">
                                                <div class="contact-messge reservation-bg">
                                                    <!-- blog-details-reply -->
                                                    <div class="leave-review">
                                                        <h5>House: #{{ $house->house_no }} Reservation</h5>
                                                        <form  action="{{ route('user.house.reservation') }}" method="post">

                                                            @csrf

                                                            <input type="text" name="house_id" value="{{ $house->id }}" style="display: none">
                                                            <input type="text" name="name" placeholder="Name and Surname">
                                                            <input type="email" name="email" placeholder="Email Address">
                                                            <input type="text" name="phone" placeholder="Phone Number">
                                                            
                                                             <div class="input-group date" data-provide="datepicker" style="margin-bottom: 20px;">
                                                                <input type="text" class="form-control" name="entry" placeholder="Entry Date" style="margin-bottom: 0">
                                                                <div class="input-group-addon">
                                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                </div>
                                                            </div>
                                                             <div class="input-group date" data-provide="datepicker" style="margin-bottom: 20px;">
                                                                <input type="text" class="form-control" name="out" placeholder="Exit Date" style="margin-bottom: 0">
                                                                <div class="input-group-addon">
                                                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                                </div>
                                                            </div>


                                                            <button type="submit" class="submit-btn-1">Reserve</button>
                                                        </form>
                                                        <p class="form-messege mb-0"></p>
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- widget-search-property -->
                           <aside class="widget widget-search-property mb-30">
                                <h5>Search</h5>
                               

                                    <form action="{{ route('house.search') }}" method="get" accept-charset="utf-8">
                                  @csrf

                                      <div class="row">
                              
                                        <div class="col-xs-12">
                                            <div class="find-home-item custom-select">                  
                                                <select class="selectpicker" title="Which City are you planing to visit?" data-hide-disabled="true" name="city">
                                                    <optgroup label="Select a city?">
                                                      
                                                        <option value="Avtonomna Respublika Krym">Avtonomna Respublika Krym</option>    
                                                        <option value="Cherkaska">Cherkaska</option> 
                                                        <option value="Chernihivska">Chernihivska</option>  
                                                        <option value="Chernivetska">Chernivetska</option>  
                                                        <option value="Dnipropetrovska">Dnipropetrovska</option>   
                                                        <option value="Donetska">Donetska</option>  
                                                        <option value="Ivano-Frankivska">Ivano-Frankivska</option>  
                                                        <option value="Kharkivska">Kharkivska</option>    
                                                        <option value="Khersonska">Khersonska</option>    
                                                        <option value="Khmelnytska">Khmelnytska</option>   
                                                        <option value="Kirovohradska">Kirovohradska</option> 
                                                        <option value="Kyiv">Kyiv</option> 
                                                        <option value="Kyivska">Kyivska</option>   
                                                        <option value="Luhanska">Luhanska</option>  
                                                        <option value="Lvivska">Lvivska</option>   
                                                        <option value="Mykolaivska">Mykolaivska</option>   
                                                        <option value="Odeska">Odeska</option>    
                                                        <option value="Poltavska">Poltavska</option> 
                                                        <option value="Rivnenska">Rivnenska</option> 
                                                        <option value="Sevastopol">Sevastopol</option>   
                                                        <option value="Sumska">Sumska</option>    
                                                        <option value="Ternopilska">Ternopilska</option>   
                                                        <option value="Vinnytska">Vinnytska</option> 
                                                        <option value="Volynska">Volynska</option>  
                                                        <option value="Zakarpatska">Zakarpatska</option>   
                                                        <option value="Zaporizka">Zaporizka</option> 
                                                        <option value="Zhytomyrska">Zhytomyrska</option>   

                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>


                                         <div class="col-xs-12">
                                             <div class="find-home-item">
                                            <!-- shop-filter -->
                                                <div class="shop-filter">
                                                    <div class="price_filter">
                                                        <div class="price_slider_amount">
                                                            <input type="submit"  value="Price Interval :"/> 
                                                            <input type="text" id="amount" name="price"  placeholder="Add Your Price" /> 
                                                        </div>
                                                        <div id="slider-range"></div>
                                                    </div>
                                                </div>
                                            </div>

                                         </div>
                                        

                                        <div class="col-xs-12">
                                            <div class="find-home-item mb-0-xs">
                                                 <button type="submit" class="button-1 btn-block btn-hover-1">@lang('index.index_ara')</button>
                                               
                                            </div>
                                        </div>
                                    </div>

                                </form>


                                  
                              
                                
                            </aside>
                            <!-- widget-featured-property -->
                          {{--   <aside class="widget widget-featured-property">
                                <h5>Featured Property</h5>
                                <div class="row">
                                    <!-- flat-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <div class="flat-item">
                                            <div class="flat-item-image">
                                                <span class="for-sale">For Sale</span>
                                                <a href="#"><img src="/user/images/flat/1.jpg" alt=""></a>
                                                <div class="flat-link">
                                                    <a href="#">More Details</a>
                                                </div>
                                                <ul class="flat-desc">
                                                    <li>
                                                        <img src="/user/images/icons/4.png" alt="">
                                                        <span>450 sqft</span>
                                                    </li>
                                                    <li>
                                                        <img src="/user/images/icons/5.png" alt="">
                                                        <span>5</span>
                                                    </li>
                                                    <li>
                                                        <img src="/user/images/icons/6.png" alt="">
                                                        <span>3</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="flat-item-info">
                                                <div class="flat-title-price">
                                                    <h5><a href="#">Masons de Villa </a></h5>
                                                    <span class="price">$52,350</span>
                                                </div>
                                                <p><img src="/user/images/icons/location.png" alt="">568 E 1st Ave, Ney Jersey</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- flat-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <div class="flat-item">
                                            <div class="flat-item-image">
                                                <span class="for-sale">For Sale</span>
                                                <a href="#"><img src="/user/images/flat/2.jpg" alt=""></a>
                                                <div class="flat-link">
                                                    <a href="#">More Details</a>
                                                </div>
                                                <ul class="flat-desc">
                                                    <li>
                                                        <img src="/user/images/icons/4.png" alt="">
                                                        <span>450 sqft</span>
                                                    </li>
                                                    <li>
                                                        <img src="/user/images/icons/5.png" alt="">
                                                        <span>5</span>
                                                    </li>
                                                    <li>
                                                        <img src="/user/images/icons/6.png" alt="">
                                                        <span>3</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="flat-item-info">
                                                <div class="flat-title-price">
                                                    <h5><a href="#">Masons de Villa </a></h5>
                                                    <span class="price">$52,350</span>
                                                </div>
                                                <p><img src="/user/images/icons/location.png" alt="">568 E 1st Ave, Ney Jersey</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- flat-item -->
                                    <div class="col-md-12 hidden-sm col-xs-12">
                                        <div class="flat-item">
                                            <div class="flat-item-image">
                                                <span class="for-sale">For Sale</span>
                                                <a href="#"><img src="/user/images/flat/3.jpg" alt=""></a>
                                                <div class="flat-link">
                                                    <a href="#">More Details</a>
                                                </div>
                                                <ul class="flat-desc">
                                                    <li>
                                                        <img src="/user/images/icons/4.png" alt="">
                                                        <span>450 sqft</span>
                                                    </li>
                                                    <li>
                                                        <img src="/user/images/icons/5.png" alt="">
                                                        <span>5</span>
                                                    </li>
                                                    <li>
                                                        <img src="/user/images/icons/6.png" alt="">
                                                        <span>3</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="flat-item-info">
                                                <div class="flat-title-price">
                                                    <h5><a href="#">Masons de Villa </a></h5>
                                                    <span class="price">$52,350</span>
                                                </div>
                                                <p><img src="/user/images/icons/location.png" alt="">568 E 1st Ave, Ney Jersey</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside> --}}
                            <!-- widget-video -->
                            <aside class="widget widget-video">
                                <h5>Take A Look</h5>
                                <div class="properties-video">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <!-- <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/40934652"></iframe> -->
                                        <iframe src="https://player.vimeo.com/video/117765418?title=0&byline=0&portrait=0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
            <!-- PROPERTIES DETAILS AREA END -->
            
           
        </section>
        <!-- End page content -->


@endsection
@section('scripts')

   
@endsection

