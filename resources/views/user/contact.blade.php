@extends('user.app') 
@section('title', 'İliteşim')


@section('header')

@endsection

@section('main-content')

	  <!-- BREADCRUMBS AREA START -->
        <div class="breadcrumbs-area bread-bg-1 bg-opacity-black-70">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h2 class="breadcrumbs-title">@lang('index.contact_iletisim')</h2>
                            <ul class="breadcrumbs-list">
                                <li><a href="/">@lang('index.contact_anasayfa')</a></li>
                                <li>@lang('index.contact_iletisim')</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMBS AREA END -->

        <!-- Start page content -->
        <section id="page-content" class="page-wrapper">

            <!-- CONTACT AREA START -->
            <div class="contact-area pt-115 pb-115">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-5 col-xs-12">
                            <!-- get-in-toch -->
                            @include('user.layouts.notifications')
                            <div class="get-in-toch">
                                <div class="section-title mb-30">
                                    <h3>GET IN</h3>
                                    <h2>@lang('index.contact_iletisim_bilgileri')</h2>
                                </div>
                                <div class="contact-desc mb-50">
                                    <p><span data-placement="top" data-toggle="tooltip" data-original-title="The name you can trust" class="tooltip-content">@lang('index.contact_aciklama')</span></p>
                                </div>
                                <ul class="contact-address">
                                    {{-- <li>
                                        <div class="contact-address-icon">
                                            <img src="images/icons/location-2.png" alt="">
                                        </div>
                                        <div class="contact-address-info">
                                            <span>@lang('index.adres1') </span>
                                            <span>@lang('index.adres2')</span>
                                        </div>
                                    </li> --}}
                                   {{--  <li>
                                        <div class="contact-address-icon">
                                            <img src="images/icons/phone-3.png" alt="">
                                        </div>
                                        <div class="contact-address-info">
                                            <span>@lang('index.telefon1')</span>
                                            <span>@lang('index.telefon2')</span>
                                        </div>
                                    </li> --}}
                                    <li>
                                        <div class="contact-address-icon">
                                            <img src="images/icons/world.png" alt="">
                                        </div>
                                        <div class="contact-address-info">
                                            <span>@lang('index.email')</span>
                                            <span>Web :<a href="#" target="_blank"> www.sotylife.com</a></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-7 col-xs-12">
                            <div class="contact-messge contact-bg">
                                <!-- blog-details-reply -->
                                <div class="leave-review">
                                    <h5>@lang('index.mesaj_yaz')</h5>
                                    
                                    <form class="form-horizontal" action="{{ route('contact.mail') }}" method="POST">
                                        
                                        @csrf

                                        <input type="text" name="name" placeholder="Name and Surname">
                                        <input type="email" name="email" placeholder="Email">
                                        <textarea name="message" placeholder="Mesage"></textarea>
                                        <button type="submit" class="submit-btn-1">Send</button>
                                    </form>
                                    <p class="form-messege mb-0"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- CONTACT AREA END -->

            <!-- GOOGLE MAP AREA START -->
            <div class="google-map-area">
                <div id="googleMap"></div>
            </div>
            <!-- GOOGLE MAP AREA END -->

        </section>
        <!-- End page content -->

@endsection
@section('scripts')

	  <!-- Google Map js -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCeeHDCOXmUMja1CFg96RbtyKgx381yoBU"></script>
    <script src="{{ asset('user/js/google-map.js') }}"></script>

@endsection

