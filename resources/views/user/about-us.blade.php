@extends('user.app') 
@section('title', 'Hakkımızda')


@section('header')

@endsection

@section('main-content')

	 <!-- BREADCRUMBS AREA START -->
        <div class="breadcrumbs-area bread-bg-about bg-opacity-black-70">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h2 class="breadcrumbs-title">@lang('index.about_us_hakkimizda')</h2>
                            <ul class="breadcrumbs-list">
                                <li><a href="/">@lang('index.about_us_anasayfa')</a></li>
                                <li>@lang('index.about_us_hakkimizda')</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMBS AREA END -->

        <!-- Start page content -->
        <section id="page-content" class="page-wrapper">
            
            <!-- ABOUT SHELTEK AREA START -->
            <div class="about-sheltek-area pt-115">
                <div class="container">

                      @include('user.layouts.notifications')
                      
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="section-title mb-30">
                                <h3>@lang('index.about_us_isim') </h3>
                                <h2>@lang('index.about_us_hakkimizda')</h2>
                            </div>
                            <div class="about-sheltek-info about-us">
                                    <p><span data-placement="top" data-toggle="tooltip" data-original-title="The name you can trust" class="tooltip-content">@lang('index.about_us_isimllc')</span>@lang('index.about_us_aciklama1')
                                    </p>
                                      <div class="author-quote">
                                        <p>@lang('index.about_us_aciklama2')
                                         </p>
                                      
                                    </div>
                                      
                                       <div class="author-quote">
                                        <p> @lang('index.about_us_aciklama3')</p>
                                       </div>

                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="about-image">
                                <img src="/user/images/about/5.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title-2 text-center">
                                <h2>@lang('index.about_us_tanitim_izle')</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="properties-video">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe src="https://player.vimeo.com/video/117765418?title=0&amp;byline=0&amp;portrait=0" allowfullscreen=""></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- ABOUT SHELTEK AREA END -->
            
            <!-- SERVICES AREA START -->
            <div class="services-area ptb-30">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                        
                        </div>
                    </div>
                </div>
            </div>
            <!-- SERVICES AREA END -->
            
  
        </section>
        <!-- End page content -->

@endsection
@section('scripts')

@endsection
