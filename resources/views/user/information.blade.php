@extends('user.app') 
@section('title', 'Duyurularımız')


@section('header')

@endsection

@section('main-content')


        <!-- BREADCRUMBS AREA START -->
        <div class="breadcrumbs-area bread-bg-information bg-opacity-black-70">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h2 class="breadcrumbs-title">@lang('index.information_guncel_haberler')</h2>
                            <ul class="breadcrumbs-list">
                                <li><a href="/">@lang('index.information_anasayfa')</a></li>
                                <li>@lang('index.information_güncel_haber_detay')</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMBS AREA END -->

        <!-- Start page content -->
        <div id="page-content" class="page-wrapper">

               <!-- BLOG AREA START -->
            <div class="blog-area pt-115 pb-120">
                <div class="container">

                      @include('user.layouts.notifications')
                    <div class="row">
                        <div class="col-md-8 col-xs-12">
                            <div class="blog-details-area">
                                <!-- blog-details-image -->
                                <div class="blog-details-image">
                                    <img src="/user/images/blog/blog-details.jpg" alt="">
                                </div>
                                <!-- blog-details-title-time -->
                                <div class="blog-details-title-time">
                                    <h5>@lang('index.information_ev_tasarım')</h5>
                                    <p>@lang('index.information_tarih_saat')</p>
                                </div>
                                <!-- blog-details-desctiption -->
                                <div class="blog-details-desctiption mb-60">
                                    <p>New Building Design ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquipx ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat lla pariatur dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</p>
                                    <p>Sheltek is the Best  should be the consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore etlore magna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacm</p>
                                 
                                    <p>There are some design tends  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor inc     dunt ut labore et dolore magna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore.</p>

                                    <p>New Building Design ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquipx ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat lla pariatur dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</p>
                                    <p>Sheltek is the Best  should be the consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore etlore magna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacm</p>
                                 
                                    <p>There are some design tends  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor inc     dunt ut labore et dolore magna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore.</p>

                                    <p>New Building Design ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquipx ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat lla pariatur dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</p>
                                    <p>Sheltek is the Best  should be the consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore etlore magna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacm</p>
                                 
                                    <p>There are some design tends  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor inc     dunt ut labore et dolore magna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore.</p>
                                </div>
                                <!-- blog-details-share-tags -->
                                <div class="blog-details-share-tags clearfix mb-75">
                                    <div class="blog-details-share f-right">
                                        <ul class="social-media">
                                            <li>Share:</li>
                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div> 
                         
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <!-- widget-search -->
                            <aside class="widget widget-search mb-30">
                                <form action="#">
                                    <input type="text" name="search" placeholder="Search...">
                                    <button type="button" class=""><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </aside>
                            <!-- widget-categories -->
                            <aside class="widget widget-categories mb-50">
                               <h5>Duyuru Kategoriler</h5>
                                <ul class="widget-categories-list">
                                    <li><a href="#">Apartment <span>1420</span></a></li>
                                    <li><a href="#">Apartment Building <span>780</span></a></li>
                                    <li><a href="#">Bungalow <span>670</span></a></li>
                                    <li><a href="#">Corporate House <span>520</span></a></li>
                                    <li><a href="#">Duplex Villa <span>350</span></a></li>
                                </ul>
                            </aside>
                            <!-- widget-recent-post -->
                           
                              <aside class="widget widget-recent-post mb-50">
                                <h5>Son Eklenenler</h5>
                                <div class="row">
                                    <!-- blog-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <article class="recent-post-item">
                                            <div class="recent-post-image">
                                                <a href="#"><img src="/user/images/recent-post/1.jpg" alt=""></a>
                                            </div>
                                            <div class="recent-post-info">
                                                <div class="recent-post-title-time">
                                                    <h5><a href="#">Maridland de Villa</a></h5>
                                                    <p>July 30, 2016 / 10 am</p>
                                                </div>
                                                <p>Lorem must explain to you how all this mistaolt</p>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- blog-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <article class="recent-post-item">
                                            <div class="recent-post-image">
                                                <a href="#"><img src="/user/images/recent-post/2.jpg" alt=""></a>
                                            </div>
                                            <div class="recent-post-info">
                                                <div class="recent-post-title-time">
                                                    <h5><a href="#">Maridland de Villa</a></h5>
                                                    <p>July 30, 2016 / 10 am</p>
                                                </div>
                                                <p>Lorem must explain to you how all this mistaolt</p>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- blog-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <article class="recent-post-item">
                                            <div class="recent-post-image">
                                                <a href="#"><img src="/user/images/recent-post/3.jpg" alt=""></a>
                                            </div>
                                            <div class="recent-post-info">
                                                <div class="recent-post-title-time">
                                                    <h5><a href="#">Maridland de Villa</a></h5>
                                                    <p>July 30, 2016 / 10 am</p>
                                                </div>
                                                <p>Lorem must explain to you how all this mistaolt</p>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- blog-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <article class="recent-post-item">
                                            <div class="recent-post-image">
                                                <a href="#"><img src="/user/images/recent-post/3.jpg" alt=""></a>
                                            </div>
                                            <div class="recent-post-info">
                                                <div class="recent-post-title-time">
                                                    <h5><a href="#">Maridland de Villa</a></h5>
                                                    <p>July 30, 2016 / 10 am</p>
                                                </div>
                                                <p>Lorem must explain to you how all this mistaolt</p>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </aside>
                         
                            
                                 <!-- widget-add -->
                            <aside class="widget widget-add mb-0">
                                <div class="widget-add-item">
                                    <div class="widget-add-image">
                                        <a href="#"><img src="{{ asset('user/images/others/add.jpg') }}" alt=""></a>
                                        <div class="widget-add-info">
                                            <h5><a href="#"><span>Sotilife ile</span> <br> Ev Satın Al<br> Günlük Kıralık ev tut.</a></h5>
                                        </div>
                                    </div>
                                </div>
                            </aside>

                        </div>
                    </div>
                </div>
            </div>
            <!-- BLOG AREA END -->
          
            <!-- SUBSCRIBE AREA START -->
            <div class="subscribe-area bg-blue call-to-bg plr-140 ptb-50">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="section-title text-white">
                                <h3>BÜLTENİMİZE</h3>
                                <h2 class="h1">ABONE OL</h2>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-12">
                            <div class="subscribe">
                                <form action="#">
                                    <input type="text" name="subscribe" placeholder="E-posta ekle...">
                                    <button type="submit" value="send">Gönder</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- SUBSCRIBE AREA END -->
        </div>
        <!-- End page content -->

@endsection
@section('scripts')


@endsection

