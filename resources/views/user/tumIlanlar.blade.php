@extends('user.app') 
@section('title', 'Tüm İlanlar')


@section('header')

@endsection

@section('main-content')

 <!-- BREADCRUMBS AREA START -->
        <div class="breadcrumbs-area bread-bg-houses bg-opacity-black-70">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h2 class="breadcrumbs-title">Rent a House</h2>
                            <ul class="breadcrumbs-list">
                                <li><a href="/">Home Page</a></li>
                                <li>Daily House Rental</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMBS AREA END -->

        <!-- Start page content -->
        <section id="page-content" class="page-wrapper">
            
            <!-- FEATURED FLAT AREA START -->
            <div class="featured-flat-area pt-115 pb-60">
                <div class="container">
                      @include('user.layouts.notifications')
                      
                    <div class="row">
                        <div class="col-md-8">
                            <div class="featured-flat">
                                <div class="row">




                                    @foreach ($houses as $house)
                                         <!-- flat-item -->
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="flat-item">
                                                    <div class="flat-item-image">
                                                        <span class="for-sale">{{ $house->price }} USD / Per day</span>
                                                        <a href="{{ route('ilanDetaylari',$house->id) }}"><img src="../../.../../../../upload/{{ $house->main_image }}" alt=""></a>
                                                        <div class="flat-link">
                                                            <a href="{{ route('ilanDetaylari',$house->id) }}">Details</a>
                                                        </div>
                                                        <ul class="flat-desc">
                                                            <li>
                                                                <img src="/user/images/icons/4.png" alt="">
                                                                <span>{{ $house->dimension }} m2</span>
                                                            </li>
                                                            <li>
                                                                <img src="/user/images/icons/5.png" alt="">
                                                                <span>{{ $house->room_number }}</span>
                                                            </li>
                                                            <li>
                                                                <img src="/user/images/icons/6.png" alt="">
                                                                <span>{{ $house->bathrom_number }}</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="flat-item-info">
                                                        <div class="flat-title-price">
                                                            <h5><a href="{{ route('ilanDetaylari',$house->id) }}">{{ $house->title }}</a></h5>
                                                        </div>
                                                        <p><img src="/user/images/icons/location.png" alt="">{{ $house->address }} | {{ $house->city }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach

                                  
                                    <!-- flat-item -->
                           
                                    <!-- pagination-area -->
                                    <div class="col-xs-12">
                                        <div class="pagination-area mb-60">
                                            <ul class="pagination-list text-center">
                                               {{ $houses->links() }}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- widget-search-property -->
                            <aside class="widget widget-search-property mb-30">
                                <h5>Search</h5>
                               


                                    <form action="{{ route('house.search') }}" method="get" accept-charset="utf-8">
                                  @csrf

                                      <div class="row">
                              
                                        <div class="col-xs-12">
                                            <div class="find-home-item custom-select">                  
                                                <select class="selectpicker" title="Which City are you planing to visit?" data-hide-disabled="true" name="city">
                                                    <optgroup label="Select a city?">
                                                      
                                                        <option value="Avtonomna Respublika Krym">Avtonomna Respublika Krym</option>    
                                                        <option value="Cherkaska">Cherkaska</option> 
                                                        <option value="Chernihivska">Chernihivska</option>  
                                                        <option value="Chernivetska">Chernivetska</option>  
                                                        <option value="Dnipropetrovska">Dnipropetrovska</option>   
                                                        <option value="Donetska">Donetska</option>  
                                                        <option value="Ivano-Frankivska">Ivano-Frankivska</option>  
                                                        <option value="Kharkivska">Kharkivska</option>    
                                                        <option value="Khersonska">Khersonska</option>    
                                                        <option value="Khmelnytska">Khmelnytska</option>   
                                                        <option value="Kirovohradska">Kirovohradska</option> 
                                                        <option value="Kyiv">Kyiv</option> 
                                                        <option value="Kyivska">Kyivska</option>   
                                                        <option value="Luhanska">Luhanska</option>  
                                                        <option value="Lvivska">Lvivska</option>   
                                                        <option value="Mykolaivska">Mykolaivska</option>   
                                                        <option value="Odeska">Odeska</option>    
                                                        <option value="Poltavska">Poltavska</option> 
                                                        <option value="Rivnenska">Rivnenska</option> 
                                                        <option value="Sevastopol">Sevastopol</option>   
                                                        <option value="Sumska">Sumska</option>    
                                                        <option value="Ternopilska">Ternopilska</option>   
                                                        <option value="Vinnytska">Vinnytska</option> 
                                                        <option value="Volynska">Volynska</option>  
                                                        <option value="Zakarpatska">Zakarpatska</option>   
                                                        <option value="Zaporizka">Zaporizka</option> 
                                                        <option value="Zhytomyrska">Zhytomyrska</option>   

                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>


                                         <div class="col-xs-12">
                                             <div class="find-home-item">
                                            <!-- shop-filter -->
                                                <div class="shop-filter">
                                                    <div class="price_filter">
                                                        <div class="price_slider_amount">
                                                            <input type="submit"  value="Price Interval :"/> 
                                                            <input type="text" id="amount" name="price"  placeholder="Add Your Price" /> 
                                                        </div>
                                                        <div id="slider-range"></div>
                                                    </div>
                                                </div>
                                            </div>

                                         </div>
                                        

                                        <div class="col-xs-12">
                                            <div class="find-home-item mb-0-xs">
                                                 <button type="submit" class="button-1 btn-block btn-hover-1">@lang('index.index_ara')</button>
                                               
                                            </div>
                                        </div>
                                    </div>

                                </form>



                            </aside>
                            <!-- widget-featured-property -->
                           {{--  <aside class="widget widget-featured-property mb-30">
                                <h5>Featured Property</h5>
                                <div class="row">
                                    <!-- flat-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <div class="flat-item">
                                            <div class="flat-item-image">
                                                <span class="for-sale">SATILIK</span>
                                                <a href="#"><img src="/user/images/flat/1.jpg" alt=""></a>
                                                <div class="flat-link">
                                                    <a href="#">İncele</a>
                                                </div>
                                                <ul class="flat-desc">
                                                    <li>
                                                        <img src="/user/images/icons/4.png" alt="">
                                                        <span>450 sqft</span>
                                                    </li>
                                                    <li>
                                                        <img src="/user/images/icons/5.png" alt="">
                                                        <span>5</span>
                                                    </li>
                                                    <li>
                                                        <img src="/user/images/icons/6.png" alt="">
                                                        <span>3</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="flat-item-info">
                                                <div class="flat-title-price">
                                                    <h5><a href="#">Masons de Villa </a></h5>
                                                    <span class="price">$52,350</span>
                                                </div>
                                                <p><img src="/user/images/icons/location.png" alt="">568 E 1st Ave, Ney Jersey</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- flat-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <div class="flat-item">
                                            <div class="flat-item-image">
                                                <span class="for-sale">SATILIK</span>
                                                <a href="#"><img src="/user/images/flat/2.jpg" alt=""></a>
                                                <div class="flat-link">
                                                    <a href="#">İncele</a>
                                                </div>
                                                <ul class="flat-desc">
                                                    <li>
                                                        <img src="/user/images/icons/4.png" alt="">
                                                        <span>450 sqft</span>
                                                    </li>
                                                    <li>
                                                        <img src="/user/images/icons/5.png" alt="">
                                                        <span>5</span>
                                                    </li>
                                                    <li>
                                                        <img src="/user/images/icons/6.png" alt="">
                                                        <span>3</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="flat-item-info">
                                                <div class="flat-title-price">
                                                    <h5><a href="#">Masons de Villa </a></h5>
                                                    <span class="price">$52,350</span>
                                                </div>
                                                <p><img src="/user/images/icons/location.png" alt="">568 E 1st Ave, Ney Jersey</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- flat-item -->
                                    <div class="col-md-12 hidden-sm col-xs-12">
                                        <div class="flat-item">
                                            <div class="flat-item-image">
                                                <span class="for-sale">SATILIK</span>
                                                <a href="#"><img src="/user/images/flat/3.jpg" alt=""></a>
                                                <div class="flat-link">
                                                    <a href="#">İncele</a>
                                                </div>
                                                <ul class="flat-desc">
                                                    <li>
                                                        <img src="/user/images/icons/4.png" alt="">
                                                        <span>450 sqft</span>
                                                    </li>
                                                    <li>
                                                        <img src="/user/images/icons/5.png" alt="">
                                                        <span>5</span>
                                                    </li>
                                                    <li>
                                                        <img src="/user/images/icons/6.png" alt="">
                                                        <span>3</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="flat-item-info">
                                                <div class="flat-title-price">
                                                    <h5><a href="#">Masons de Villa </a></h5>
                                                    <span class="price">$52,350</span>
                                                </div>
                                                <p><img src="/user/images/icons/location.png" alt="">568 E 1st Ave, Ney Jersey</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside> --}}
                            <!-- widget-video -->
                            <aside class="widget widget-video mb-0">
                                <h5>Take A Look</h5>
                                <div class="properties-video">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <!-- <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/40934652"></iframe> -->
                                        <iframe src="https://player.vimeo.com/video/117765418?title=0&byline=0&portrait=0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FEATURED FLAT AREA END -->
            
          
        </section>
        <!-- End page content -->

@endsection
@section('scripts')


@endsection

