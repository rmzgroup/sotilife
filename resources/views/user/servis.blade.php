@extends('user.app') 
@section('title', 'Servislerimiz')


@section('header')

@endsection

@section('main-content')




        <!-- BREADCRUMBS AREA START -->
        <div class="breadcrumbs-area bread-bg-services bg-opacity-black-70">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h2 class="breadcrumbs-title">@lang('index.information_servislerimiz')</h2>
                            <ul class="breadcrumbs-list">
                                <li><a href="/">@lang('index.information_anasayfa')</a></li>
                                <li>@lang('index.information_servislerimiz')</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMBS AREA END -->

        <!-- Start page content -->
        <div id="page-content" class="page-wrapper">

			<div class="elements-area ptb-55">
                <div class="container">

                      @include('user.layouts.notifications')
                      
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="panel-group" id="accordion_1">
                                <div class="panel panel-default">
                                    <div class="panel-heading" id="headingOne">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="true" class="">
                                      		  @lang('index.index_emlak_danismanligi')
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne_1" class="panel-collapse collapse in" aria-expanded="true" style="">
                                        <div class="panel-body">

                                        	<div class="col-md-4">
                                        		 <div class="service-item-image">
			                                        <a href="service-details.html"><img src="{{ asset('user/images/service/1.jpg') }}" alt=""></a>
			                                    </div>
                                        	</div>
                                        	<div class="col-md-8">
                                        		   <p> @lang('index.information_aciklama1')</p>
													<p> @lang('index.information_aciklama2')</p>
                                        	</div>	
                                 		 

                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_1" href="#collapseTwo_1" aria-expanded="false">
                                          		@lang('index.index_reh_tic_danismanligi')
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo_1" class="panel-collapse collapse" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                        	<div class="col-md-8">
                                        		   <p> 
                                        		  @lang('index.information_reh_tic_danismanligi_aciklama1')</p>
													<p>@lang('index.information_reh_tic_danismanligi_aciklama2')</p>
                                        	</div>	
                                        	<div class="col-md-4">
                                        		 <div class="service-item-image">
			                                        <a href="javascript:void(0)"><img src="{{ asset('user/images/service/2.jpg') }}" alt=""></a>
			                                    </div>
                                        	</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading" id="headingSeven">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_1" href="#collapseSeven_1" aria-expanded="false">
                                                 Education Consultancy
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseSeven_1" class="panel-collapse collapse" aria-labelledby="headingSeven" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <div class="col-md-4">
                                                 <div class="service-item-image">
                                                    <a href="javascript:void(0)"><img src="{{ asset('user/images/service/3.jpg') }}" alt=""></a>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <p>Ukrainian universities implement the most appreciated education systems in the world, and are given the right to teach by the Bologna agreement governing it. Each of the universities is a member of the European Universities Association and the studies are followed by the auditing department of the association. In addition, when the curriculum vitae of academicians and teaching staff in Ukrainian universities are examined, it is seen that each one has accomplished successful works in his field and he fulfills his duties with being conscious of his responsibility.</p>
                                               
                                            </div>  
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_1" href="#collapseThree_1" aria-expanded="false">
                                                 @lang('index.navbar_kripto_forex')
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree_1" class="panel-collapse collapse" aria-labelledby="headingThree" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                        	<div class="col-md-4">
                                        		 <div class="service-item-image">
			                                        <a href="javascript:void(0)"><img src="{{ asset('user/images/service/3.jpg') }}" alt=""></a>
			                                    </div>
                                        	</div>
                                        	<div class="col-md-8">
                                        		<p>@lang('index.information_kripto_forex_acıklama1')</p>
                                        		<p>@lang('index.information_kripto_forex_acıklama2')</p>
                                        		<p>@lang('index.information_kripto_forex_acıklama3')</p>
                                        	</div>	
                                        </div>
                                    </div>
                                </div>

                                 <div class="panel panel-default">
                                    <div class="panel-heading" id="headingFour">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_1" href="#collapseFour_1" aria-expanded="false">
                                               @lang('index.information_rentecar') 
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour_1" class="panel-collapse collapse" aria-labelledby="headingFour" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                        	<div class="col-md-8">
                                        	<p>@lang('index.information_rentecar_aciklama1')</p>
                                        	<p>@lang('index.information_rentecar_aciklama1')</p>
                                        	</div>	
                                        	<div class="col-md-4">
                                        		 <div class="service-item-image">
			                                        <a href="javascript:void(0)"><img src="{{ asset('user/images/service/6.jpg') }}" alt=""></a>
			                                    </div>
                                        	</div>
                                        </div>
                                    </div>
                                </div>

                                 <div class="panel panel-default">
                                    <div class="panel-heading" id="headingFive">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_1" href="#collapseFive_1" aria-expanded="false">
                                              @lang('index.information_hacaalani')
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFive_1" class="panel-collapse collapse" aria-labelledby="headingFive" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                        	<div class="col-md-8">
                                        	<p>@lang('index.information_hacaalani_aciklama1')</p>
                                        	<p>@lang('index.information_hacaalani_aciklama2')</p>
                                        	</div>	
                                        	<div class="col-md-4">
                                        		 <div class="service-item-image">
			                                        <a href="javascript:void(0)"><img src="{{ asset('user/images/service/5.jpg') }}" alt=""></a>
			                                    </div>
                                        	</div>
                                        </div>
                                    </div>
                                </div>


                            </div>                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
@endsection
@section('scripts')


@endsection

