
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');

 window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('coin', require('./components/coin.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 new Vue({
 	el: '#casheDeposit',

 	data () {
 		return {
 			info: null,
 			amount_usd: '',
 			items: [
 			{id:25, name: '25 USD'},
 			{id:50, name: '50 USD'},
 			{id:75, name: '75 USD'},
 			{id:100, name: '100 USD'},
 			{id:125, name: '125 USD'},
 			{id:150, name: '150 USD'},
 			],
 		}
 	},

 	filters: {
 		currencydecimal (value) {
 			return value.toFixed(2)
 		}
 	},


 	methods:{


 		CoinConvert:function(){

 			axios.get('/member/my-investments/convert/15', this.amount_usd.id)
		 			 headers: {
					   Authorization: 'Bearer ' + token //the token is a variable which holds the token
					 }
			 		.then(response => {
			 			this.info = response.data
			 			console.log(response.data);
			 		}).catch(error => {
			 			console.log(error.response);
			 		})},


 	},



 	mounted () {
 		this.CoinConvert()
 		
 	}
 })

