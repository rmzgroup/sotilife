<?php

return [
    //footer.blade.php
    'iletisim' => 'Contact',
    'adres1' => 'Kiev',
    'adres2' => 'Ukraine',

    'email' => 'Email : info@sotylife.com',

    'bilgi' => 'SOTYLIFE LLC manages investments that makes a long term businesses in various business opportunities. We are committed to providing well maintained homes with competitive prices and exceptional customer service, with our long-standing reputation and experience in the real estate market.',

    'gönder' => 'Send',
    'iletisime_gec' => 'Contact Us Now',
    'iletisime_yazı' => 'Contact Us for Your Questions and Problems',
    'yazılım_alt_bilgi' => '© 2018 - 2019 Sotylife. All Rights Reserved.',
    'e_posta_yaz' => 'Please enter your email address',
    'mesaj_yaz' => 'Type your message here',



    //navbar.blade.php
    'navbar_telefon' => '',
    'navbar_email' => 'info@sotylife.com',
    'navbar_login' => 'Login',
    'navbar_register' => 'Register',
    'navbar_anasayfa' => 'Home Page',
    'navbar_hakkimizda' => 'About Us',
    'navbar_servislerimiz' => 'Our Services',
    'navbar_emlak_danismanligi' => 'Real Estate Consultancy',
    'navbar_reh_tic_danis' => 'Guidance and Commercial Consulting',
    'navbar_egit_danıs' => 'Education Consultancy',
    'navbar_kripto_forex' => 'Crypto Currency and Forex Trading',
    'navbar_güncel_haber' => 'Latest News',
    'navbar_iletisim' => 'Contact',
    'navbar_arac_kiralama' => 'Rent a Car',
    'navbar_ev_kiralama' => 'Rent a House',
    'navbar_günlük_kiralama' => 'Daily Rental',


    //about-us.blade.php
    'about_us_hakkimizda' => 'About Us',
    'about_us_anasayfa' => 'Home Page',
    'about_us_isim' => 'SOTYLIFE',
    'about_us_isimllc' => 'SOTYLIFE LLC,',

    'about_us_aciklama1' => 'He manages the investment portfolio that makes long-term investments in various business sectors. We are committed to providing well maintained homes with competitive prices and exceptional customer service, with our long-standing reputation and experience in the real estate market.',

    'about_us_aciklama2' => 'SOTYLIFE LLC, provides business guidance and consulting services to businesses. It also provides business consultancy services in the fields of business administration and follow up all transactions, establishment of company, work permit, marriage, sworn translator, notary public and deport.',

 	'about_us_aciklama3' => 'SOTYLIFE LLC, Ukraine; For education, it is one of the most preferred countries for students and parents. Because Ukrainian universities are equivalent to EU universities in terms of education quality. All these pluses are now very easy to register to all Ukrainian universities with SOTYLIFE LLC! With only your high school diploma, you can study in any section you want and at the same time you can master multiple languages while studying. Also, SOTYLIFE LLC invests under the supervision of an expert team in Crypto Currency and Foreign Exchange Trading.',

    'about_us_tanitim_izle' => 'WATCH OUR SHORT VIDEO',


    //cars.blade.php
    'cars_arac_kiralama' => 'RENT A CAR',
    'cars_anasayfa' => 'Home Page',
    'cars_rantacar' => 'RENT A CAR',
    'cars_aciklama' => 'SOTYLIFE LLC, Rent A Car services can be used without any problems. Rent a car that suits your budget rental lease is a subsidiary of the Rent A Car office. You can write your vehicle demand on whatsapp support line and get the necessary information. Prices of vehicles in the economic, medium and luxury segment vary daily and monthly.',
    
    //contact.blade.php
    'contact_iletisim' => 'Contact',
    'contact_anasayfa' => 'Home page',
    'contact_iletisim_bilgileri' => 'Contact information',
    'contact_aciklama' => 'We value your thoughts, please do not hesitate! We will consider your criticism and be better than the hot words.',



    //index.blade.php
    'index_kiralık_daire_aciklama' => 'Daily rental apartments in the amazing cities of Ukraine',
    'index_sotylife_nedir' => 'What is Sotylife?',
    'index_sotylife' => 'Sotylife',
    'index_dunyasi' => 'world',
    'index_aciklama1' => 'Guarantees for tourists and owners',
    'index_incele' => 'Take a look',
    'index_aciklama2' => 'Daily rental apartments in the amazing cities of Ukraine',
    'index_aciklama3' => 'Savings when booking reservation',
    'index_sotylife_ile' => 'With Sotylife',
    'index_konfarlu_ev_tut' => 'House hold comfort',
    'index_ara' => 'Search',
    'index_hakkimizda' => 'About Us',
    'index_aciklama4' => ', He manages the investment portfolio that makes long-term investments in various business sectors. It has a variety of property portfolio consisting of residential areas of Kiev, Odessa and Lviv.',
	'index_aciklama5' => 'With our long-standing reputation and experience in the real estate market, we are committed to providing well-maintained houses with competitive prices and exceptional customer service.',
	'index_aciklama6' => 'SOTYLIFE LLC, provides business guidance and consulting services to businesses.',
    'index_daha_fazla_oku' => 'Read More',
    'index_servislerimiz' => 'OUR SERVICES',
    'index_emlak_danismanligi' => 'Real Estate Consultancy',
    'index_aciklama7' => 'SOTYLIFE LLC manages its investment portfolio that makes long-term investments in various business opportunities.',
    'index_reh_tic_danismanligi' => 'Guidance and Commercial Consulting',
    'index_aciklama8' => 'SOTYLIFE LLC, provides business guidance and consulting services to businesses.',
    'index_kripto_aciklama' => 'SOTYLIFE LLC invests under the supervision of an expert team in Crypto Currency and Foreign Exchange Trading.',
    'index_havaalani_transfer' => 'Airport Transfer',
    'index_havaalani_transfer_aciklama' => 'If you want to experience the comfort of your travels from the first step, you will have to let us welcome you and assure your transfer for your next flight. ',
    'index_rantacar' => 'RENT A CAR SERVICES',
    'index_rantacar_aciklama' => ' Rent A Car services without any problems. Lease your automobile rental contract ...',
    'index_book_your' => 'BOOK YOUR',
    'index_home_here' => 'FIND A HOUSE',
    'index_aciklama9' => 'Choose from thousands of houses, apartments, villas, residences, and apartments!',
    'index_ara_tel' => '',
    'index_gunluk_kiralık' => 'FIND A HOUSE',
    'index_aciklama10' => 'Sotylife is one of the must sucured daily rental house system in Kiev, daily rental apartment, summer house, apartment and holiday villa rental platform. In order to provide daily, weekly and comfortable accommodation in comfortable, economical and home comfort, you can make online and 24/7 secure online rental.',
   

    //information.blade.php
    'information_guncel_haberler' => 'The latest news',
    'information_anasayfa' => 'Home page',
    'information_güncel_haber_detay' => 'Current News details',
    'information_ev_tasarım' => 'House designs',
    'information_tarih_saat' => 'July 18, 2016 / 5 pm',


    //servis.blade.php
    'information_servislerimiz' => 'Our service',
    'information_emlak_danismanligi' => 'Our service',
    'information_aciklama1' => 'SOTYLIFE LLC manages its investment portfolio that makes long-term investments in various business opportunities. It has a variety of property portfolio consisting of residential areas of Kiev, Odessa and Lviv.',
 	'information_aciklama2' => 'With our long-standing reputation and experience in the real estate market, we are committed to providing well-maintained houses with competitive prices and exceptional customer service.',
    'information_reh_tic_danismanligi' => 'Our service',
    'information_reh_tic_danismanligi_aciklama1' => ' SOTYLIFE LLC, provides business guidance and consulting services to businesses.',
    'information_reh_tic_danismanligi_aciklama2' => ' She provides commercial consultancy services in the fields of customs clearance and follow up of all transactions, establishment of company, work permit, marriage, sworn translator, notary public and deport.',
    'information_kripto_forex_acıklama1' => 'SOTYLIFE LLC invests under the supervision of an expert team in Crypto Currency and Foreign Exchange Trading.',
    'information_kripto_forex_acıklama2' => 'It is a decentralized global market where the global market leader crypto currencies trade. ',
   'information_kripto_forex_acıklama3' => 'The Forex market is the world\'s largest and most liquid market with a daily average transaction volume of over $ 5 trillion',
    'information_rentecar' => 'Rent a Car Services ',
    'information_rentecar_aciklama1' => 'SOTYLIFE LLC, Rent A Car services can be used without any problems. Rent a car that suits your budget rental lease is a subsidiary of the Rent A Car office. You can write your vehicle demand on whatsapp support line and get the necessary information.',
    'information_rentecar_aciklama2' => 'Prices of vehicles in the economic, medium and luxury segment vary daily and monthly.  ',
    'information_hacaalani' => 'Airport Transfer ',
    'information_hacaalani_aciklama1' => 'SOTYLIFE LLC, If you want to experience the comfort of your travels from the first step, you can take advantage of the special chauffeured transfer service from Zulhany Airport, Borispol Airport. If you wish, you can also request Turkish, English and Russian drivers.',
    'information_hacaalani_aciklama2' => 'Our driver can stop at the exchange office and market for the duration of the trip. Your name is printed on the paper at the airport. You can easily find our driver with that method. Have a nice journey!',





];
