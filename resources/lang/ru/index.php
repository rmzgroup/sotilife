<?php

return [
    //footer.blade.php
    'iletisim' => 'İletişim',
    'adres1' => '8901 Marmora Raod,',
    'adres2' => 'Glasgow, D04  89GR',

    'telefon1' => 'Telefon : (801) 4256  9856',
    'telefon2' => 'Telefon : (801) 4256  9658',

    'email' => 'Email : info@sheltek.com',

    'bilgi' => '© 2017-2018 sotylife.com sitesinde kullanıcılar tarafından sağlanan her türlü ilan, bilgi, içerik ve görselin gerçekliği, orijinalliği, güvenilirliği ve doğruluğuna ilişkin sorumluluk bu içerikleri giren kullanıcıya ait.',
    'gönder' => 'Gönder',
    'iletisime_gec' => 'İletişime Geçin',
    'iletisime_yazı' => 'Soru Ve Sorunlarınız İçin Bizimle İletişime Geçin',
    'yazılım_alt_bilgi' => '© 2017- @php date("Y") @endphp Sotylife.Tüm Hakları Saklıdır.',
    'e_posta_yaz' => 'E-Posta Adresinizi Yazınız.',
    'mesaj_yaz' => 'Mesajınızı Yazınız',



    //navbar.blade.php
    'navbar_telefon' => '+88 (012) 564 979 56',
    'navbar_email' => 'info@sheltek.com',
    'navbar_login' => 'Giriş Yap',
    'navbar_register' => 'Kayıt Ol',
    'navbar_anasayfa' => 'Anasayfa',
    'navbar_hakkimizda' => 'Hakkımızda',
    'navbar_servislerimiz' => 'Servislerimiz',
    'navbar_emlak_danismanligi' => 'Emlak Danışmanlığı',
    'navbar_reh_tic_danis' => 'Rehberlik ve Ticari Danışmanlık',
    'navbar_egit_danıs' => 'Eğitim Danışmanlığı',
    'navbar_kripto_forex' => 'Kripto para birimi ve Forex Ticareti',
    'navbar_güncel_haber' => 'Güncel Haberler',
    'navbar_iletisim' => 'İLETİŞİM',
    'navbar_arac_kiralama' => 'Araç Kiralama',
    'navbar_ev_kiralama' => 'Ev Kiralama',
    'navbar_günlük_kiralama' => 'Günlük Kiralama',


    //about-us.blade.php
    'about_us_hakkimizda' => 'Hakkımızda',
    'about_us_anasayfa' => 'Anasayfa',
    'about_us_isim' => 'SOTYLIFE',
    'about_us_isimllc' => 'SOTYLİFE LLC,',

    'about_us_aciklama1' => ' Çeşitli iş fırsatlarına uzun vadeli yatırımlar yapan yatırım portföyünü yönetmektedir. Kiev, Odesa, Lviv yerleşim birimlerinden oluşan çeşitli mülk portföyüne sahiptir.Gayrimenkul piyasasındaki uzun yıllara dayanan itibarımız ve tecrübemizle, rekabetçi Fiyatlarla ve olağanüstü müşteri hizmetleriyle bakımlı evler sağlamayı taahhüt ediyoruz.',

    'about_us_aciklama2' => 'SOTYLİFE LLC, İşletmelerin ticari rehberlik ve danışmanlık hizmetleri sunmaktadır. İşletmeler Arası ürünlerin gümrüklenmesi ve tüm işlemlerin takip edilmesi, şirket kurulumu, çalışma izni, Evlilik, yeminli tercümanlık, noter ve deport alanlarında ticari danışmanlık hizmetleri vermektedir.',

 	'about_us_aciklama3' => ' SOTYLİFE LLC, Ukrayna; eğitim için, öğrencilerin ve velilerin en çok tercih ettiği ülkelerden birisidir. Çünkü Ukrayna üniversiteleri eğitim kalitesi bakımından AB üniversiteleri ile denktir.  Tüm bu artıların yanında Ukrayna üniversitelerine kayıt olmak SOTYLİFE LLC, ile artık çok kolay! Sadece lise diplomanız ile  istediğiniz her bölümde eğitim olabilir ve aynı zamanda eğitim alırken birden çok dile hakim olabilirsiniz. Aynı zamanda  SOTYLİFE LLC, Kripto Para Birimi ve Döviz Ticaretinde uzman ekip eşliğinde yatırımlar yapmaktadır.',

    'about_us_tanitim_izle' => 'KISA TANITIM VİDEOMUZU İZLEYİN',


    //cars.blade.php
    'cars_arac_kiralama' => 'ARAÇ KIRALAMA',
    'cars_anasayfa' => 'Anasayfa',
    'cars_rantacar' => 'RENT A CAR',
    'cars_aciklama' => 'SOTYLİFE LLC, Rent A Car hizmetlerinden sorunsuz faydalanabilirsiniz. Bütçenize uygun otomobillerinizi kiralama kontratı iştiraki olduğumuz Rent A Car ofisinden yapılmaktadır. Kiralama yapacağınız tarih ve modeldeki araç talebinizi whatsapp destek hattına yazıp gerekli bilgileri alabilirsiniz. Araç fiyatları ekonomik, orta ve lüks segmentdeki araçların fiyatları günlük ve aylık olarak değişkenlik göstermektedir. ',
    
    //contact.blade.php
    'contact_iletisim' => 'İletişim',
    'contact_anasayfa' => 'Anasayfa',
    'contact_iletisim_bilgileri' => 'İletişim Bilgileri',
    'contact_aciklama' => 'Düşüncelerinize değer veriyoruz, yazın, tereddüt etmeyin! Eleştirinizi dikkate alacağiz ve sıcak sözcüklerden daha iyi olacağız.',



    //index.blade.php
    'index_kiralık_daire_aciklama' => 'Ukraynanın şaşırtıcı şehirlerinde günlük kiralık daireler',
    'index_sotylife_nedir' => 'Sotylife Nedir?',
    'index_sotylife' => 'Sotylife',
    'index_dunyasi' => 'Dünyası',
    'index_aciklama1' => 'Turistler ve sahipler için teminatlar',
    'index_incele' => 'İncele',
    'index_aciklama2' => 'Ukrayna nın şaşırtıcı şehirlerinde günlük kiralık daireler',
    'index_aciklama3' => 'Rezervasyon sırasında tasarruflar yapı',
    'index_sotylife_ile' => 'Sotylife ile',
    'index_konfarlu_ev_tut' => 'konforlu ev tut',
    'index_ara' => 'Ara',
    'index_hakkimizda' => 'Hakkımızda',
    'index_aciklama4' => ', Çeşitli iş fırsatlarına uzun vadeli yatırımlar yapan yatırım portföyünü yönetmektedir. Kiev, Odesa, Lviv yerleşim birimlerinden oluşan çeşitli mülk portföyüne sahiptir.',
	'index_aciklama5' => 'Gayrimenkul piyasasındaki uzun yıllara dayanan itibarımız ve tecrübemizle, rekabetçi Fiyatlarla ve olağanüstü müşteri hizmetleriyle bakımlı evler sağlamayı taahhüt ediyoruz.',
	'index_aciklama6' => 'SOTYLİFE LLC, İşletmelerin ticari rehberlik ve danışmanlık hizmetleri sunmaktadır. İşletmeler.',
    'index_daha_fazla_oku' => 'Daha Fazla Oku',
    'index_servislerimiz' => 'SERVİSLERİMİZ',
    'index_emlak_danismanligi' => 'Emlak Danışmanlığı',
    'index_aciklama7' => 'SOTYLİFE LLC, Çeşitli iş fırsatlarına uzun vadeli yatırımlar yapan yatırım portföyünü yönetmektedir. ',
    'index_reh_tic_danismanligi' => 'Rehberlik ve Ticari Danışmanlık',
    'index_aciklama8' => 'SOTYLİFE LLC, İşletmelerin ticari rehberlik ve danışmanlık hizmetleri sunmaktadır.',
    'index_kripto_aciklama' => 'SOTYLİFE LLC, Kripto Para Birimi ve Döviz Ticaretinde uzman ekip eşliğinde yatırımlar yapmaktadır.',
    'index_havaalani_transfer' => 'Havaalanı Transferi',
    'index_havaalani_transfer_aciklama' => 'Seyahatlerinizde konforu ilk adımdan itibaren yaşamak istiyorsanız, "size özel" Zulhany Havalimanı ... ',
    'index_rantacar' => 'RENT A CAR HİZMETLERİ',
    'index_rantacar_aciklama' => ' Rent A Car hizmetlerinden sorunsuz faydalanabilirsiniz. Bütçenize uygun otomobillerinizi kiralama kontratı ...',
    'index_book_your' => 'BOOK YOUR',
    'index_home_here' => 'HOME HERE',
    'index_aciklama9' => 'Binlerce ev, daire, villa, rezidans, ve apart’dan birini seçin!',
    'index_ara_tel' => 'Ara : +0123  456  789  ',
    'index_gunluk_kiralık' => 'HOME HERE',
    'index_aciklama10' => 'Sotylife Kievin en büyük günlük kiralık ev, günlük kiralık daire, yazlık ev, apart daire ve tatil villası kiralama platformudur. Konforlu, ekonomik ve ev rahatlığında günlük veya haftalık konaklamalar sağlayabilmek adına, 7/24 güvenli ve taksit imkanlı online kiralama yapabilirsiniz. Türkiye ve Dünya’dan 700 ü aşkın ilde günlük kiralık eşyalı konut bulabilmek için hemen Destek Hattımıza ulaşın.',
   

    //information.blade.php
    'information_guncel_haberler' => 'Güncel Haberler',
    'information_anasayfa' => 'Anasayfa',
    'information_güncel_haber_detay' => 'Güncel Haber detayları',
    'information_ev_tasarım' => 'Ev Tasarımları',
    'information_tarih_saat' => 'July 18, 2016 / 5 pm',


    //servis.blade.php
    'information_servislerimiz' => 'Servislerimiz',
    'information_emlak_danismanligi' => 'Servislerimiz',
    'information_aciklama1' => 'SOTYLİFE LLC, Çeşitli iş fırsatlarına uzun vadeli yatırımlar yapan yatırım portföyünü yönetmektedir. Kiev, Odesa, Lviv yerleşim birimlerinden oluşan çeşitli mülk portföyüne sahiptir.',
 	'information_aciklama2' => 'Gayrimenkul piyasasındaki uzun yıllara dayanan itibarımız ve tecrübemizle, rekabetçi Fiyatlarla ve olağanüstü müşteri hizmetleriyle bakımlı evler sağlamayı taahhüt ediyoruz.',
    'information_reh_tic_danismanligi' => 'Servislerimiz',
    'information_reh_tic_danismanligi_aciklama1' => ' SOTYLİFE LLC, İşletmelerin ticari rehberlik ve danışmanlık hizmetleri sunmaktadır. İşletmeler',
    'information_reh_tic_danismanligi_aciklama2' => ' Arası ürünlerin gümrüklenmesi ve tüm işlemlerin takip edilmesi, şirket kurulumu, çalışma izni, Evlilik, yeminli tercümanlık, noter ve deport alanlarında ticari danışmanlık hizmetleri vermektedir.',
    'information_kripto_forex_acıklama1' => 'SOTYLİFE LLC, Kripto Para Birimi ve Döviz Ticaretinde uzman ekip eşliğinde yatırımlar yapmaktadır.',
    'information_kripto_forex_acıklama2' => 'Küresel Pazar lideri olan kripto para birimlerinin ticaret yaptığı merkezi olmayan bir küresel pazardır. ',
   'information_kripto_forex_acıklama3' => 'Forex piyasası, günlük ortalama işlem hacmi 5 trilyon doları aşan, dünyanın en büyük ve en likit piyasasıdır',
    'information_rentecar' => 'RENT A CAR HİZMETLERİ ',
    'information_rentecar_aciklama1' => 'SOTYLİFE LLC, Rent A Car hizmetlerinden sorunsuz faydalanabilirsiniz. Bütçenize uygun otomobillerinizi kiralama kontratı iştiraki olduğumuz Rent A Car ofisinden yapılmaktadır. Kiralama yapacağınız tarih ve modeldeki araç talebinizi whatsapp destek hattına yazıp gerekli bilgileri alabilirsiniz.',
    'information_rentecar_aciklama2' => 'Araç fiyatları ekonomik, orta ve lüks segmentdeki araçların fiyatları günlük ve aylık olarak değişkenlik göstermektedir.   ',
    'information_hacaalani' => 'Havaalanı Transferi ',
    'information_hacaalani_aciklama1' => 'SOTYLİFE LLC,     Seyahatlerinizde konforu ilk adımdan itibaren yaşamak istiyorsanız, "size özel" Zulhany Havalimanı,Borispol Havalimanı özel şoförlü transfer hizmetinden faydalanabilirsiniz. Dilerseniz Türkçe, İngilizce, Rusça bilen şoför de talep edilebilir.',
    'information_hacaalani_aciklama2' => 'Şoförümüz yolculuk süresince döviz bürosu ve market ihtiyacınız için durabilmektedir . Havalimanında isminiz kağıda çıktı alınmaktadır . Şoförümüzü o yöntemle kolaylıkla bulabilirsiniz. İyi Yolculuklar ...',





];
